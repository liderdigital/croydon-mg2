<?php


namespace Croydon\Balance\Controller\Index;

use Croydon\Balance\Helper\Balance;
use Croydon\Balance\Logger\Logger;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Quote\Api\CartRepositoryInterface;

class Index extends Action
{

    protected $resultJsonFactory;

    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;
    /**
     * @var CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Session
     */
    protected $checkoutSession;


    /**
     * Index constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param CartRepositoryInterface $quoteRepository
     * @param CookieManagerInterface $cookieManager
     * @param Logger $logger
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        CartRepositoryInterface $quoteRepository,
        CookieManagerInterface $cookieManager,
        Logger $logger,
        Session $checkoutSession
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->quoteRepository = $quoteRepository;
        $this->cookieManager = $cookieManager;
        $this->logger = $logger;
        $this->checkoutSession = $checkoutSession;
    }


    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $resultJsonFactory = $this->resultJsonFactory->create();
        Balance::$calculate = true;
        $quote = $this->checkoutSession->getQuote();
        $quote->collectTotals()->save();
        $this->quoteRepository->save($quote);
        $resultJsonFactory->setData(['result' => 'sucess']);
        $this->logger->info('adding discount');
        return $resultJsonFactory;
    }
}