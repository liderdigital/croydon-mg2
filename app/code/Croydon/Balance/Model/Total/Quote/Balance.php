<?php

namespace Croydon\Balance\Model\Total\Quote;

use \Croydon\Balance\Logger\Logger;
use \Croydon\Services\Model\BalanceInterface;
use Croydon\Servicios\Aurora\ErrorResponse;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use \Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use \Magento\Quote\Api\Data\ShippingAssignmentInterface;
use \Magento\Quote\Model\Quote;
use \Magento\Quote\Model\Quote\Address\Total;
use \Magento\Quote\Model\Quote\Address\Total\AbstractTotal;

class Balance extends AbstractTotal
{

    /**
     * @var float
     */
    protected $value = 0;
    protected $apply = false;
    /**
     * @var bool
     */
    protected $hasQueried = false;
    /**
     * @var PriceCurrencyInterface
     */
    protected $_priceCurrency;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var BalanceInterface
     */
    protected $balance;
    /**
     * @var CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * Balance constructor.
     * @param PriceCurrencyInterface $priceCurrency
     * @param Logger $logger
     * @param BalanceInterface $balance
     * @param CookieManagerInterface $cookieManager
     */
    public function __construct(
        PriceCurrencyInterface $priceCurrency,
        Logger $logger,
        BalanceInterface $balance,
        CookieManagerInterface $cookieManager
    )
    {
        $this->_priceCurrency = $priceCurrency;
        $this->logger = $logger;
        $this->balance = $balance;
        $this->cookieManager = $cookieManager;
    }

    /**
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this|bool
     */
    public function collect(Quote $quote, ShippingAssignmentInterface $shippingAssignment, Total $total)
    {
        parent::collect($quote, $shippingAssignment, $total);
        $this->getValue($quote);
        $balance = $this->cookieManager->getCookie('balance');
        $this->logger->info('Info: ' . \Croydon\Balance\Helper\Balance::$calculate);
        if (!\Croydon\Balance\Helper\Balance::$calculate) {
            return $this;
        }
        $this->apply = true;

        $total->addTotalAmount('balance', -$this->value);
        $total->addBaseTotalAmount('balance', -$this->value);

        $quote->setBalance(-$this->value);
        $quote->setBaseBalance(-$this->value);

        $total->setBaseGrandTotal($total->getBaseGrandTotal() - $this->value);
        $total->setGrandTotal($total->getGrandTotal() - $this->value);
        $this->logger->info('Added');
        return $this;
    }

    public function fetch(Quote $quote, Total $total)
    {
        $this->getValue($quote);
        return array(
            'code' => 'balance',
            'title' => __('Saldo a favor'),
            'value' => $this->value,
            'apply' => $this->apply
        );
    }

    public function getLabel()
    {
        return __('Saldo a favor');
    }

    private function getValue(Quote $quote)
    {
        $this->logger->info(sprintf('Queried %s', $this->hasQueried));
        if (!$this->hasQueried) {
            try {
                $document = $quote->getShippingAddress()->getDocument();
                if (is_null($document) || empty($document)) {
                    return;
                }
                $facConsulta = $this->balance->facConsultaEstadoCartera($document);
                if ($facConsulta instanceof ErrorResponse) {
                    $this->logger->info(sprintf('Error: %s; %s', $facConsulta->getCodigo(), $facConsulta->getDescripcion()));
                    return;
                }
                $value = (float)$facConsulta->getSaldo();
                $this->value = $value > 0 ? $value : 0;
                $this->hasQueried = true;
            } catch (InvokeServiceException $e) {
                $this->logger->info(sprintf('InvokeServiceException; %s', $e->getMessage()));
            } catch (MethodNotFoundException $e) {
                $this->logger->info(sprintf('MethodNotFoundException; %s', $e->getMessage()));
            }
        }
    }

}
