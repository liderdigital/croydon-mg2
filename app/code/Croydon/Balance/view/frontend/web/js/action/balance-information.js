define([
    'ko',
    'jquery',
    'mage/url',
    'mage/storage',
    'mage/translate',
    'Magento_Checkout/js/model/error-processor',
    'Magento_SalesRule/js/model/payment/discount-messages',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/model/quote',
], function (ko, $, urlBuilder, storage, $t, errorProcessor, messageContainer, totals, getPaymentInformationAction, fullScreenLoader, quote) {
    'use strict';
    return function () {
        var quoteId = quote.getQuoteId(),
            message = $t('Saldo a favor aplicado.');
        fullScreenLoader.startLoader();
        return storage.get(
            urlBuilder.build('balance'),
            JSON.stringify({quote: quoteId, apply: true}),
            true
        ).done(function (response) {
                var deferred;
                deferred = $.Deferred();
                totals.isLoading(true);
                getPaymentInformationAction(deferred);
                $.when(deferred).done(function () {
                    fullScreenLoader.stopLoader();
                    totals.isLoading(false);
                });
                messageContainer.addSuccessMessage({
                    'message': message
                });
            }
        ).fail(function (response) {
            fullScreenLoader.stopLoader();
            totals.isLoading(false);
            errorProcessor.process(response, messageContainer);
        });
    }
});