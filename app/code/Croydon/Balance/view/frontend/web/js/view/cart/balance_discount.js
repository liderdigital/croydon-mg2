/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/totals',
    'Magento_Catalog/js/price-utils'
], function (Component, quote, totals, priceUtils) {
    return Component.extend({
        defaults: {
            template: 'Croydon_Balance/cart/balance_discount'
        },
        totals: quote.getTotals(),
        isDisplayed: function () {
            var balance = totals.getSegment('balance');
            console.log(balance);
            var value = balance.value;
            return value > 0;
        },
        getValue: function () {
            var balance = totals.getSegment('balance');
            var value = balance.value;
            return '- ' + priceUtils.formatPrice(value, quote.getBasePriceFormat());
        }
    });
});