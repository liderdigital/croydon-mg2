define([
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/totals',
    'Magento_Catalog/js/price-utils',
    'Croydon_Balance/js/action/balance-information'
], function (Component, quote, totals, priceUtils, balanceInformationAction) {
    return Component.extend({
        defaults: {
            template: 'Croydon_Balance/payment/balance',
        },
        totals: quote.getTotals(),
        initialize: function () {
            this._super();
            return this;
        },
        getTotal: function () {
            var balance = totals.getSegment('balance');
            var value = balance.value;
            return '- ' + priceUtils.formatPrice(value, quote.getBasePriceFormat());
        },
        isVisibleBalance: function () {
            var balance = totals.getSegment('balance');
            var value = balance.value;
            return value > 0;
        },
        apply: function () {
            balanceInformationAction();
        }
    })
});
