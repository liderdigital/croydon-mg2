<?php
/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 14/05/19
 * Time: 9:20
 */

namespace Croydon\Baloto\Controller\Index;


use Croydon\Services\Helper\Data\ConsumeService;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * @var \Magento\Framework\Registry $_coreRegistry
     */
    protected $_coreRegistry;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $resultPage = $this->_pageFactory->create();
        return $resultPage;
    }


}