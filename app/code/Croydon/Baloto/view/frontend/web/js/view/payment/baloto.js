define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'baloto',
                component: 'Croydon_Baloto/js/view/payment/method-renderer/baloto-method'
            }
        );
        return Component.extend({});
    }
);