/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper'
], function ($, wrapper) {
    'use strict';
    console.log(wrapper);
    return function (targetModule) {
        console.log(targetModule);
        var reloadPrice = targetModule.prototype._reloadPrice;
        targetModule.prototype._reloadPrice = wrapper.wrap(reloadPrice, function (original) {
            //do extra stuff
            //call original method
            var result = original();
            //do extra stuff
            var simpleSku = this.options.spConfig.skus[this.simpleProduct];
            console.log(simpleSku);
            if (simpleSku !== '') {
                $('div.product-info-main .product.attibute.sku .value').html(simpleSku);
            }
            //return original value
            return result;
        });
        return targetModule;
    };
});