<?php


namespace Croydon\Checkout\Helper;

use Croydon\Checkout\Logger\Logger;
use Magento\Checkout\Api\Data\ShippingInformationExtensionInterface;
use Magento\Customer\Model\Session;
use \Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Quote\Api\Data\AddressExtensionInterface;

class HelperRegisterShippingInformation
{

    /**
     * @var Session
     */
    protected $session;
    /**
     * @var ShippingInformationInterface
     */
    protected $shippingInformation;
    /**
     * @var AddressExtensionInterface|null
     */
    protected $extencionAttributes;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * HelperRegisterShippingInformation constructor.
     * @param Session $session
     * @param Logger $logger
     */
    public function __construct(Session $session, Logger $logger)
    {
        $this->session = $session;
        $this->logger = $logger;
    }

    /**
     * @param ShippingInformationInterface $shippingInformation
     */
    public function setShippingInformation(ShippingInformationInterface $shippingInformation): void
    {
        $this->shippingInformation = $shippingInformation;
        $this->extencionAttributes = $shippingInformation->getShippingAddress()->getExtensionAttributes();
    }

    public function getDocument()
    {
        $this->logger->info(sprintf('Logged: %s, Document: %s', $this->session->isLoggedIn(), $this->extencionAttributes->getDocument()));
        if ($this->session->isLoggedIn())
            return $this->session->getCustomer()->getDocumentNumber();
        return $this->extencionAttributes->getDocument();
    }

    public function getGender()
    {
        if ($this->session->isLoggedIn())
            return $this->session->getCustomer()->getGender();
        return $this->extencionAttributes->getGender();
    }

    public function getSubscriber()
    {
        if ($this->session->isLoggedIn())
            return false;
        return $this->extencionAttributes->getSubscriber();
    }

    public function getTerms()
    {
        return true;
    }

    public function getType()
    {
        if ($this->session->isLoggedIn())
            return $this->session->getCustomer()->getDocumentType();
        return $this->extencionAttributes->getType();
    }
}