<?php


namespace Croydon\Checkout\Helper\Model;

use Croydon\Servicios\Aurora\ErrorResponse;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Croydon\Checkout\Logger\Logger;
use Croydon\Services\Model\InventoryInterface;

class Cart extends AbstractHelper
{


    /**
     * @var InventoryInterface
     */
    protected $inventoryQuery;

    /**
     * @var bool
     */
    protected $services = true;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * Cart constructor.
     * @param Context $context
     * @param InventoryInterface $inventoryQuery
     * @param Logger $logger
     */
    public function __construct(Context $context, InventoryInterface $inventoryQuery, Logger $logger)
    {
        parent::__construct($context);
        $this->inventoryQuery = $inventoryQuery;
        $this->logger = $logger;
    }

    /**
     * @param $sku
     * @param $qty
     * @return bool
     * @throws \Exception
     */
    public function invConsultaDispItemBod($sku, $qty)
    {
        if (!$this->services) {
            return true;
        }
        try {
            $response = $this->inventoryQuery->invConsultaDispItemBod($sku, $qty);
        } catch (InvokeServiceException $e) {
            $this->logger->info(sprintf('Error Servicios: %s; Sku: %s', sprintf('%s:%s', __METHOD__, __LINE__), $sku));
            throw new \Exception($e->getMessage());
        } catch (MethodNotFoundException $e) {
            $this->logger->info(sprintf('Error Servicios: %s; Sku: %s', sprintf('%s:%s', __METHOD__, __LINE__), $sku));
            throw new \Exception($e->getMessage());
        }
        if ($response instanceof ErrorResponse || $response->getCantDisponible() < $qty) {
            $this->logger->info(sprintf('Producto no disponible: %s; Sku: %s', sprintf('%s:%s', __METHOD__, __LINE__), $sku));
            return false;
        }
        return true;
    }
}