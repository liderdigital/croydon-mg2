<?php


namespace Croydon\Checkout\Helper;


use Magento\Framework\Setup\SchemaSetupInterface;

class Setup
{
    /**
     * @var string
     */
    const birth_date = 'birth_date';
    /**
     * @var string
     */
    const gender = 'gender';
    /**
     * @var string
     */
    const terms = 'terms';
    /**
     * @var string
     */
    const subscriber = 'subscriber';
    /**
     * @var string
     */
    const document = 'document';
    /**
     * @var string
     */
    const type = 'type';
    /**
     * @var string
     */
    const OrderIncrementId = 'increment_id';
    /**
     * @var string
     */
    const AuroraOrder = 'aurora_order';
    /**
     * @var string
     */
    const AuroraInvoice = 'aurora_invoice';


    /**
     * @param SchemaSetupInterface $installer
     * @param $table
     */
    public function addColumnIncrementId(SchemaSetupInterface $installer, $table)
    {
        $installer->getConnection()
            ->addColumn($installer->getTable($table),
                Setup::OrderIncrementId,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Order increment id'
                ]
            );
    }

    public function addColumnsAurora(SchemaSetupInterface $installer, $table)
    {
        $installer->getConnection()
            ->addColumn($installer->getTable($table),
                Setup::AuroraOrder,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Order increment id'
                ]
            );
        $installer->getConnection()
            ->addColumn($installer->getTable($table),
                Setup::AuroraInvoice,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Order increment id'
                ]
            );
    }


    /**
     * @param SchemaSetupInterface $installer
     * @param $table
     */
    public function addColumns(SchemaSetupInterface $installer, $table)
    {
        $installer->getConnection()
            ->addColumn(
                $installer->getTable($table),
                Setup::birth_date,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => NULL,
                    'comment' => 'Birth date'
                ]
            );
        $installer->getConnection()
            ->addColumn(
                $installer->getTable($table),
                Setup::type,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => NULL,
                    'comment' => 'Document type'
                ]
            );
        $installer->getConnection()
            ->addColumn(
                $installer->getTable($table),
                Setup::document,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'length' => 255,
                    'comment' => 'Document number'
                ]
            );
        $installer->getConnection()
            ->addColumn(
                $installer->getTable($table),
                Setup::gender,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'default' => NULL,
                    'comment' => 'Gender'
                ]
            );
        $installer->getConnection()
            ->addColumn(
                $installer->getTable($table),
                Setup::subscriber,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'nullable' => true,
                    'default' => 0,
                    'comment' => 'Subscriber'
                ]
            );
        $installer->getConnection()
            ->addColumn(
                $installer->getTable($table),
                Setup::terms,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'nullable' => true,
                    'default' => 0,
                    'comment' => 'Gender'
                ]
            );
    }

    /**
     * @param SchemaSetupInterface $installer
     * @param $table
     */
    public function updateColumnGender(SchemaSetupInterface $installer, $table)
    {
        $installer->getConnection()
            ->changeColumn(
                $installer->getTable($table),
                Setup::gender,
                Setup::gender,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 12
                ]
            );
    }

    /**
     * @param SchemaSetupInterface $installer
     * @param $table
     */
    public function updateColumnType(SchemaSetupInterface $installer, $table)
    {
        $installer->getConnection()
            ->changeColumn(
                $installer->getTable($table),
                Setup::type,
                Setup::type,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 12
                ]
            );
    }
}