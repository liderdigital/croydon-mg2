<?php


namespace Croydon\Checkout\Helper;

use Croydon\Checkout\Logger\Logger;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Model\Order;

class ShippingInformation
{

    protected $quoteRepository;

    protected $logger;
    protected $request;
    /**
     * @var HelperRegisterShippingInformation
     */
    protected $helperRegisterShippingInformation;

    /**
     * ShippingInformationManagement constructor.
     * @param QuoteRepository $quoteRepository
     * @param Logger $logger
     * @param HelperRegisterShippingInformation $helperRegisterShippingInformation
     */
    public function __construct(
        QuoteRepository $quoteRepository,
        Logger $logger,
        HelperRegisterShippingInformation $helperRegisterShippingInformation
    )
    {
        $this->quoteRepository = $quoteRepository;
        $this->logger = $logger;
        $this->helperRegisterShippingInformation = $helperRegisterShippingInformation;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     * @throws NoSuchEntityException
     */
    public function beforeSaveAddressInformation(ShippingInformationManagement $subject, $cartId, ShippingInformationInterface $addressInformation)
    {
        $shippingAddress = $addressInformation->getShippingAddress();
        $extAttributes = $shippingAddress->getExtensionAttributes();
        $this->helperRegisterShippingInformation->setShippingInformation($addressInformation);
        $birthDate = $extAttributes->getBirthDate();
        $gender = $this->helperRegisterShippingInformation->getGender();
        $terms = $this->helperRegisterShippingInformation->getTerms();
        $subscriber = $this->helperRegisterShippingInformation->getSubscriber();
        $document = $this->helperRegisterShippingInformation->getDocument();
        $type = $this->helperRegisterShippingInformation->getType();
        if ($extAttributes) {
            $this->logger->info(sprintf('%s; %s; %s; %s;%s',
                $document,
                $gender,
                $subscriber,
                $terms,
                $type
            ));
            //$shippingAddress->setBirthDate($shippingAddressExtensionAttributes->getBirthDate());
            $shippingAddress->setDocument($document);
            $shippingAddress->setGender($gender);
            $shippingAddress->setSubscriber($subscriber);
            $shippingAddress->setTerms($terms);
            $shippingAddress->setType($type);
        }
        try {
            $quote = $this->quoteRepository->getActive($cartId);
            $this->logger->info(sprintf('Entity id: %s', $quote->getEntityId()));
            $this->logger->info('Email: ' . $quote->getCustomerEmail() . ' ' . $quote->getShippingAddress()->getEmail());
            $quote->setBirthDate($birthDate);
            $quote->setGender($gender);
            $quote->setTerms($terms);
            $quote->setSubscriber($subscriber);
            $quote->setDocument($document);
            $quote->setType($type);
            $quote->setIncrementId($quote->getId());
            $quote->save();
            $this->logger->info(sprintf('Increment Id %s', $quote->getIncrementId()));
        } catch (NoSuchEntityException $e) {
            throw new NoSuchEntityException(new Phrase($e->getMessage()));
        }
    }

    /**
     * @return Order|null
     */
    protected function getLastOrder()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var Order $orderDatamodel */
        $orderDatamodel = $objectManager->get(Order::class)->getCollection()->getLastItem();
        return $orderDatamodel;
    }

    protected function getIncrementId()
    {
        $order = $this->getLastOrder();
        if (is_null($order)) {
            return '000000001';
        }
        return $order->getIncrementId() + 1;
    }

}