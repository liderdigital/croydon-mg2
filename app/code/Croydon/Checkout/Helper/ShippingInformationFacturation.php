<?php

namespace Croydon\Checkout\Helper;

use Croydon\Checkout\Exceptions\QtyException;
use Croydon\Checkout\Logger\Logger;
use Croydon\Services\Model\FacturationInterface;
use Croydon\Services\Model\InventoryInterface;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Quote\Model\QuoteRepository;
use Magento\Quote\Model\ResourceModel\Quote\Item;

class ShippingInformationFacturation extends AbstractHelper
{

    /**
     * @var FacturationInterface
     */
    protected $facturation;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var InventoryInterface
     */
    protected $inventory;

    /**
     * @var
     */
    protected $quote;

    /**
     * @var ValidateResponse
     */
    protected $validateResponse;

    /**
     * ShippingInformationFacturation constructor.
     * @param Context $context
     * @param FacturationInterface $facturation
     * @param QuoteRepository $quoteRepository
     * @param InventoryInterface $inventory
     * @param ValidateResponse $validateResponse
     * @param Logger $logger
     */
    public function __construct(Context $context, FacturationInterface $facturation, QuoteRepository $quoteRepository, InventoryInterface $inventory, ValidateResponse $validateResponse, Logger $logger)
    {
        parent::__construct($context);
        $this->facturation = $facturation;
        $this->quoteRepository = $quoteRepository;
        $this->logger = $logger;
        $this->inventory = $inventory;
        $this->validateResponse = $validateResponse;
    }

    /**
     *
     * @param $quoteId
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     * @throws QtyException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function validateQty($quoteId)
    {
        $this->logger->info('Validating qty');
        $errors = [];
        $this->quote = $this->quoteRepository->getActive($quoteId);
        $items = $this->quote->getAllItems();
        $reflection = new \ReflectionClass($this->inventory);
        $this->logger->info(sprintf('Class: %s', $reflection->getName()));
        /** @var Item $item */
        foreach ($items as $item) {
            if (!$this->inventory->invConsultaDispItemBod($item->getSku(), $item->getQty())) {
                $errors[] = [
                    'sku' => $item->getSku(),
                    'name' => $item->getName()
                ];
            }
        }
        if (count($errors) > 0) {
            $this->logger->info(sprintf('Cart id: %s; Productos no disponibles: %s', $quoteId, json_encode($errors)));
            throw new QtyException();
        }
    }

    /**
     * @param $quoteId
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     * @throws \Exception
     */
    public function factActOrder($quoteId)
    {
        try {
            $response = $this->facturation->factAct($this->quote);
            $this->logger->info(sprintf('Aurora code: %s', $response->getNumdoc()));
            $this->validateResponse->validate($response);
            $this->quote->setAuroraOrder($response->getNumdoc());
            $this->quoteRepository->save($this->quote);
        } catch (InvokeServiceException $e) {
            throw new InvokeServiceException($e);
        } catch (MethodNotFoundException $e) {
            throw new MethodNotFoundException($e->getMessage());
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}
