<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Checkout\Helper;

use Croydon\Servicios\Facturacion\FacActOrdenPedidoResponse;

/**
 * Description of ValidateResponse
 *
 * @author jonathan
 */
class ValidateResponse {

    /**
     * @param FacActOrdenPedidoResponse $response
     * @throws ResponseException 
     */
    public function validate(FacActOrdenPedidoResponse $response) {
        if ($response->getCoddoc() == 'ER') {
            throw new ResponseException($response->getMsg());
        }
        if ($response->getCoddoc() != 'PE' && $response->getCoddoc() != 'FA') {
            throw new ResponseException($response->getMsg());
        }
        if (str_replace(' ', '', $response->getNumdoc()) == '') {
            throw new ResponseException($response->getMsg());
        }
    }

}
