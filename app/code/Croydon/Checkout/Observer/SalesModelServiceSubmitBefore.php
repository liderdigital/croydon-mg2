<?php

namespace Croydon\Checkout\Observer;

use Croydon\Checkout\Helper\ValidateResponse;
use Croydon\Checkout\Logger\Logger;
use Croydon\Services\Model\FacturationInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Phrase;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;

class SalesModelServiceSubmitBefore implements ObserverInterface
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var FacturationInterface
     */
    private $facturation;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var ValidateResponse
     */
    protected $validateResponse;

    /**
     * SalesModelServiceSubmitBefore constructor.
     * @param Logger $logger
     * @param FacturationInterface $facturation
     * @param ManagerInterface $messageManager
     * @param ValidateResponse $validateResponse
     */
    public function __construct(Logger $logger, FacturationInterface $facturation, ManagerInterface $messageManager, ValidateResponse $validateResponse)
    {
        $this->logger = $logger;
        $this->facturation = $facturation;
        $this->messageManager = $messageManager;
        $this->validateResponse = $validateResponse;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        $message = 'No es posible procesar su pedido en este momento';
        /** @var Quote $quote */
        $quote = $observer->getEvent()->getData('quote');
        $this->logger->info(sprintf('Quote Increment id: %s', $quote->getAuroraOrder()));
        /** @var Order $order */
        $order = $observer->getEvent()->getData('order');
        try {
            $response = $this->facturation->factActFact($order);
            $this->validateResponse->validate($response);
            $quote->setAuroraInvoice($response->getNumdoc());
            $quote->save();
            $order->setAuroraInvoice($response->getNumdoc());
            $order->save();
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($message));
            throw new LocalizedException(new Phrase(__($message)));
        }
    }

}
