<?php


namespace Croydon\Checkout\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\QuoteFactory;
use Croydon\Checkout\Logger\Logger;
use Magento\Sales\Model\Order;

class SalesOrderSaveAfter implements ObserverInterface
{

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var QuoteFactory
     */
    private $quoteFactory;

    /**
     * SalesOrderSaveAfter constructor.
     * @param Logger $logger
     */
    public function __construct(
        Logger $logger,
        QuoteFactory $quoteFactory
    )
    {
        $this->logger = $logger;
        $this->quoteFactory = $quoteFactory;
    }


    /**
     * @param Observer $observer
     * @return void
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getOrder();
        $quote = $this->quoteFactory->create()->load($order->getQuoteId());
        $order->getShippingAddress()->setDocument($quote->getShippingAddress()->getDocument());
        $order->getShippingAddress()->setGender($quote->getShippingAddress()->getGender());
        $order->getShippingAddress()->setBirthDate($quote->getShippingAddress()->getBirthDate());
        $order->getShippingAddress()->setSubscriber($quote->getShippingAddress()->getSubscriber());
        $order->getShippingAddress()->setTerms($quote->getShippingAddress()->getTerms());
        $order->getShippingAddress()->setType($quote->getShippingAddress()->getType());
        $order->getShippingAddress()->save();
        if (is_null($quote->getBillingAddress()->getDocument())) {
            $quote->getBillingAddress()->setDocument($quote->getShippingAddress()->getDocument());
            $quote->getBillingAddress()->setGender($quote->getShippingAddress()->getGender());
            $quote->getBillingAddress()->setBirthDate($quote->getShippingAddress()->getBirthDate());
            $quote->getBillingAddress()->setSubscriber($quote->getShippingAddress()->getSubscriber());
            $quote->getBillingAddress()->setTerms($quote->getShippingAddress()->getTerms());
            $quote->getBillingAddress()->setType($quote->getShippingAddress()->getType());
            $quote->save();
        }
        $order->getBillingAddress()->setDocument($quote->getBillingAddress()->getDocument());
        $order->getBillingAddress()->setGender($quote->getBillingAddress()->getGender());
        $order->getBillingAddress()->setBirthDate($quote->getBillingAddress()->getBirthDate());
        $order->getBillingAddress()->setSubscriber($quote->getBillingAddress()->getSubscriber());
        $order->getBillingAddress()->setTerms($quote->getBillingAddress()->getTerms());
        $order->getBillingAddress()->setType($quote->getBillingAddress()->getType());
        $order->getBillingAddress()->save();
        $order->setAuroraOrder($quote->getAuroraOrder());
        $order->setAuroraInvoice($quote->getAuroraInvoice());
        $order->save();
    }
}