<?php


namespace Croydon\Checkout\Plugin\Checkout;


class LayoutProcessor
{
    private $documentCode = 'document';
    private $type = 'type';
    private $birthdate = 'birthdate';
    private $subscriber = 'suscriber';
    private $terms = 'terms';
    private $gender = 'gender';

    public function process(array $jsLayout)
    {
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$this->documentCode] = $this->document();
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$this->type] = $this->type();
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$this->birthdate] = $this->birthdate();
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$this->subscriber] = $this->subscriber();
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$this->terms] = $this->terms();
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$this->gender] = $this->gender();
        return $jsLayout;
    }

    private function document()
    {
        $document = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                // customScope is used to group elements within a single form (e.g. they can be validated separately)
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
            ],
            'dataScope' => 'shippingAddress.custom_attributes.' . $this->documentCode,
            'label' => 'Documento',
            'provider' => 'checkoutProvider',
            'sortOrder' => 0,
            'validation' => [
                'required-entry' => true,
                'min_text_length' => 6,
                'max_text_length' => 10,
                'validate-number' => true
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            'value' => '', // value field is used to set a default value of the attribute,
            'additionalClasses' => 'custom-document'
        ];
        return $document;
    }

    private function type()
    {
        $document = [
            'component' => 'Magento_Ui/js/form/element/select',
            'config' => [
                // customScope is used to group elements within a single form (e.g. they can be validated separately)
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
            ],
            'options' => [
                [
                    'value' => 'cc',
                    'label' => 'CC'
                ],
                [
                    'value' => 'ce',
                    'label' => 'CE'
                ],
                [
                    'value' => 'nit',
                    'label' => 'NIT'
                ]
            ],
            'dataScope' => 'shippingAddress.custom_attributes.' . $this->type,
            'label' => 'Tipo',
            'provider' => 'checkoutProvider',
            'sortOrder' => 5,
            'validation' => [
                'required-entry' => true,
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            'value' => '', // value field is used to set a default value of the attribute
            'additionalClasses' => 'custom-type-document'
        ];
        return $document;
    }

    public function birthdate()
    {
        $birthdate = [
            'component' => 'Magento_Ui/js/form/element/date',
            'config' => [
                // customScope is used to group elements within a single form (e.g. they can be validated separately)
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/date',
            ],
            'dataScope' => 'shippingAddress.custom_attributes.' . $this->birthdate,
            'label' => 'Fecha de nacimiento',
            'provider' => 'checkoutProvider',
            'sortOrder' => 150,
            'validation' => [
                'required-entry' => true
            ],
            'options' => [
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => sprintf('1900:%s', date('Y') - 18)
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            'value' => '', // value field is used to set a default value of the attribute,
        ];
        return $birthdate;
    }

    public function subscriber()
    {
        $subscriber = [
            'component' => 'Magento_Ui/js/form/element/boolean',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/checkbox',
            ],
            'dataScope' => 'shippingAddress.custom_attributes.' . $this->subscriber,
            'label' => 'Deseo recibir ofertas por correo electrónico',
            'provider' => 'checkoutProvider',
            'sortOrder' => 200,
            'validation' => [
                'required-entry' => true
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            //'value' => '', // value field is used to set a default value of the attribute,
            'additionalClasses' => 'custom-susbcriber'
        ];
        return $subscriber;
    }

    public function terms()
    {
        $terms = [
            'component' => 'Magento_Ui/js/form/element/boolean',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'Croydon_Checkout/terms',
            ],
            'dataScope' => 'shippingAddress.custom_attributes.' . $this->terms,
            'label' => '',
            'provider' => 'checkoutProvider',
            'sortOrder' => 210,
            'validation' => [
                'required-entry' => true,
                'checked' => true
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            //'value' => '', // value field is used to set a default value of the attribute,
            'additionalClasses' => 'custom-terms'
        ];
        return $terms;
    }

    public function gender()
    {
        $gender = [
            'component' => 'Magento_Ui/js/form/element/select',
            'config' => [
                // customScope is used to group elements within a single form (e.g. they can be validated separately)
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
            ],
            'options' => [
                [
                    'value' => '',
                    'label' => 'Seleccione'
                ],
                [
                    'value' => '1',
                    'label' => 'Hombre'
                ],
                [
                    'value' => '2',
                    'label' => 'Mujer'
                ]
            ],
            'dataScope' => 'shippingAddress.custom_attributes.' . $this->gender,
            'label' => 'Género:',
            'provider' => 'checkoutProvider',
            'sortOrder' => 112,
            'validation' => [
                'required-entry' => true,
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            'value' => '', // value field is used to set a default value of the attribute
            'additionalClasses' => 'custom-gender'
        ];
        return $gender;
    }
}