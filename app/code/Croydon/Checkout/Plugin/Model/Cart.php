<?php


namespace Croydon\Checkout\Plugin\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Phrase;
use Croydon\Checkout\Logger\Logger;
use Magento\Quote\Model\ResourceModel\Quote\Item;
use \Croydon\Checkout\Helper\Model\Cart as HelperCart;

class Cart
{

    /**
     * @var Configurable
     */
    protected $configurableProduct;
    /**
     * @var ManagerInterface
     */
    private $_messageManager;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var HelperCart
     */
    protected $cart;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Cart constructor.
     * @param Configurable $configurableProduct
     * @param ManagerInterface $_messageManager
     * @param Logger $logger
     * @param ProductRepositoryInterface $productRepository
     * @param HelperCart $cart
     */
    public function __construct(
        Configurable $configurableProduct,
        ManagerInterface $_messageManager,
        Logger $logger,
        ProductRepositoryInterface $productRepository,
        HelperCart $cart)
    {
        $this->configurableProduct = $configurableProduct;
        $this->_messageManager = $_messageManager;
        $this->logger = $logger;
        $this->cart = $cart;
        $this->productRepository = $productRepository;
    }

    /**
     * @param \Magento\Checkout\Model\Cart $subject
     * @param Product $productInfo
     * @param DataObject|null $requestInfo
     * @return array
     * @throws \Exception
     */
    public function beforeAddProduct(\Magento\Checkout\Model\Cart $subject, $productInfo, $requestInfo = null)
    {
        $message = 'El producto no esta disponible en estos momentos';
        $this->logger->info(sprintf('%s; Sku: %s', __METHOD__, $productInfo->getSku()));
        $simpleProduct = $this->configurableProduct->getProductByAttributes(isset($requestInfo['super_attribute']) ? $requestInfo['super_attribute'] : '', $productInfo);
        if (!$simpleProduct) {
            $simpleProduct = $productInfo;
        }
        $item = $this->getRealQty($subject, $productInfo);
        $qty = isset($requestInfo['qty']) ? $requestInfo['qty'] : 1;
        $backQty = !is_null($item) ? $item->getQty() : 0;
        $realQty = !is_null($item) ? $backQty + $qty : $qty;
        try {
            if (!$this->cart->invConsultaDispItemBod($simpleProduct->getSku(), $realQty)) {
                throw new \Exception($message);
            }
        } catch (\Exception $e) {
            $this->logger->info(sprintf('Producto no disponible: %s; Sku: %s', sprintf('%s:%s', __METHOD__, __LINE__), $productInfo->getSku()));
            $this->_messageManager->addErrorMessage(__($message));
            throw new LocalizedException(new Phrase(__($message)));
        }
        return array($productInfo, $requestInfo);
    }

    /**
     * @param \Magento\Checkout\Model\Cart $subject
     * @param Product $product
     * @return Item|null
     */
    public function getRealQty(\Magento\Checkout\Model\Cart $subject, Product $product)
    {
        $items = $subject->getQuote()->getAllItems();
        /** @var Item $item */
        foreach ($items as $item) {
            if ($item->getSku() == $product->getSku()) {
                return $item;
            }
        }
    }

}