<?php


namespace Croydon\Checkout\Plugin\Model;

use Croydon\Checkout\Exceptions\QtyException;
use Croydon\Checkout\Helper\ShippingInformationFacturation;
use Croydon\Checkout\Logger\Logger;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement as ShippingInformation;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Croydon\Checkout\Helper\ShippingInformation as HelperShippingInformation;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Phrase;


class ShippingInformationManagement
{

    /**
     * @var HelperShippingInformation
     */
    protected $helperShippingInformation;

    /**
     * @var ShippingInformationFacturation
     */
    protected $shippingInformationFacturation;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * ShippingInformationManagement constructor.
     * @param HelperShippingInformation $helperShippingInformation
     * @param ShippingInformationFacturation $shippingInformationFacturation
     * @param ManagerInterface $messageManager
     * @param Logger $logger
     */
    public function __construct(HelperShippingInformation $helperShippingInformation, ShippingInformationFacturation $shippingInformationFacturation, ManagerInterface $messageManager, Logger $logger)
    {
        $this->helperShippingInformation = $helperShippingInformation;
        $this->shippingInformationFacturation = $shippingInformationFacturation;
        $this->messageManager = $messageManager;
        $this->logger = $logger;
    }


    /**
     * @param ShippingInformation $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     * @throws LocalizedException
     */
    public function beforeSaveAddressInformation(ShippingInformation $subject, $cartId, ShippingInformationInterface $addressInformation)
    {
        /** @var Logger $logger */
        $message = 'No es posible procesar su orden en este momento';
        try {
            $this->logger->info('Cart id: ' . $cartId);
            $this->helperShippingInformation->beforeSaveAddressInformation($subject, $cartId, $addressInformation);
        } catch (\Exception $e) {
            $this->logger->info('Exception: ' . $e->getMessage());
            $this->messageManager->addErrorMessage(__($message));
            throw new LocalizedException(new Phrase(__($message)));
        }
    }


    /**
     * @param ShippingInformation $shipping
     * @param \Magento\Checkout\Model\PaymentDetails $result
     * @param $cartId
     * @return \Magento\Checkout\Model\PaymentDetails
     * @throws LocalizedException
     */
    public function afterSaveAddressInformation(ShippingInformation $shipping, $result, $cartId)
    {

        $message = 'No es posible procesar su orden en este momento';
        try {
            $this->logger->info('Creando orden');
            $this->shippingInformationFacturation->validateQty($cartId);
            $this->shippingInformationFacturation->factActOrder($cartId);
        } catch (QtyException $e) {
            $this->logger->info('Producto no disponible: ');
            $messageQty = 'El producto no esta disponible en la cantidad deseada en este momento';
            $this->messageManager->addErrorMessage(__($messageQty));
            throw new LocalizedException(new Phrase(__($messageQty)));
        } catch (\Exception $e) {
            $this->logger->info('Exception: ' . $e->getMessage());
            $this->messageManager->addErrorMessage(__($message));
            throw new LocalizedException(new Phrase(__($message)));
        }
        return $result;
    }
}