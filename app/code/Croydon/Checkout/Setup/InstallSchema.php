<?php


namespace Croydon\Checkout\Setup;


use Croydon\Checkout\Helper\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup->startSetup();
        $setupClass = new Setup();
        $setupClass->addColumns($installer, 'quote_address');
        $setupClass->addColumns($installer, 'sales_order_address');
        $installer->endSetup();
    }

}