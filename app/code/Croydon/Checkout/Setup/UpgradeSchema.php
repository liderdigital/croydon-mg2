<?php


namespace Croydon\Checkout\Setup;


use Croydon\Checkout\Helper\Setup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup->startSetup();
        $setupClass = new Setup();
        //---- For upload in production
        $setupClass->addColumnIncrementId($installer, 'quote');
        $setupClass->addColumnsAurora($installer, 'quote');
        $setupClass->addColumnsAurora($installer, 'sales_order');
        //------
        //$setupClass->updateColumnGender($installer, 'quote_address');
        //$setupClass->updateColumnGender($installer, 'sales_order_address');
        //$setupClass->updateColumnType($installer, 'quote_address');
        //$setupClass->updateColumnType($installer, 'sales_order_address');
        $installer->endSetup();
    }
}