/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/model/customer',
], function ($, wrapper, quote, customer) {
    'use strict';
    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }
            if (!customer.isLoggedIn()) {
                shippingAddress['extension_attributes']['type'] = shippingAddress.customAttributes['type'];
                shippingAddress['extension_attributes']['document'] = shippingAddress.customAttributes['document'];
                //shippingAddress['extension_attributes']['birth_date'] = shippingAddress.customAttributes['birth_date'];
                shippingAddress['extension_attributes']['gender'] = shippingAddress.customAttributes['gender'];
                shippingAddress['extension_attributes']['terms'] = shippingAddress.customAttributes['terms'];
                shippingAddress['extension_attributes']['subscriber'] = shippingAddress.customAttributes['subscriber'];
            }
            shippingAddress['email'] = $('#customer-email').val();
            console.log('saving extension_attributes');
            console.log(shippingAddress);
            // pass execution to original action ('Magento_Checkout/js/action/set-shipping-information')
            return originalAction();
        });
    };
});