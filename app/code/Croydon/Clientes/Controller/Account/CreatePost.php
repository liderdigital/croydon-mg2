<?php


namespace Croydon\Clientes\Controller\Account;

use Croydon\Clientes\Logger\Logger;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Customer\Helper\Address;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Customer\Model\Metadata\FormFactory;
use Magento\Customer\Model\Registration;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlFactory;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Store\Model\StoreManagerInterface;


class CreatePost extends \Magento\Customer\Controller\Account\CreatePost
{

    private $validator;
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * CreatePost constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param AccountManagementInterface $accountManagement
     * @param Address $addressHelper
     * @param UrlFactory $urlFactory
     * @param FormFactory $formFactory
     * @param SubscriberFactory $subscriberFactory
     * @param RegionInterfaceFactory $regionDataFactory
     * @param AddressInterfaceFactory $addressDataFactory
     * @param CustomerInterfaceFactory $customerDataFactory
     * @param CustomerUrl $customerUrl
     * @param Registration $registration
     * @param Escaper $escaper
     * @param CustomerExtractor $customerExtractor
     * @param DataObjectHelper $dataObjectHelper
     * @param AccountRedirect $accountRedirect
     * @param Collection $collection
     * @param Logger $logger
     * @param Validator|null $formKeyValidator
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        AccountManagementInterface $accountManagement,
        Address $addressHelper,
        UrlFactory $urlFactory,
        FormFactory $formFactory,
        SubscriberFactory $subscriberFactory,
        RegionInterfaceFactory $regionDataFactory,
        AddressInterfaceFactory $addressDataFactory,
        CustomerInterfaceFactory $customerDataFactory,
        CustomerUrl $customerUrl,
        Registration $registration,
        Escaper $escaper,
        CustomerExtractor $customerExtractor,
        DataObjectHelper $dataObjectHelper,
        AccountRedirect $accountRedirect,
        Collection $collection,
        Logger $logger,
        Validator $formKeyValidator = null)
    {
        parent::__construct(
            $context,
            $customerSession,
            $scopeConfig,
            $storeManager,
            $accountManagement,
            $addressHelper,
            $urlFactory,
            $formFactory,
            $subscriberFactory,
            $regionDataFactory,
            $addressDataFactory,
            $customerDataFactory,
            $customerUrl,
            $registration,
            $escaper,
            $customerExtractor,
            $dataObjectHelper,
            $accountRedirect,
            $formKeyValidator
        );
        $this->collection = $collection;
        $this->validator = $formKeyValidator ?: ObjectManager::getInstance()->get(Validator::class);
        $this->logger = $logger;
    }


    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->session->isLoggedIn() || !$this->registration->isAllowed()) {
            $resultRedirect->setPath('*/*/');
            return $resultRedirect;
        }
        if (!$this->getRequest()->isPost() || !$this->validator->validate($this->getRequest())) {
            $url = $this->urlModel->getUrl('*/*/create', ['_secure' => true]);
            $resultRedirect->setUrl($this->_redirect->error($url));
            return $resultRedirect;
        }
        if ($this->existCustomerDocument()) {
            $url = $this->urlModel->getUrl('*/*/login', ['_secure' => true]);
            $this->messageManager->addErrorMessage(__('Ya hay un cliente registrado con este número de documento'));
            $resultRedirect->setUrl($this->_redirect->error($url));
            return $resultRedirect;
        }
        return parent::execute(); // TODO: Change the autogenerated stub
    }

    protected function existCustomerDocument()
    {
        try {
            $this->logger->info('-----------');
            $this->logger->info(sprintf('Document: %s; Type: %s', $this->getRequest()->getParam('document_number'), $this->getRequest()->getParam('document_type')));
            $collection = $this->collection->addAttributeToSelect('*')
                ->addAttributeToFilter('document_number', $this->getRequest()->getParam('document_number'))
                ->addAttributeToFilter('document_type', $this->getRequest()->getParam('document_type'))
                ->load();
            $this->logger->info(sprintf('Count: %s', $collection->count()));
            $this->logger->info('-----------');
            return $collection->count() > 0;
        } catch (LocalizedException $e) {
            return true;
        }
    }
}