<?php


namespace Croydon\Clientes\Model\Customer\Attribute\Source;

class CustomerType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => '1', 'label' => __('Croydonista')],
                ['value' => '2', 'label' => __('Web')]
            ];
        }
        return $this->_options;
    }
}
