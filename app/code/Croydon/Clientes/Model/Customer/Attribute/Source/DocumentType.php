<?php


namespace Croydon\Clientes\Model\Customer\Attribute\Source;

class DocumentType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => (string)'', 'label' => __('Seleccione')],
                ['value' => (string)'cc', 'label' => __('Cédula de ciudadanía')],
                ['value' => (string)'ce', 'label' => __('Cédula Extranjera')],
                ['value' => (string)'nit', 'label' => __('NIT')]
            ];
        }
        return $this->_options;
    }
}
