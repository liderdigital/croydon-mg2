<?php


namespace Croydon\Clientes\Plugin\Controller\Account;


use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Phrase;

class CreatePost
{

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * CreatePost constructor.
     * @param Collection $collection
     * @param ManagerInterface $messageManager
     */
    public function __construct(Collection $collection, ManagerInterface $messageManager)
    {
        $this->collection = $collection;
        $this->messageManager = $messageManager;
    }


    /**
     * @param \Magento\Customer\Controller\Account\CreatePost $createPost
     * @throws LocalizedException
     */
    public function beforeExtractAddress(\Magento\Customer\Controller\Account\CreatePost $createPost)
    {
        try {

        } catch (LocalizedException $e) {
            throw new LocalizedException(new Phrase($e->getMessage()));
        }
    }

}