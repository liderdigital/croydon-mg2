<?php


namespace Croydon\Deposit\Block;

use Magento\Framework\View\Asset\Repository as AssetRepository;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Image extends Template
{

    protected $assetRepository;

    /**
     * Image constructor.
     * @param Context $context
     * @param AssetRepository $assetRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        AssetRepository $assetRepository,
        array $data = []
    )
    {
        $this->assetRepository = $assetRepository;
        parent::__construct($context, $data);
    }


    public function getDepositConfig()
    {
        $output['depositImageSrc'] = $this->getViewFileUrl('Croydon_Deposit::images/deposit_logo.png');
        return $output;
    }

    public function getDepositBanks()
    {
        $output['depositImageAvvillas'] = $this->getViewFileUrl('Croydon_Deposit::images/avvillas.jpeg');
        $output['depositImageBbva'] = $this->getViewFileUrl('Croydon_Deposit::images/bbva.jpeg');
        $output['depositImageBogota'] = $this->getViewFileUrl('Croydon_Deposit::images/bogota.jpeg');
        $output['depositImageDavivienda'] = $this->getViewFileUrl('Croydon_Deposit::images/davivienda.jpeg');
        return $output;
    }

    public function getViewFileUrl($fileId, array $params = [])
    {
        $params = array_merge(['_secure' => $this->_request->isSecure()], $params);
        return $this->assetRepository->getUrlWithParams($fileId, $params);
    }

}