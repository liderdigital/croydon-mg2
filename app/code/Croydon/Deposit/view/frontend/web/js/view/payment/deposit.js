define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'deposit',
                component: 'Croydon_Deposit/js/view/payment/method-renderer/deposit-method'
            }
        );
        return Component.extend({});
    }
);