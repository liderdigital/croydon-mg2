<?php
/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 14/05/19
 * Time: 9:45
 */

namespace Croydon\Efecty\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

class CreateOrderObserver implements ObserverInterface
{

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $_coreSession;

    /**
     * CreateOrderObserver constructor.
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     */
    public function __construct(\Magento\Framework\Session\SessionManagerInterface $coreSession)
    {
        $this->_coreSession = $coreSession;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws \Croydon\Servicios\InvokeServiceException
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getOrder();
        if (!$this->_coreSession->isSessionExists()) {
            $this->_coreSession->start();
        }
        $this->_coreSession->setData('order_efecty_payment', $order->getId());
    }
}