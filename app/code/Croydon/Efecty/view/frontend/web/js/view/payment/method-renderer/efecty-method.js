/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/action/place-order',
        'mage/url',
        'Magento_Checkout/js/model/totals',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/model/quote',
    ],
    function ($, Component, customer, placeOrderAction, urlBuilder, totals, priceUtils, quote) {
        'use strict';
        return Component.extend({
            defaults: {
                redirectAfterPlaceOrder: false,
                template: 'Croydon_Efecty/payment/efecty'
            },
            total: 0,
            totals: quote.getTotals(),
            placeOrderHandler: null,
            efectyImageSrc: window.populateEfecty.efectyImageSrc,
            getData: function () {
                return {
                    "method": this.item.method,
                    "additional_data": this.getAdditionalData()
                };
            },
            getAdditionalData: function () {
                return null;
            },
            getInstructions: function () {
                return null;
            },
            placeOrder: function (data, event) {
                if (event) {
                    event.preventDefault();
                }
                var self = this,
                    placeOrder,
                    emailValidationResult = customer.isLoggedIn(),
                    loginFormSelector = 'form[data-role=email-with-possible-login]';
                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }
                if (emailValidationResult && this.validate()) {
                    this.isPlaceOrderActionAllowed(false);
                    placeOrder = placeOrderAction(this.getData(), this.redirectAfterPlaceOrder);
                    $.when(placeOrder).done(function () {
                        $.mage.redirect(urlBuilder.build('efecty'));
                    }).fail(function () {
                        self.isPlaceOrderActionAllowed(true);
                    });
                    return true;
                }
                return false;
            },
            setTotal: function (total) {
                this.total = total;
            },
            getTotal: function () {
                var total = totals.getSegment('grand_total');
                return priceUtils.formatPrice(total.value, quote.getBasePriceFormat());
            }
        });
    }
);
