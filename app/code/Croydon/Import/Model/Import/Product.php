<?php


namespace Croydon\Import\Model\Import;


use Magento\Catalog\Model\Config as CatalogConfig;
use Magento\CatalogImportExport\Model\Import\Product\MediaGalleryProcessor;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\ResourceModel\Db\ObjectRelationProcessor;
use Magento\Framework\Model\ResourceModel\Db\TransactionManagerInterface;
use Magento\Framework\Stdlib\DateTime;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Psr\Log\LoggerInterface;

class Product extends \Magento\CatalogImportExport\Model\Import\Product
{

    public function getImagesFromRow(array $rowData)
    {
        /** @var LoggerInterface $logger */
        $logger = ObjectManager::getInstance()->create(LoggerInterface::class);
        $images = [];
        $labels = [];
        foreach ($this->_imagesArrayKeys as $column) {
            if (!empty($rowData[$column])) {
                $images[$column] = array_unique(
                    array_map(
                        'trim',
                        explode($this->getMultipleValueSeparator(), $rowData[$column])
                    )
                );

                if (!empty($rowData[$column . '_label'])) {
                    $labels[$column] = $this->parseMultipleValues($rowData[$column . '_label']);

                    if (count($labels[$column]) > count($images[$column])) {
                        $labels[$column] = array_slice($labels[$column], 0, count($images[$column]));
                    }
                }
            }
        }
        if (isset($images['_media_image']) && is_array($images['_media_image'])) {
            foreach ($images['_media_image'] as $key => $image) {
                if (!$this->checkRemoteFile($image)) {
                    $logger->info(sprintf('Skipping: %s', $image));
                    unset($images['_media_image'][$key]);
                }
            }
        }
        if (isset($images['image'][0])) {
            $logger->info(sprintf('%s: %s', __METHOD__, $images['image'][0]));
            if (!$this->checkRemoteFile($images['image'][0])) {
                //$images['image'][0] = null;
                //$logger->info(sprintf('%s: %s', __METHOD__, $images['image'][0]));
                unset($images['image']);
            }
        }
        if (isset($images['small_image'][0])) {
            if (!$this->checkRemoteFile($images['small_image'][0])) {
                //$images['small_image'][0] = null;
                unset($images['small_image']);
            }
        }
        if (isset($images['thumbnail'][0])) {
            if (!$this->checkRemoteFile($images['thumbnail'][0])) {
                //$images['thumbnail'][0] = null;
                unset($images['thumbnail']);
            }
        }
        //$logger->info(sprintf('Images: %s', json_encode($images['_media_image'])));
        return [$images, $labels];
    }

    private function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $length = (curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD));
        if ($length > 3000000) {
            return false;
        }
        curl_close($ch);
        if ($result !== FALSE) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Parse values from multiple attributes fields
     *
     * @param string $labelRow
     * @return array
     */
    private function parseMultipleValues($labelRow)
    {
        return $this->parseMultiselectValues(
            $labelRow,
            $this->getMultipleValueSeparator()
        );
    }


}