<?php

namespace Croydon\Logged\Observer\Customer;

class Login implements \Magento\Framework\Event\ObserverInterface {

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    private $responseFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(\Magento\Customer\Model\Session $customerSession, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository, \Magento\Framework\App\ResponseFactory $responseFactory, \Magento\Framework\UrlInterface $url) {
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $value = $this->customerRepository->getById($observer->getCustomer()->getId())->getCustomAttribute('customer_type')->getValue();
        if ($value == 1) {
            $this->responseFactory->create()->setRedirect('http://croydonistas.local')->sendResponse();
            die();
        }
        return $this->responseFactory;
    }

}
