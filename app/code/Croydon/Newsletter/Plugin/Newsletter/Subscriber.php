<?php


namespace Croydon\Newsletter\Plugin\Newsletter;

use Magento\Framework\App\Request\Http;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

class Subscriber
{
    /**
     * @var Http
     */
    protected $request;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Subscriber constructor.
     * @param Http $request
     * @param LoggerInterface $logger
     */
    public function __construct(Http $request, LoggerInterface $logger)
    {
        $this->request = $request;
        $this->logger = $logger;
    }

    /**
     * @param $subject
     * @param \Closure $proceed
     * @param $email
     * @return mixed
     * @throws \Exception
     */
    public function aroundSubscribe($subject, \Closure $proceed, $email)
    {
        $this->logger->info('registrando newsletter');
        $result = $proceed($email);
        if ($this->request->isPost() && $this->request->getPost('gender')) {
            $gender = $this->request->getPost('gender');
            $subject->setGender($gender);
            try {
                $subject->save();
                $this->logger->info('Newsletter registerd: ');
            } catch (\Exception $e) {
                $this->logger->info('Error Subscriber: ' . $e->getMessage());
                throw new \Exception($e->getMessage());
            }
        }
        return $result;
    }
}