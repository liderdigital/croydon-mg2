<?php


namespace Croydon\Newsletter\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $table = $setup->getTable('newsletter_subscriber');
        $setup->getConnection()->addColumn(
            $table,
            'gender',
            [
                'type' => Table::TYPE_INTEGER,
                'nullable' => true,
                'comment' => 'Gender'
            ]
        );
        $setup->endSetup();
    }
}