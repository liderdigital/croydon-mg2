<?php


namespace Croydon\Payment\Controller\Post;

use Croydon\Baloto\Model\Payment\Baloto;
use Croydon\Deposit\Model\Payment\Deposit;
use Croydon\Efecty\Model\Payment\Efecty;
use Croydon\Payulatam\Model\Payulatam;
use Croydon\Pse\Model\Payment\Pse;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\Encryption\Encryptor;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Framework\Phrase;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class Index extends Action
{

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $order;
    protected $customerFactory;
    protected $storeManager;
    protected $quote;
    protected $quoteRepository;
    /**
     * @var \Magento\Customer\Api\Data\CustomerInterface
     */
    protected $customer;
    protected $encryptor;
    protected $customerRegistry;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var ResourceConnection
     */
    protected $resource;


    public function __construct(
        Context $context,
        CustomerRepositoryInterface $customerRepository,
        OrderRepositoryInterface $orderRepository,
        Validator $formKeyValidator,
        CustomerInterfaceFactory $customerFactory,
        StoreManagerInterface $storeManager,
        CartRepositoryInterface $quoteRepository,
        Encryptor $encryptor,
        CustomerRegistry $customerRegistry,
        TransactionFactory $transactionFactory,
        ResourceConnection $resource
    )
    {
        parent::__construct($context);
        $this->customerRepository = $customerRepository;
        $this->orderRepository = $orderRepository;
        $this->formKeyValidator = $formKeyValidator;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->quoteRepository = $quoteRepository;
        $this->encryptor = $encryptor;
        $this->customerRegistry = $customerRegistry;
        $this->logger = $this->_objectManager->get(LoggerInterface::class);
        $this->transactionFactory = $transactionFactory;
        $this->resource = $resource;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if ($this->formKeyValidator->validate($this->getRequest())) {
            if ($this->validatePassword()) {
                $this->order = $this->orderRepository->get($this->getRequest()->getParam('orderId'));
                $connection = $this->resource->getConnection(ResourceConnection::DEFAULT_CONNECTION);
                try {
                    $connection->beginTransaction();
                    $this->createCustomer();
                    $this->updateOrder();
                    $this->savePassword();
                    $connection->commit();
                    $this->messageManager->addSuccessMessage('Cliente registrado correctamente');
                } catch (InputException $e) {
                    $connection->rollBack();
                    $this->logger->error($e->getMessage());
                    $this->messageManager->addErrorMessage(__('Ha ocurrido un error registrando la contraseña'));
                } catch (InputMismatchException $e) {
                    $connection->rollBack();
                    $this->logger->error($e->getMessage());
                    $this->messageManager->addErrorMessage(__('Ha ocurrido un error registrando la contraseña'));
                } catch (LocalizedException $e) {
                    $connection->rollBack();
                    $this->logger->error($e->getMessage());
                    $this->messageManager->addErrorMessage(__('Ha ocurrido un error registrando la contraseña'));
                }
            } else {
                $this->messageManager->addErrorMessage(__('Las contraseñas ingresadas no coinciden'));
            }
        } else {
            $this->messageManager->addErrorMessage(__('Ha ocurrido un error, por favor vuelva a intentarlo'));
        };
        $resultRedirect->setUrl($this->_redirect->getRedirectUrl($this->getRoute()));
        return $resultRedirect;
    }


    /**
     * @return bool
     */
    private function validatePassword()
    {
        return $this->getRequest()->getParam('password') == $this->getRequest()->getParam('comfirmPassword');
    }

    /**
     *
     * @throws InputException
     * @throws InputMismatchException
     * @throws LocalizedException
     */
    private function createCustomer()
    {
        $firstName = $this->order->getShippingAddress()->getFirstname();
        $email = $this->order->getShippingAddress()->getEmail();
        $lastName = $this->order->getShippingAddress()->getLastname();
        $password = $this->getRequest()->getParam('password');
        $this->customer = $this->customerFactory->create();
        $document = $this->order->getShippingAddress()->getDocument();
        $type = $this->order->getShippingAddress()->getType();
        $gender = $this->order->getShippingAddress()->getGender();
        $this->customer->setEmail($email);
        $this->customer->setDob('10/10/2019');
        $this->customer->setFirstname($firstName);
        $this->customer->setLastname($lastName);
        $this->customer->setGender($gender);
        $this->customer->setCustomAttribute('document_number', $document);
        $this->customer->setCustomAttribute('document_type', $type);
        $this->customer->setCustomAttribute('customer_type', 1);
        try {
            $this->customer = $this->customerRepository->save($this->customer);
            $this->logger->info(sprintf('Customer: %s', $this->customer->getId()));
            return true;
        } catch (InputException $e) {
            throw new InputException(new Phrase($e->getMessage()));
        } catch (InputMismatchException $e) {
            throw new InputMismatchException(new Phrase($e->getMessage()));
        } catch (LocalizedException $e) {
            throw new LocalizedException(new Phrase($e->getMessage()));
        }
    }

    /**
     *
     * @throws NoSuchEntityException
     */
    private function updateOrder()
    {
        $this->order->setCustomerIsGuest(0);
        try {
            $this->quote = $this->quoteRepository->get($this->order->getQuoteId());
            $this->quote->setCustomer($this->customer);
            $this->order->setCustomer($this->customer);
            $this->order->setCustomerFirstname($this->customer->getFirstname());
            $this->order->setCustomerLastname($this->customer->getLastname());
            $this->quoteRepository->save($this->quote);
            $this->orderRepository->save($this->order);
            return true;
        } catch (NoSuchEntityException $e) {
            throw new NoSuchEntityException(new Phrase($e->getMessage()));
        }
    }


    /**
     *
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    private function savePassword()
    {
        try {
            $customerId = $this->customer->getId();
            $customer = $this->customerRepository->getById($customerId);
            $customerSecure = $this->customerRegistry->retrieveSecureData($customerId);
            $customerSecure->setRpToken(null);
            $customerSecure->setRpTokenCreatedAt(null);
            $customerSecure->setPasswordHash($this->encryptor->getHash($this->getRequest()->getParam('password'), true));
            $this->customerRepository->save($customer);
        } catch (NoSuchEntityException $e) {
            throw new NoSuchEntityException(new Phrase($e->getMessage()));
        } catch (LocalizedException $e) {
            throw new LocalizedException(new Phrase($e->getMessage()));
        }
    }

    public function getRoute()
    {
        switch ($this->order->getPayment()->getMethod()) {
            case Payulatam::CODE:
                return 'payulatam/onepage/success';
            case Baloto::CODE:
                return 'baloto';
            case Efecty::CODE:
                return 'efecty';
            case Pse::CODE:
                return 'pse/onepage/success';
            case Deposit::CODE:
                return 'deposit';
        }
    }
}