<?php


namespace Croydon\Payulatam\Block;

use Magento\Framework\View\Asset\Repository as AssetRepository;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Image extends Template
{

    protected $assetRepository;

    /**
     * Image constructor.
     * @param Context $context
     * @param AssetRepository $assetRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        AssetRepository $assetRepository,
        array $data = []
    )
    {
        $this->assetRepository = $assetRepository;
        parent::__construct($context, $data);
    }


    public function getPayulatamConfig()
    {
        $output['payulatamImageSrc'] = $this->getViewFileUrl('Croydon_Payulatam::images/payulatam_logo.png');

        return $output;
    }

    public function getViewFileUrl($fileId, array $params = [])
    {
        $params = array_merge(['_secure' => $this->_request->isSecure()], $params);
        return $this->assetRepository->getUrlWithParams($fileId, $params);
    }

}