<?php


namespace Croydon\Payulatam\Controller\Onepage;


use Magento\Checkout\Controller\Onepage\Failure;
use Magento\Framework\App\ResponseInterface;

//class Error extends Failure
class Error extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    /** @var \Magento\Framework\Registry $_coreRegistry */
    protected $_coreRegistry;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        return $this->_pageFactory->create();
    }
}