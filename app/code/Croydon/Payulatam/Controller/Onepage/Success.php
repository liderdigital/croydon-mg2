<?php


namespace Croydon\Payulatam\Controller\Onepage;


use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\View\Result\PageFactory;

//class Success extends \Magento\Checkout\Controller\Onepage\Success
class Success extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    /** @var \Magento\Framework\Registry $_coreRegistry */
    protected $_coreRegistry;

    protected $_modelCart;
    protected $_checkoutSession;

    public function __construct(Context $context, PageFactory $pageFactory, CheckoutSession $checkoutSession, Cart $modelCart)
    {
        parent::__construct($context);
        $this->_pageFactory = $pageFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_modelCart = $modelCart;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $this->_modelCart->truncate()->save();
        return $this->_pageFactory->create();
    }
}