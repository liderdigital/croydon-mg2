<?php


namespace Croydon\Payulatam\Controller\Onepage;


use Croydon\Payulatam\Helper\Api\CaptureInterface;
use Croydon\Payulatam\Helper\Api\Response\Json;
use Croydon\Payulatam\Helper\Api\Response\PayuResponseCodeException;
use Croydon\Payulatam\Helper\Api\Response\PayuResponseException;
use Croydon\Payulatam\Helper\Api\Response\PayuStateException;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Test extends Action
{

    protected $orderRepository;
    protected $capture;

    public function __construct(
        Context $context,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        CaptureInterface $capture
    )
    {
        parent::__construct($context);
        $this->orderRepository = $orderRepository;
        $this->capture = $capture;
    }


    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     * @throws \ReflectionException
     * @throws PayuResponseException
     * @throws PayuResponseCodeException
     * @throws PayuStateException
     */
    public function execute()
    {
        /** @var \Magento\Sales\Model\Order\Interceptor */
        $order = $this->orderRepository->get(1);
        $this->capture->setOrder($order);
        //$this->capture->setPayment($payment);
        $jsonResponse = $this->capture->sendRequest();
        //$this->_logger->info($jsonResponse);
        $xml = new Json($jsonResponse);
        try {
            $xml->processResponse();
            $xml->processResponseCode();
            $xml->processState();
            echo 'Funciono';
        } catch (PayuResponseException $e) {
            var_dump($e);
            //$this->messageManager->addErrorMessage($e->getMessage());
            //$this->_logger->error('RESPONSE: ' . $e->getMessage());
            //throw new PayuResponseException($e->getMessage());
        } catch (PayuResponseCodeException $e) {
            var_dump($e);
            //$this->messageManager->addErrorMessage($e->getMessage());
            //$this->_logger->error('CODE: ' . $e->getPayuCode());
            //throw new PayuResponseCodeException($e->getMessage(), $e->getPayuCode());
        } catch (PayuStateException $e) {
            var_dump($e);
            //$this->messageManager->addErrorMessage($e->getMessage());
            //$this->_logger->error('STATE: ' . $e->getPayuCode());

            //throw new PayuStateException($e->getMessage(), $e->getPayuCode());
        }
        var_dump($jsonResponse);
    }
}