<?php


namespace Croydon\Payulatam\Helper\Api;


use Croydon\Payulatam\Helper\Data;
use Croydon\Payulatam\Logger\Logger;
use Magento\Customer\Model\Session;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\HTTP\Header;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;

class Capture implements CaptureInterface
{


    /**
     * @var PayuCustomer
     */
    protected $payuCustomer;

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var \Magento\Payment\Model\InfoInterface
     */
    protected $payment;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @var Header
     */
    protected $httpHeader;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Capture constructor.
     * @param PayuCustomer $payuCustomer
     * @param Data $data
     * @param RequestInterface $request
     * @param CookieManagerInterface $cookieManager
     * @param Session $customerSession
     * @param RemoteAddress $remoteAddress
     * @param Header $httpHeader
     * @param Logger $logger
     */
    public function __construct(
        PayuCustomer $payuCustomer,
        Data $data,
        RequestInterface $request,
        CookieManagerInterface $cookieManager,
        Session $customerSession,
        RemoteAddress $remoteAddress,
        Header $httpHeader,
        Logger $logger
    )
    {
        $this->payuCustomer = $payuCustomer;
        $this->data = $data;
        $this->request = $request;
        $this->cookieManager = $cookieManager;
        $this->customerSession = $customerSession;
        $this->remoteAddress = $remoteAddress;
        $this->httpHeader = $httpHeader;
        $this->logger = $logger;
        $this->payuCustomer->isTest = $this->data->isEnvironment();
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return \Magento\Payment\Model\InfoInterface
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }


    protected function _setDataCreditCard()
    {
        $expirationDate = sprintf('%s/%s', $this->payment->getCcExpYear(), str_pad($this->payment->getCcExpMonth(), 2, '0', STR_PAD_LEFT));
        $name = 'APPROVED';
        $number = $this->payment->getCcNumber();
        $securityCode = $this->payment->getCcCid();
        $installmentsNumber = 1;
        $this->payuCustomer->setDataCreditCard($expirationDate, $name, $number, $securityCode, $installmentsNumber);
    }

    protected function _setShippingAddress()
    {
        $state = $this->order->getShippingAddress()->getRegion();
        $city = $this->order->getShippingAddress()->getCity();
        $country = 'CO';
        $phone = $this->order->getShippingAddress()->getTelephone();
        $postalCode = !is_null($this->order->getShippingAddress()->getPostcode()) ? $this->order->getShippingAddress()->getPostcode() : '000000';
        $street1 = $this->order->getShippingAddress()->getStreet()[0];
        $street2 = isset($this->order->getShippingAddress()->getStreet()[1]) ? $this->order->getShippingAddress()->getStreet()[1] : '';
        $this->payuCustomer->setShippingAddress($state, $city, $country, $phone, $postalCode, $street1, $street2);
    }

    protected function _setBillingAddress()
    {
        $state = $this->order->getBillingAddress()->getRegion();
        $city = $this->order->getBillingAddress()->getCity();
        $country = 'CO';
        $phone = $this->order->getBillingAddress()->getTelephone();
        $postalCode = !is_null($this->order->getShippingAddress()->getPostcode()) ? $this->order->getShippingAddress()->getPostcode() : '000000';
        $street1 = $this->order->getBillingAddress()->getStreet()[0];
        $street2 = isset($this->order->getBillingAddress()->getStreet()[1]) ? $this->order->getBillingAddress()->getStreet()[1] : '';
        $this->payuCustomer->setBillingAddress($state, $city, $country, $phone, $postalCode, $street1, $street2);
    }

    protected function _setBuyer()
    {
        $phone = $this->order->getShippingAddress()->getTelephone();
        $dniNumber = 1072366680;
        $emailAddress = $this->order->getShippingAddress()->getEmail();
        $fullName = $this->order->getShippingAddress()->getName();
        $merchantBuyerId = !is_null($this->order->getShippingAddress()->getCustomerId()) ? $this->order->getShippingAddress()->getCustomerId() : 0;
        $this->payuCustomer->setBuyer($phone, $dniNumber, $emailAddress, $fullName, $merchantBuyerId);
    }

    protected function _setPayer()
    {
        $phone = $this->order->getShippingAddress()->getTelephone();
        $dniNumber = 1072366680;
        $emailAddress = $this->order->getShippingAddress()->getEmail();
        $fullName = $this->order->getShippingAddress()->getName();
        $merchantBuyerId = !is_null($this->order->getShippingAddress()->getCustomerId()) ? $this->order->getShippingAddress()->getCustomerId() : 0;
        $this->payuCustomer->setPayer($phone, $dniNumber, $emailAddress, $fullName, $merchantBuyerId);
    }

    protected function _setValues()
    {
        $currency = $this->order->getOrderCurrencyCode();
        $tax = $this->order->getTaxAmount();
        $taxReturnBase = $this->order->getGrandTotal() - $tax;
        $total = $this->order->getGrandTotal();
        $this->payuCustomer->setValues($currency, $tax, $taxReturnBase, $total);
    }

    protected function _setOrder()
    {
        /** @var Quote $quote */
        $quote = ObjectManager::getInstance()->create(Quote::class);
        $quote = $quote->load($this->order->getQuoteId());
        $this->logger->info('Payu Order id: ' . $quote->getAuroraInvoice());
        $lenguage = 'es';
        $accountId = $this->data->getAccountId();
        $description = $this->order->getItems()[0]->getName();
        $notifyUrl = '';
        $referenceCode = $quote->getAuroraInvoice();
        $apiKey = $this->data->getApiKey();
        $merchantId = $this->data->getMerchantId();
        $this->payuCustomer->setOrder($lenguage, $accountId, $description, $notifyUrl, $referenceCode, $apiKey, $merchantId);
    }

    protected function _setTransaction()
    {
        $cookie = md5($this->customerSession->getSessionId());
        $deviceSessionId = md5($this->customerSession->getSessionId());
        $ipAddress = $this->remoteAddress->getRemoteAddress();
        $paymentCountry = 'CO';
        $paymentMethod = $this->getCcType($this->payment->getCcType());
        $type = 'AUTHORIZATION_AND_CAPTURE';
        $userAgent = $this->httpHeader->getHttpUserAgent();
        $this->payuCustomer->setTransaction($cookie, $deviceSessionId, $ipAddress, $paymentCountry, $paymentMethod, $type, $userAgent);
    }

    protected function _setApiPayu()
    {
        $command = 'SUBMIT_TRANSACTION';
        $lenguage = 'es';
        $apiKey = $this->data->getApiKey();
        $apiLogin = $this->data->getApiLogin();
        $test = $this->data->isEnvironment();
        $this->payuCustomer->setApiPayu($command, $lenguage, $apiKey, $apiLogin, $test);
    }

    /**
     *
     * @return bool|string
     */
    public function sendRequest()
    {
        $this->_setDataCreditCard();
        $this->_setShippingAddress();
        $this->_setBillingAddress();
        $this->_setBuyer();
        $this->_setPayer();
        $this->_setValues();
        $this->_setOrder();
        $this->_setTransaction();
        $this->_setApiPayu();
        return $this->payuCustomer->request();
    }

    /**
     * @return string
     */
    public function getContentRequest()
    {
        return $this->payuCustomer->getContentRequest();
    }


    private function getCcType($ccType)
    {
        $array = array(
            'MC' => 'MASTERCARD',
            'VI' => 'VISA',
            'DN' => 'DINERS',
            'AE' => 'AMEX'
        );
        return $array[$ccType];
    }
}