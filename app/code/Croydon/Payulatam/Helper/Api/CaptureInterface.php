<?php


namespace Croydon\Payulatam\Helper\Api;


use Magento\Sales\Model\Order;

interface CaptureInterface
{
    /**
     * @return Order
     */
    public function getOrder();

    /**
     * @param Order $order
     */
    public function setOrder($order);

    /**
     * @return \Magento\Payment\Model\InfoInterface
     */
    public function getPayment();

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     */
    public function setPayment($payment);


    /**
     *
     * @throws \ReflectionException
     */
    public function sendRequest();
}