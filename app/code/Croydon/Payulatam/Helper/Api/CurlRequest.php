<?php

/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 10/07/19
 * Time: 8:28
 */

namespace Croydon\Payulatam\Helper\Api;

use Croydon\Payulatam\Helper\Service\AbstractEntityService;
use Psr\Log\LoggerInterface;

class CurlRequest
{

    private $ch;

    /**
     * @var string
     */
    private $contentRequest;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CurlRequest constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    /**
     * Este metodo convierte el contenido a string para consumir el servicio
     * @param AbstractEntityService $contentRequest
     */
    public function prepareContent(AbstractEntityService $contentRequest)
    {
        if (is_object($contentRequest)) {
            $this->contentRequest = json_encode($contentRequest);
        } else {
            $this->contentRequest = $contentRequest;
        }
    }

//'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi'

    /**
     * Prepara el recurso curl
     * @param string $url
     * @return bool|string
     */
    public function request(string $url)
    {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($this->contentRequest),
                'Accept: application/json'
            )
        );
        curl_setopt($this->ch, CURLOPT_POST, 1); // set post data to true
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->contentRequest);   // post data
        $json = curl_exec($this->ch);
        curl_close($this->ch);
        return $json;
    }

    /**
     * Este metodo convierte a array el contenido de una variable
     * @param $class
     * @return array
     * @throws \ReflectionException
     */
    private function toJson(AbstractEntityService $class)
    {
        $data = array();
        foreach ($class->toArray() as $item => $value) {
            if (is_object($value)) {
                $data[$item] = $this->toJson($value);
            } else {
                $data[$item] = $value;
            }
        }
        return $data;
    }

}
