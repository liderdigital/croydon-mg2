<?php
/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 10/07/19
 * Time: 8:40
 */

namespace Croydon\Payulatam\Helper\Api;


use Croydon\Payulatam\Helper\Service\AdditionalValues;
use Croydon\Payulatam\Helper\Service\ApiPayu;
use Croydon\Payulatam\Helper\Service\BillingAddress;
use Croydon\Payulatam\Helper\Service\Buyer;
use Croydon\Payulatam\Helper\Service\CreditCard;
use Croydon\Payulatam\Helper\Service\ExtraParameters;
use Croydon\Payulatam\Helper\Service\Order;
use Croydon\Payulatam\Helper\Service\Payer;
use Croydon\Payulatam\Helper\Service\ShippingAddress;
use Croydon\Payulatam\Helper\Service\Transaction;
use Psr\Log\LoggerInterface;

class PayuCustomer
{

    /**
     * @var CurlRequest
     */
    private $curlRequest;

    /**
     * @var bool
     */
    public $isTest;

    /**
     * @var string
     */
    private static $urlPayuTest = 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi';
    /**
     * @var string
     */
    private static $urlPayuProduction = 'https://api.payulatam.com/payments-api/4.0/service.cgi';

    /**
     * @var ApiPayu
     */
    private $apiPayu;

    /**
     * @var CreditCard
     */
    private $creditCard;

    /**
     * @var ExtraParameters
     */
    private $extraParameters;

    /**
     * @var ShippingAddress
     */
    private $shippingAddress;

    /**
     * @var BillingAddress
     */
    private $billingAddress;

    /**
     * @var Buyer
     */
    private $buyer;
    /**
     * @var Payer
     */
    private $payer;

    /**
     * @var AdditionalValues
     */
    private $additionalValues;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PayuCustomer constructor.
     * @param LoggerInterface $logger
     * @param CurlRequest $curlRequest
     * @param bool $isTest
     */
    public function __construct(LoggerInterface $logger, CurlRequest $curlRequest, $isTest = true)
    {
        $this->isTest = $isTest;
        $this->curlRequest = $curlRequest;
        $this->apiPayu = new ApiPayu();
        $this->logger = $logger;
    }


    /**
     * @param string $expirationDate format YYYY/MM example 2020/12
     * @param string $name
     * @param string $number
     * @param string $securityCode
     * @param int $installmentsNumber Número de cuotas
     */
    public function setDataCreditCard(string $expirationDate, string $name, string $number, string $securityCode, int $installmentsNumber)
    {
        $this->creditCard = new CreditCard();
        $this->creditCard->setExpirationDate($expirationDate);
        $this->creditCard->setName($name);
        $this->creditCard->setNumber($number);
        $this->creditCard->setSecurityCode($securityCode);
        $this->extraParameters = new ExtraParameters();
        $this->extraParameters->setINSTALLMENTSNUMBER($installmentsNumber);
    }

    /**
     * @param string $state
     * @param string $city
     * @param string $country
     * @param string $phone
     * @param string $postalCode
     * @param string $street1
     * @param string $street2
     */
    public function setShippingAddress(string $state, string $city, string $country, string $phone, string $postalCode, string $street1, string $street2)
    {
        $this->shippingAddress = new ShippingAddress();
        $this->shippingAddress->setState($state);
        $this->shippingAddress->setCity($city);
        $this->shippingAddress->setCountry($country);
        $this->shippingAddress->setPhone($phone);
        $this->shippingAddress->setPostalCode($postalCode);
        $this->shippingAddress->setStreet1($street1);
        $this->shippingAddress->setStreet2($street2);
    }

    /**
     * @param string $state
     * @param string $city
     * @param string $country
     * @param string $phone
     * @param string $postalCode
     * @param string $street1
     * @param string $street2
     */
    public function setBillingAddress(string $state, string $city, string $country, string $phone, string $postalCode, string $street1, string $street2)
    {
        $this->billingAddress = new BillingAddress();
        $this->billingAddress->setState($state);
        $this->billingAddress->setCity($city);
        $this->billingAddress->setCountry($country);
        $this->billingAddress->setPhone($phone);
        $this->billingAddress->setPostalCode($postalCode);
        $this->billingAddress->setStreet1($street1);
        $this->billingAddress->setStreet2($street2);
    }

    /**
     * @param string $contactPhone
     * @param string $dniNumber
     * @param string $emailAddress
     * @param string $fullName
     * @param int $merchantBuyerId
     */
    public function setBuyer(string $contactPhone, string $dniNumber, string $emailAddress, string $fullName, int $merchantBuyerId)
    {
        $this->buyer = new Buyer();
        $this->buyer->setShippingAddress($this->shippingAddress);
        $this->buyer->setContactPhone($contactPhone);
        $this->buyer->setDniNumber($dniNumber);
        $this->buyer->setEmailAddress($emailAddress);
        $this->buyer->setFullName($fullName);
        $this->buyer->setMerchantBuyerId($merchantBuyerId);
    }

    /**
     * @param string $contactPhone
     * @param string $dniNumber
     * @param string $emailAddress
     * @param string $fullName
     * @param int $merchantBuyerId
     */
    public function setPayer(string $contactPhone, string $dniNumber, string $emailAddress, string $fullName, int $merchantBuyerId)
    {
        $this->payer = new Payer();
        $this->payer->setBillingAddress($this->billingAddress);
        $this->payer->setContactPhone($contactPhone);
        $this->payer->setDniNumber($dniNumber);
        $this->payer->setEmailAddress($emailAddress);
        $this->payer->setFullName($fullName);
        $this->payer->setMerchantPayerId($merchantBuyerId);
    }

    /**
     * @param string $currency
     * @param float $tax
     * @param float $taxReturnBase
     * @param float $total
     */
    public function setValues(string $currency, float $tax, float $taxReturnBase, float $total)
    {
        $this->additionalValues = new AdditionalValues();
        if ($tax > 0) {
            $txTax = new \Croydon\Payulatam\Helper\Service\TxTax();
            $txTax->setValue($tax);
            $txTax->setCurrency($currency);
            $this->additionalValues->setTXTAX($txTax);
            $txTaxReturnBase = new \Croydon\Payulatam\Helper\Service\TxTaxReturnBase();
            $txTaxReturnBase->setCurrency($currency);
            $txTaxReturnBase->setValue($taxReturnBase);
            $this->additionalValues->setTXTAXRETURNBASE($txTaxReturnBase);
        }
        $txValue = new \Croydon\Payulatam\Helper\Service\TxValue();
        $txValue->setValue($total);
        $txValue->setCurrency($currency);
        $this->additionalValues->setTXVALUE($txValue);
    }

    /**
     * @param string $language
     * @param string $accountId
     * @param string $description
     * @param string $notifyUrl
     * @param string $referenceCode
     * @param string $apiKey
     * @param string $merchantId
     */
    public function setOrder(string $language, string $accountId, string $description, string $notifyUrl, string $referenceCode, string $apiKey, string $merchantId)
    {
        $this->order = new Order();
        $this->order->setLanguage($language);
        $this->order->setAccountId($accountId);
        $this->order->setAdditionalValues($this->additionalValues);
        $this->order->setBuyer($this->buyer);
        $this->order->setDescription($description);
        $this->order->setNotifyUrl($notifyUrl);
        $this->order->setReferenceCode($referenceCode);
        $this->order->setShippingAddress($this->shippingAddress);
        $this->order->setSignature($this->getSignature($apiKey, $merchantId, $this->order->getReferenceCode(), $this->additionalValues->getTXVALUE()->getValue(), $this->additionalValues->getTXVALUE()->getCurrency()));
    }


    /**
     * @param string $cookie
     * @param string $deviceSessionId
     * @param string $ipAddress
     * @param string $paymentCountry
     * @param string $paymentMethod
     * @param string $type
     * @param string $userAgent
     */
    public function setTransaction(string $cookie, string $deviceSessionId, string $ipAddress, string $paymentCountry, string $paymentMethod, string $type, string $userAgent)
    {
        $this->transaction = new Transaction();
        $this->transaction->setCookie($cookie);
        $this->transaction->setCreditCard($this->creditCard);
        $this->transaction->setDeviceSessionId($deviceSessionId);
        $this->transaction->setExtraParameters($this->extraParameters);
        $this->transaction->setIpAddress($ipAddress);
        $this->transaction->setOrder($this->order);
        $this->transaction->setPayer($this->payer);
        $this->transaction->setPaymentCountry($paymentCountry);
        $this->transaction->setPaymentMethod($paymentMethod);
        $this->transaction->setType($type);
        $this->transaction->setUserAgent($userAgent);
    }

    /**
     * @param string $command
     * @param string $language
     * @param string $apiKey
     * @param string $apiLogin
     * @param bool $test
     */
    public function setApiPayu(string $command, string $language, string $apiKey, string $apiLogin, bool $test)
    {
        $this->apiPayu = new ApiPayu();
        $this->apiPayu->setCommand($command);
        $this->apiPayu->setLanguage($language);
        $merchant = new \Croydon\Payulatam\Helper\Service\Merchant();
        $merchant->setApiKey($apiKey);
        $merchant->setApiLogin($apiLogin);
        $this->apiPayu->setMerchant($merchant);
        $this->apiPayu->setTransaction($this->transaction);
        $this->apiPayu->setTest($test);
    }

    /**
     * @param string $apiKey
     * @param string $merchantId
     * @param string $referenceCode
     * @param float $txValue
     * @param string $currency
     * @return string
     */
    private function getSignature(string $apiKey, string $merchantId, string $referenceCode, float $txValue, string $currency)
    {
        return md5(sprintf('%s~%s~%s~%s~%s', $apiKey, $merchantId, $referenceCode, $txValue, $currency));
    }


    /**
     * Realiza la peticion a payu
     * @return bool|string
     */
    public function request()
    {
        $this->curlRequest->prepareContent($this->apiPayu);
        return $this->curlRequest->request($this->isTest ? self::$urlPayuTest : self::$urlPayuProduction);
    }

    /**
     * @return string
     */
    public function getContentRequest()
    {
        return $this->curlRequest->getContentRequest();
    }


}