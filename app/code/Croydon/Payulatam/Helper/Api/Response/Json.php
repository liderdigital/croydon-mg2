<?php


namespace Croydon\Payulatam\Helper\Api\Response;


class Json
{

    /**
     * @var \stdClass
     */
    private $response;

    /**
     * Json constructor.
     * @param string $response
     */
    public function __construct($response)
    {
        $this->response = json_decode($response);
    }

    /**
     *
     * @throws PayuResponseException
     */
    public function processResponse()
    {
        $code = $this->response->code;
        if ($code != PayuResponseVars::SUCCESS) {
            throw new PayuResponseException(
                $this->response->error
            );
        }

    }

    /**
     * @throws PayuResponseCodeException
     */
    public function processResponseCode()
    {
        $responseCode = $this->response->transactionResponse->responseCode;
        if ($responseCode != PayuResponseVars::RESPONSE_CODE_APPROVED) {
            throw new PayuResponseCodeException(
                PayuResponseVars::$responseCodeMessages[$responseCode],
                $responseCode
            );
        }
    }

    /**
     *
     * @throws PayuStateException
     */
    public function processState()
    {
        $state = $this->response->transactionResponse->state;
        if ($state != PayuResponseVars::STATE_APPROVED && $state != PayuResponseVars::STATE_PENDING) {
            $responseMessage = $this->response->transactionResponse->responseMessage;
            throw new PayuStateException(
                $responseMessage,
                $state
            );
        }
    }

}