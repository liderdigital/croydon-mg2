<?php


namespace Croydon\Payulatam\Helper\Api\Response;


class PayuPaymentException extends \Exception
{


    /**
     * @var string
     */
    protected $payuCode;

    public function __construct($message = "", $payuCode = '', \Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
        $this->payuCode = $payuCode;
    }

    /**
     * @return string
     */
    public function getPayuCode(): string
    {
        return $this->payuCode;
    }

    /**
     * @param string $payuCode
     */
    public function setPayuCode(string $payuCode): void
    {
        $this->payuCode = $payuCode;
    }

}