<?php


namespace Croydon\Payulatam\Helper\Api\Response;


class PayuResponseCodeException extends PayuPaymentException
{
    public function __construct($message = "", $payuCode = '', \Throwable $previous = null)
    {
        parent::__construct($message, $payuCode, $previous);
    }
}