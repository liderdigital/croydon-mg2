<?php


namespace Croydon\Payulatam\Helper\Api\Response;


class PayuResponseVars
{


    /**
     * @var string
     */
    const SUCCESS = 'SUCCESS';

    /**
     * @var string
     */
    const ERROR = 'ERROR';

    /**
     *
     * @var string
     */
    const STATE_DECLINED = 'DECLINED';
    /**
     *
     * @var string La transacción fue aprobada.
     */
    const STATE_APPROVED = 'APPROVED';

    /**
     *
     * @var string Ocurrió un error general.
     */
    const STATE_ERROR = 'ERROR';

    /**
     * @var string
     */
    const STATE_PENDING = 'PENDING';


    /**
     *
     * @var string La transacción fue aprobada.
     */
    const RESPONSE_CODE_APPROVED = 'APPROVED';

    /**
     * @var string La transacción fue rechazada por el sistema anti-fraude.
     */
    const RESPONSE_CODE_ANTIFRAUD_REJECTED = 'ANTIFRAUD_REJECTED';
    /**
     * @var string La red financiera rechazó la transacción.
     */
    const RESPONSE_CODE_PAYMENT_NETWORK_REJECTED = 'PAYMENT_NETWORK_REJECTED';
    /**
     * @var string La transacción fue declinada por el banco o por la red financiera debido a un error.
     */
    const RESPONSE_CODE_ENTITY_DECLINED = 'ENTITY_DECLINED';
    /**
     * @var string Ocurrió un error en el sistema intentando procesar el pago.
     */
    const RESPONSE_CODE_INTERNAL_PAYMENT_PROVIDER_ERROR = 'INTERNAL_PAYMENT_PROVIDER_ERROR';
    /**
     * @var string El proveedor de pagos no se encontraba activo.
     */
    const RESPONSE_CODE_INACTIVE_PAYMENT_PROVIDER = 'INACTIVE_PAYMENT_PROVIDER';
    /**
     * @var string La red financiera reportó un error en la autenticación.
     */
    const RESPONSE_CODE_DIGITAL_CERTIFICATE_NOT_FOUND = 'DIGITAL_CERTIFICATE_NOT_FOUND';
    /**
     * @var string El código de seguridad o la fecha de expiración estaba inválido.
     */
    const RESPONSE_CODE_INVALID_EXPIRATION_DATE_OR_SECURITY_CODE = 'INVALID_EXPIRATION_DATE_OR_ SECURITY_CODE';
    /**
     * @var string Tipo de respuesta no válida. La entidad aprobó parcialmente la transacción y debe ser cancelada automáticamente por el sistema.
     */
    const RESPONSE_CODE_INVALID_RESPONSE_PARTIAL_APPROVAL = 'INVALID_RESPONSE_PARTIAL_APPROVAL';
    /**
     * @var string La cuenta no tenía fondos suficientes.
     */
    const RESPONSE_CODE_INSUFFICIENT_FUNDS = 'INSUFFICIENT_FUNDS';
    /**
     * @var string La tarjeta de crédito no estaba autorizada para transacciones por Internet.
     */
    const RESPONSE_CODE_CREDIT_CARD_NOT_AUTHORIZED_FOR_INTERNET_TRANSACTIONS = 'CREDIT_CARD_NOT_AUTHORIZED _FOR_INTERNET_TRANSACTIONS';
    /**
     * @var string La red financiera reportó que la transacción fue inválida.
     */
    const RESPONSE_CODE_INVALID_TRANSACTION = 'INVALID_TRANSACTION';
    /**
     * @var string La tarjeta es inválida.
     */
    const RESPONSE_CODE_INVALID_CARD = 'INVALID_CARD';
    /**
     * @var string La tarjeta ya expiró.
     */
    const RESPONSE_CODE_EXPIRED_CARD = 'EXPIRED_CARD';
    /**
     * @var string La tarjeta presenta una restricción.
     */
    const RESPONSE_CODE_RESTRICTED_CARD = 'RESTRICTED_CARD';
    /**
     * @var string Debe contactar al banco.
     */
    const RESPONSE_CODE_CONTACT_THE_ENTITY = 'CONTACT_THE_ENTITY';
    /**
     * @var string Se debe repetir la transacción.
     */
    const RESPONSE_CODE_REPEAT_TRANSACTION = 'REPEAT_TRANSACTION';
    /**
     * @var string La red financiera reportó un error de comunicaciones con el banco.
     */
    const RESPONSE_CODE_ENTITY_MESSAGING_ERROR = 'ENTITY_MESSAGING_ERROR';
    /**
     * @var string El banco no se encontraba disponible.
     */
    const RESPONSE_CODE_BANK_UNREACHABLE = 'BANK_UNREACHABLE';
    /**
     * @var string La transacción excede un monto establecido por el banco.
     */
    const RESPONSE_CODE_EXCEEDED_AMOUNT = 'EXCEEDED_AMOUNT';
    /**
     * @var string La transacción no fue aceptada por el banco por algún motivo.
     */
    const RESPONSE_CODE_NOT_ACCEPTED_TRANSACTION = 'NOT_ACCEPTED_TRANSACTION';
    /**
     * @var string Ocurrió un error convirtiendo los montos a la moneda de pago.
     */
    const RESPONSE_CODE_ERROR_CONVERTING_TRANSACTION_AMOUNTS = 'ERROR_CONVERTING_TRANSACTION_AMOUNTS';
    /**
     * @var string La transacción expiró.
     */
    const RESPONSE_CODE_EXPIRED_TRANSACTION = 'EXPIRED_TRANSACTION';
    /**
     * @var string La transacción fue detenida y debe ser revisada, esto puede ocurrir por filtros de seguridad.
     */
    const RESPONSE_CODE_PENDING_TRANSACTION_REVIEW = 'PENDING_TRANSACTION_REVIEW';
    /**
     * @var string La transacción está pendiente de ser confirmada.
     */
    const RESPONSE_CODE_PENDING_TRANSACTION_CONFIRMATION = 'PENDING_TRANSACTION_CONFIRMATION';
    /**
     * @var string La transacción está pendiente para ser trasmitida a la red financiera. Normalmente esto aplica para transacciones con medios de pago en efectivo.
     */
    const RESPONSE_CODE_PENDING_TRANSACTION_TRANSMISSION = 'PENDING_TRANSACTION_TRANSMISSION';
    /**
     * @var string El mensaje retornado por la red financiera es inconsistente.
     */
    const RESPONSE_CODE_PAYMENT_NETWORK_BAD_RESPONSE = 'PAYMENT_NETWORK_BAD_RESPONSE';
    /**
     * @var string No se pudo realizar la conexión con la red financiera.
     */
    const RESPONSE_CODE_PAYMENT_NETWORK_NO_CONNECTION = 'PAYMENT_NETWORK_NO_CONNECTION';
    /**
     * @var string La red financiera no respondió.
     */
    const RESPONSE_CODE_PAYMENT_NETWORK_NO_RESPONSE = 'PAYMENT_NETWORK_NO_RESPONSE';
    /**
     * @var string Clínica de transacciones: Código de manejo interno.
     */
    const RESPONSE_CODE_FIX_NOT_REQUIRED = 'FIX_NOT_REQUIRED';
    /**
     * @var string Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.
     */
    const RESPONSE_CODE_AUTOMATICALLY_FIXED_AND_SUCCESS_REVERSAL = 'AUTOMATICALLY_FIXED_AND_SUCCESS_REVERSAL';
    /**
     * @var string Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.
     */
    const RESPONSE_CODE_AUTOMATICALLY_FIXED_AND_UNSUCCESS_REVERSAL = 'AUTOMATICALLY_FIXED_AND_UNSUCCESS_REVERSAL';
    /**
     * @var string Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.
     */
    const RESPONSE_CODE_AUTOMATIC_FIXED_NOT_SUPPORTED = 'AUTOMATIC_FIXED_NOT_SUPPORTED';
    /**
     * @var string Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.
     */
    const RESPONSE_CODE_NOT_FIXED_FOR_ERROR_STATE = 'NOT_FIXED_FOR_ERROR_STATE';
    /**
     * @var string Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.
     */
    const RESPONSE_CODE_ERROR_FIXING_AND_REVERSING = 'ERROR_FIXING_AND_REVERSING';

    /**
     * @var string Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.
     */
    const RESPONSE_CODE_ERROR_FIXING_INCOMPLETE_DATA = 'ERROR_FIXING_INCOMPLETE_DATA';


    /**
     * @var array
     */
    public static $responseCodeMessages = array(
        array('ERROR' => 'Ocurrió un error general.'),
        array('APPROVED' => 'La transacción fue aprobada.'),
        array('ANTIFRAUD_REJECTED' => 'La transacción fue rechazada por el sistema anti-fraude.'),
        array('PAYMENT_NETWORK_REJECTED' => 'La red financiera rechazó la transacción.'),
        array('ENTITY_DECLINED' => 'La transacción fue declinada por el banco o por la red financiera debido a un error.'),
        array('INTERNAL_PAYMENT_PROVIDER_ERROR' => 'Ocurrió un error en el sistema intentando procesar el pago.'),
        array('INACTIVE_PAYMENT_PROVIDER' => 'El proveedor de pagos no se encontraba activo.'),
        array('DIGITAL_CERTIFICATE_NOT_FOUND' => 'La red financiera reportó un error en la autenticación.'),
        array('INVALID_EXPIRATION_DATE_OR_ SECURITY_CODE' => 'El código de seguridad o la fecha de expiración estaba inválido.'),
        array('INVALID_RESPONSE_PARTIAL_APPROVAL' => 'Tipo de respuesta no válida. La entidad aprobó parcialmente la transacción y debe ser cancelada automáticamente por el sistema.'),
        array('INSUFFICIENT_FUNDS' => 'La cuenta no tenía fondos suficientes.'),
        array('CREDIT_CARD_NOT_AUTHORIZED _FOR_INTERNET_TRANSACTIONS' => 'La tarjeta de crédito no estaba autorizada para transacciones por Internet.'),
        array('INVALID_TRANSACTION' => 'La red financiera reportó que la transacción fue inválida.'),
        array('INVALID_CARD' => 'La tarjeta es inválida.'),
        array('EXPIRED_CARD' => 'La tarjeta ya expiró.'),
        array('RESTRICTED_CARD' => 'La tarjeta presenta una restricción.'),
        array('CONTACT_THE_ENTITY' => 'Debe contactar al banco.'),
        array('REPEAT_TRANSACTION' => 'Se debe repetir la transacción.'),
        array('ENTITY_MESSAGING_ERROR' => 'La red financiera reportó un error de comunicaciones con el banco.'),
        array('BANK_UNREACHABLE' => 'El banco no se encontraba disponible.'),
        array('EXCEEDED_AMOUNT' => 'La transacción excede un monto establecido por el banco.'),
        array('NOT_ACCEPTED_TRANSACTION' => 'La transacción no fue aceptada por el banco por algún motivo.'),
        array('ERROR_CONVERTING_TRANSACTION_AMOUNTS' => 'Ocurrió un error convirtiendo los montos a la moneda de pago.'),
        array('EXPIRED_TRANSACTION' => 'La transacción expiró.'),
        array('PENDING_TRANSACTION_REVIEW' => 'La transacción fue detenida y debe ser revisada, esto puede ocurrir por filtros de seguridad.'),
        array('PENDING_TRANSACTION_CONFIRMATION' => 'La transacción está pendiente de ser confirmada.'),
        array('PENDING_TRANSACTION_TRANSMISSION' => 'La transacción está pendiente para ser trasmitida a la red financiera. Normalmente esto aplica para transacciones con medios de pago en efectivo.'),
        array('PAYMENT_NETWORK_BAD_RESPONSE' => 'El mensaje retornado por la red financiera es inconsistente.'),
        array('PAYMENT_NETWORK_NO_CONNECTION' => 'No se pudo realizar la conexión con la red financiera.'),
        array('PAYMENT_NETWORK_NO_RESPONSE' => 'La red financiera no respondió.'),
        array('FIX_NOT_REQUIRED' => 'Clínica de transacciones: Código de manejo interno.'),
        array('AUTOMATICALLY_FIXED_AND_SUCCESS_REVERSAL' => 'Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.'),
        array('AUTOMATICALLY_FIXED_AND_UNSUCCESS_REVERSAL' => 'Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.'),
        array('AUTOMATIC_FIXED_NOT_SUPPORTED' => 'Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.'),
        array('NOT_FIXED_FOR_ERROR_STATE' => 'Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.'),
        array('ERROR_FIXING_AND_REVERSING' => 'Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.'),
        array('ERROR_FIXING_INCOMPLETE_DATA' => 'Clínica de transacciones: Código de manejo interno. Sólo aplica para la API de reportes.'),

    );

}