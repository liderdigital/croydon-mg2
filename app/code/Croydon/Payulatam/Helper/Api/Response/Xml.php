<?php


namespace Croydon\Payulatam\Helper\Api\Response;

use Psr\Log\LoggerInterface;

class Xml extends \SimpleXMLElement
{

    /**
     *
     * @throws PayuResponseException
     */
    public function processResponse()
    {
        $code = (string)$this->children()->code;
        file_put_contents(__DIR__ . '/code.txt', $code);
        if ($code != PayuResponseVars::SUCCESS) {
            throw new PayuResponseException($this->children()->error);
        }

    }

    /**
     * @throws PayuResponseCodeException
     */
    public function processResponseCode()
    {
        $responseCode = (string)$this->children()->transactionResponse->responseCode;
        file_put_contents(__DIR__ . '/responseCode.txt', $responseCode);
        if ($responseCode != PayuResponseVars::RESPONSE_CODE_APPROVED) {
            throw new PayuResponseCodeException(PayuResponseVars::$responseCodeMessages[$responseCode], $responseCode);
        }
    }

    /**
     *
     * @throws PayuStateException
     */
    public function processState()
    {
        $state = (string)$this->children()->transactionResponse->state;
        file_put_contents(__DIR__ . '/state.txt', $state);
        if ($state != PayuResponseVars::STATE_APPROVED || $state != PayuResponseVars::STATE_PENDING) {
            $responseMessage = (string)$this->children()->transactionResponse->responseMessage;
            throw new PayuStateException($responseMessage, $state);
        }

    }

}