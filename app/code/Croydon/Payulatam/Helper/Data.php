<?php


namespace Croydon\Payulatam\Helper;


use Magento\Framework\App\Config\Initial;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\View\LayoutFactory;
use Magento\Payment\Model\Config;
use Magento\Payment\Model\Method\Factory;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Payment\Helper\Data
{

    /**
     * @var bool
     */
    protected $_environment;

    /**
     * Data constructor.
     * @param Context $context
     * @param LayoutFactory $layoutFactory
     * @param Factory $paymentMethodFactory
     * @param Emulation $appEmulation
     * @param Config $paymentConfig
     * @param Initial $initialConfig
     */
    public function __construct(Context $context, LayoutFactory $layoutFactory, Factory $paymentMethodFactory, Emulation $appEmulation, Config $paymentConfig, Initial $initialConfig)
    {
        parent::__construct($context, $layoutFactory, $paymentMethodFactory, $appEmulation, $paymentConfig, $initialConfig);
        $this->_environment = (bool)(int)$this->scopeConfig->getValue('payment/croydon_payulatam/environment', ScopeInterface::SCOPE_STORE);
    }

    public function getMerchantId()
    {
        if ($this->_environment) {
            return $this->scopeConfig->getValue('payment/croydon_payulatam/enviroment_payu/development/merchantId', ScopeInterface::SCOPE_STORE);
        } else {
            return $this->scopeConfig->getValue('payment/croydon_payulatam/enviroment_payu/production/merchantId', ScopeInterface::SCOPE_STORE);
        }
    }

    public function getAccountId()
    {
        if ($this->_environment) {
            return $this->scopeConfig->getValue('payment/croydon_payulatam/enviroment_payu/development/accountId', ScopeInterface::SCOPE_STORE);
        } else {
            return $this->scopeConfig->getValue('payment/croydon_payulatam/enviroment_payu/production/accountId', ScopeInterface::SCOPE_STORE);
        }
    }

    public function getApiKey()
    {
        if ($this->_environment) {
            return $this->scopeConfig->getValue('payment/croydon_payulatam/enviroment_payu/development/apiKey', ScopeInterface::SCOPE_STORE);
        } else {
            return $this->scopeConfig->getValue('payment/croydon_payulatam/enviroment_payu/production/apiKey', ScopeInterface::SCOPE_STORE);
        }
    }

    public function getApiLogin()
    {
        if ($this->_environment) {
            return $this->scopeConfig->getValue('payment/croydon_payulatam/enviroment_payu/development/apiLogin', ScopeInterface::SCOPE_STORE);
        } else {
            return $this->scopeConfig->getValue('payment/croydon_payulatam/enviroment_payu/production/apiLogin', ScopeInterface::SCOPE_STORE);
        }
    }

    /**
     * @return bool
     */
    public function isEnvironment()
    {
        return $this->_environment;
    }


}