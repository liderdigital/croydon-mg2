<?php


namespace Croydon\Payulatam\Helper\Service;


abstract class AbstractEntityService implements \JsonSerializable
{


    public abstract function toArray(): array;

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function __toString()
    {
        if (count($this->toArray()) > 0)
            return json_encode($this);
        return '';
    }

}