<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of AdditionalValues
 *
 * @author joncasasq
 */
class AdditionalValues extends AbstractEntityService
{

    /**
     * @var TxValue
     */
    private $TX_VALUE;

    /**
     * @var TxTax
     */
    private $TX_TAX;

    /**
     * @var TxTaxReturnBase;
     */
    private $TX_TAX_RETURN_BASE;

    /**
     * @return TxValue
     */
    public function getTXVALUE(): TxValue
    {
        return $this->TX_VALUE;
    }

    /**
     * @param TxValue $TX_VALUE
     */
    public function setTXVALUE(TxValue $TX_VALUE)
    {
        $this->TX_VALUE = $TX_VALUE;
    }

    /**
     * @return TxTax
     */
    public function getTXTAX(): TxTax
    {
        return $this->TX_TAX;
    }

    /**
     * @param TxTax $TX_TAX
     */
    public function setTXTAX(TxTax $TX_TAX)
    {
        $this->TX_TAX = $TX_TAX;
    }

    /**
     * @return TxTaxReturnBase
     */
    public function getTXTAXRETURNBASE(): TxTaxReturnBase
    {
        return $this->TX_TAX_RETURN_BASE;
    }

    /**
     * @param TxTaxReturnBase $TX_TAX_RETURN_BASE
     */
    public function setTXTAXRETURNBASE(TxTaxReturnBase $TX_TAX_RETURN_BASE)
    {
        $this->TX_TAX_RETURN_BASE = $TX_TAX_RETURN_BASE;
    }


    public function toArray(): array
    {
        $array = array();
        if (!is_null($this->TX_TAX_RETURN_BASE) && $this->TX_TAX_RETURN_BASE->getValue() > 0) {
            $array['TX_TAX_RETURN_BASE'] = $this->TX_TAX_RETURN_BASE;
        }
        if (!is_null($this->TX_TAX) && $this->TX_TAX->getValue() > 0) {
            $array['TX_TAX'] = $this->TX_TAX;
        }
        $array['TX_VALUE'] = $this->TX_VALUE;
        return $array;
    }
}
