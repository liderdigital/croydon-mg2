<?php

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of ApiPayu
 *
 * @author joncasasq
 */
class ApiPayu extends AbstractEntityService
{

    /**
     * @var string
     */
    private $language; //String

    /**
     * @var string
     */
    private $command; //String

    /**
     * @var Merchant
     */
    private $merchant; //Merchant

    /**
     * @var Transaction
     */
    private $transaction; //Transaction

    /**
     * @var bool
     */
    private $test;

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @param string $command
     */
    public function setCommand(string $command)
    {
        $this->command = $command;
    }

    /**
     * @return Merchant
     */
    public function getMerchant(): Merchant
    {
        return $this->merchant;
    }

    /**
     * @param Merchant $merchant
     */
    public function setMerchant(Merchant $merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * @return Transaction
     */
    public function getTransaction(): Transaction
    {
        return $this->transaction;
    }

    /**
     * @param Transaction $transaction
     */
    public function setTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return bool
     */
    public function isTest(): bool
    {
        return $this->test;
    }

    /**
     * @param bool $test
     */
    public function setTest(bool $test)
    {
        $this->test = $test;
    }

    public function toArray(): array
    {
        return array(
            'language' => $this->language,
            'command' => $this->command,
            'merchant' => $this->merchant,
            'transaction' => $this->transaction,
            'test' => $this->test,
        );
    }
}
