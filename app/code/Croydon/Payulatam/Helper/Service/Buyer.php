<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of Buyer
 *
 * @author joncasasq
 */
class Buyer extends AbstractEntityService
{

    /**
     * @var string
     */
    private $merchantBuyerId; //String

    /**
     * @var string
     */
    private $fullName; //String

    /**
     * @var string
     */
    private $emailAddress; //String

    /**
     * @var string
     */
    private $contactPhone; //String

    /**
     * @var string
     */
    private $dniNumber; //String

    /**
     * @var ShippingAddress
     */
    private $shippingAddress;

    /**
     * @return string
     */
    public function getMerchantBuyerId(): string
    {
        return $this->merchantBuyerId;
    }

    /**
     * @param string $merchantBuyerId
     */
    public function setMerchantBuyerId(string $merchantBuyerId)
    {
        $this->merchantBuyerId = $merchantBuyerId;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName(string $fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress(string $emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function getContactPhone(): string
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     */
    public function setContactPhone(string $contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return string
     */
    public function getDniNumber(): string
    {
        return $this->dniNumber;
    }

    /**
     * @param string $dniNumber
     */
    public function setDniNumber(string $dniNumber)
    {
        $this->dniNumber = $dniNumber;
    }

    /**
     * @return ShippingAddress
     */
    public function getShippingAddress(): ShippingAddress
    {
        return $this->shippingAddress;
    }

    /**
     * @param ShippingAddress $shippingAddress
     */
    public function setShippingAddress(ShippingAddress $shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;
    }


    public function toArray(): array
    {
        return array(
            'merchantBuyerId' => $this->merchantBuyerId,
            'fullName' => $this->fullName,
            'emailAddress' => $this->emailAddress,
            'contactPhone' => $this->contactPhone,
            'dniNumber' => $this->dniNumber,
            'shippingAddress' => $this->shippingAddress,
        );
    }
}
