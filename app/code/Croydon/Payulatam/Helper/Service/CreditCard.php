<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of CreditCard
 *
 * @author joncasasq
 */
class CreditCard extends AbstractEntityService
{

    /**
     * @var string
     */
    private $number; //String

    /**
     * @var string
     */
    private $securityCode; //String

    /**
     * @var string
     */
    private $expirationDate; //String

    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getSecurityCode(): string
    {
        return $this->securityCode;
    }

    /**
     * @param string $securityCode
     */
    public function setSecurityCode(string $securityCode)
    {
        $this->securityCode = $securityCode;
    }

    /**
     * @return string
     */
    public function getExpirationDate(): string
    {
        return $this->expirationDate;
    }

    /**
     * @param string $expirationDate
     */
    public function setExpirationDate(string $expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function toArray(): array
    {
        return array(
            'number' => $this->number,
            'securityCode' => $this->securityCode,
            'expirationDate' => $this->expirationDate,
            'name' => $this->name,
        );
    }
}
