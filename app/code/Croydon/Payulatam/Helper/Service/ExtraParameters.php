<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of ExtraParameters
 *
 * @author joncasasq
 */
class ExtraParameters extends AbstractEntityService
{

    /**
     * @var int
     */
    private $INSTALLMENTS_NUMBER;

    /**
     * @return int
     */
    public function getINSTALLMENTSNUMBER(): int
    {
        return $this->INSTALLMENTS_NUMBER;
    }

    /**
     * @param int $INSTALLMENTS_NUMBER
     */
    public function setINSTALLMENTSNUMBER(int $INSTALLMENTS_NUMBER)
    {
        $this->INSTALLMENTS_NUMBER = $INSTALLMENTS_NUMBER;
    }


    public function toArray(): array
    {
        return array(
            'INSTALLMENTS_NUMBER' => $this->INSTALLMENTS_NUMBER
        );
    }
}
