<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of Merchant
 *
 * @author joncasasq
 */
class Merchant extends AbstractEntityService
{

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $apiLogin;

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getApiLogin(): string
    {
        return $this->apiLogin;
    }

    /**
     * @param string $apiLogin
     */
    public function setApiLogin(string $apiLogin)
    {
        $this->apiLogin = $apiLogin;
    }


    public function toArray(): array
    {
        return array(
            'apiKey' => $this->apiKey,
            'apiLogin' => $this->apiLogin
        );

    }
}
