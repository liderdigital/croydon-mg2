<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of Order
 *
 * @author joncasasq
 */
class Order extends AbstractEntityService
{

    /**
     * @var string
     */
    private $accountId; //String

    /**
     * @var string
     */
    private $referenceCode; //String

    /**
     * @var string
     */
    private $description; //String

    /**
     * @var string
     */
    private $language; //String

    /**
     * @var string
     */
    private $signature; //String

    /**
     * @var string
     */
    private $notifyUrl; //String

    /**
     * @var AdditionalValues
     */
    private $additionalValues; //AdditionalValues

    /**
     * @var Buyer
     */
    private $buyer; //Buyer

    /**
     * @var ShippingAddress
     */
    private $shippingAddress;

    /**
     * @return string
     */
    public function getAccountId(): string
    {
        return $this->accountId;
    }

    /**
     * @param string $accountId
     */
    public function setAccountId(string $accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return string
     */
    public function getReferenceCode(): string
    {
        return $this->referenceCode;
    }

    /**
     * @param string $referenceCode
     */
    public function setReferenceCode(string $referenceCode)
    {
        $this->referenceCode = $referenceCode;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getSignature(): string
    {
        return $this->signature;
    }

    /**
     * @param string $signature
     */
    public function setSignature(string $signature)
    {
        $this->signature = $signature;
    }

    /**
     * @return string
     */
    public function getNotifyUrl(): string
    {
        return $this->notifyUrl;
    }

    /**
     * @param string $notifyUrl
     */
    public function setNotifyUrl(string $notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @return AdditionalValues
     */
    public function getAdditionalValues(): AdditionalValues
    {
        return $this->additionalValues;
    }

    /**
     * @param AdditionalValues $additionalValues
     */
    public function setAdditionalValues(AdditionalValues $additionalValues)
    {
        $this->additionalValues = $additionalValues;
    }

    /**
     * @return Buyer
     */
    public function getBuyer(): Buyer
    {
        return $this->buyer;
    }

    /**
     * @param Buyer $buyer
     */
    public function setBuyer(Buyer $buyer)
    {
        $this->buyer = $buyer;
    }

    /**
     * @return ShippingAddress
     */
    public function getShippingAddress(): ShippingAddress
    {
        return $this->shippingAddress;
    }

    /**
     * @param ShippingAddress $shippingAddress
     */
    public function setShippingAddress(ShippingAddress $shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;
    }


    public function toArray(): array
    {
        return array(
            'accountId' => $this->accountId,
            'referenceCode' => $this->referenceCode,
            'description' => $this->description,
            'language' => $this->language,
            'signature' => $this->signature,
            'notifyUrl' => $this->notifyUrl,
            'additionalValues' => $this->additionalValues,
            'buyer' => $this->buyer,
            'shippingAddress' => $this->shippingAddress
        );
    }
}
