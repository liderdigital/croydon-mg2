<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of Payer
 *
 * @author joncasasq
 */
class Payer extends AbstractEntityService
{

    /**
     * @var string
     */
    private $merchantPayerId; //String

    /**
     * @var string
     */
    private $fullName; //String

    /**
     * @var string
     */
    private $emailAddress; //String

    /**
     * @var string
     */
    private $contactPhone; //String

    /**
     * @var string
     */
    private $dniNumber; //String

    /**
     * @var BillingAddress
     */
    private $billingAddress;

    /**
     * @return string
     */
    public function getMerchantPayerId(): string
    {
        return $this->merchantPayerId;
    }

    /**
     * @param string $merchantPayerId
     */
    public function setMerchantPayerId(string $merchantPayerId)
    {
        $this->merchantPayerId = $merchantPayerId;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName(string $fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress(string $emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function getContactPhone(): string
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     */
    public function setContactPhone(string $contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return string
     */
    public function getDniNumber(): string
    {
        return $this->dniNumber;
    }

    /**
     * @param string $dniNumber
     */
    public function setDniNumber(string $dniNumber)
    {
        $this->dniNumber = $dniNumber;
    }

    /**
     * @return BillingAddress
     */
    public function getBillingAddress(): BillingAddress
    {
        return $this->billingAddress;
    }

    /**
     * @param BillingAddress $billingAddress
     */
    public function setBillingAddress(BillingAddress $billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }


    public function toArray(): array
    {
        return array(
            'merchantPayerId' => $this->merchantPayerId,
            'fullName' => $this->fullName,
            'emailAddress' => $this->emailAddress,
            'contactPhone' => $this->contactPhone,
            'dniNumber' => $this->dniNumber,
            'billingAddress' => $this->billingAddress,
        );
    }

}
