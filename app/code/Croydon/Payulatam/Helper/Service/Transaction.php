<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of Transaction
 *
 * @author joncasasq
 */
class Transaction extends AbstractEntityService
{

    /**
     * @var Order
     */
    private $order; //Order

    /**
     * @var Payer
     */
    private $payer; //Payer

    /**
     * @var CreditCard
     */
    private $creditCard; //CreditCard

    /**
     * @var ExtraParameters
     */
    private $extraParameters; //ExtraParameters

    /**
     * @var string
     */
    private $type; //String

    /**
     * @var string
     */
    private $paymentMethod; //String

    /**
     * @var string
     */
    private $paymentCountry; //String

    /**
     * @var string
     */
    private $deviceSessionId; //String

    /**
     * @var string
     */
    private $ipAddress; //String

    /**
     * @var string
     */
    private $cookie; //String

    /**
     * @var string
     */
    private $userAgent;

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Payer
     */
    public function getPayer(): Payer
    {
        return $this->payer;
    }

    /**
     * @param Payer $payer
     */
    public function setPayer(Payer $payer)
    {
        $this->payer = $payer;
    }

    /**
     * @return CreditCard
     */
    public function getCreditCard(): CreditCard
    {
        return $this->creditCard;
    }

    /**
     * @param CreditCard $creditCard
     */
    public function setCreditCard(CreditCard $creditCard)
    {
        $this->creditCard = $creditCard;
    }

    /**
     * @return ExtraParameters
     */
    public function getExtraParameters(): ExtraParameters
    {
        return $this->extraParameters;
    }

    /**
     * @param ExtraParameters $extraParameters
     */
    public function setExtraParameters(ExtraParameters $extraParameters)
    {
        $this->extraParameters = $extraParameters;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getPaymentMethod(): string
    {
        return $this->paymentMethod;
    }

    /**
     * @param string $paymentMethod
     */
    public function setPaymentMethod(string $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function getPaymentCountry(): string
    {
        return $this->paymentCountry;
    }

    /**
     * @param string $paymentCountry
     */
    public function setPaymentCountry(string $paymentCountry)
    {
        $this->paymentCountry = $paymentCountry;
    }

    /**
     * @return string
     */
    public function getDeviceSessionId(): string
    {
        return $this->deviceSessionId;
    }

    /**
     * @param string $deviceSessionId
     */
    public function setDeviceSessionId(string $deviceSessionId)
    {
        $this->deviceSessionId = $deviceSessionId;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress(string $ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return string
     */
    public function getCookie(): string
    {
        return $this->cookie;
    }

    /**
     * @param string $cookie
     */
    public function setCookie(string $cookie)
    {
        $this->cookie = $cookie;
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * @param string $userAgent
     */
    public function setUserAgent(string $userAgent)
    {
        $this->userAgent = $userAgent;
    }


    public function toArray(): array
    {
        return array(
            'order' => $this->order,
            'payer' => $this->payer,
            'creditCard' => $this->creditCard,
            'extraParameters' => $this->extraParameters,
            'type' => $this->type,
            'paymentMethod' => $this->paymentMethod,
            'paymentCountry' => $this->paymentCountry,
            'deviceSessionId' => $this->deviceSessionId,
            'ipAddress' => $this->ipAddress,
            'cookie' => $this->cookie,
            'userAgent' => $this->userAgent
        );
    }
}
