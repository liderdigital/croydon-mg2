<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Payulatam\Helper\Service;

/**
 * Description of TxTaxReturnBase
 *
 * @author joncasasq
 */
class TxTaxReturnBase extends AbstractEntityService
{

    /**
     * @var int
     */
    private $value; //int

    /**
     * @var string
     */
    private $currency;

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    public function toArray(): array
    {
        if (!is_null($this->value) && $this->value > 0)
            return array(
                'value' => $this->value,
                'currency' => $this->currency
            );
        return array();
    }


}
