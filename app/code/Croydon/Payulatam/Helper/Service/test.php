<?php
/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 9/07/19
 * Time: 16:41
 */


$apiPayu = new \Croydon\Payulatam\Helper\Service\ApiPayu();
$apiPayu->setCommand('SUBMIT_TRANSACTION');
$apiPayu->setLanguage('es');
$merchant = new \Croydon\Payulatam\Helper\Service\Merchant();
$merchant->setApiKey('4Vj8eK4rloUd272L48hsrarnUA');
$merchant->setApiLogin('pRRXKOl8ikMmt9u');
$apiPayu->setMerchant($merchant);

$transaction = new \Croydon\Payulatam\Helper\Service\Transaction();
$transaction->setCookie('pt1t38347bs6jc9ruv2ecpv7o2');
$creditCard = new \Croydon\Payulatam\Helper\Service\CreditCard();
$creditCard->setExpirationDate('2020/12');
$creditCard->setName('APPROVED');
$creditCard->setNumber('4097440000000004');
$creditCard->setSecurityCode('321');
$transaction->setCreditCard($creditCard);
$transaction->setDeviceSessionId('vghs6tvkcle931686k1900o6e1');
$extraParameters = new \Croydon\Payulatam\Helper\Service\ExtraParameters();
$extraParameters->setINSTALLMENTSNUMBER(1);
$transaction->setExtraParameters($extraParameters);
$transaction->setIpAddress('127.0.0.1');

$order = new \Croydon\Payulatam\Helper\Service\Order();
$order->setLanguage('es');
$order->setAccountId(512321);
$additionalValues = new \Croydon\Payulatam\Helper\Service\AdditionalValues();
$txTax = new \Croydon\Payulatam\Helper\Service\TxTax();
$txTax->setValue(3193);
$txTax->setCurrency('COP');
$additionalValues->setTXTAX($txTax);
$txTaxReturnBase = new \Croydon\Payulatam\Helper\Service\TxTaxReturnBase();
$txTaxReturnBase->setCurrency('COP');
$txTaxReturnBase->setValue(16806);
$additionalValues->setTXTAXRETURNBASE($txTaxReturnBase);
$txValue = new \Croydon\Payulatam\Helper\Service\TxValue();
$txValue->setValue('20000');
$txValue->setCurrency('COP');
$additionalValues->setTXVALUE($txValue);
$order->setAdditionalValues($additionalValues);

$buyer = new \Croydon\Payulatam\Helper\Service\Buyer();
$shippingAddress = new \Croydon\Payulatam\Helper\Service\ShippingAddress();
$shippingAddress->setState('Antioquia');
$shippingAddress->setCity('Medellin');
$shippingAddress->setCountry('CO');
$shippingAddress->setPhone('7563126');
$shippingAddress->setPostalCode('000000');
$shippingAddress->setStreet1('calle 100');
$shippingAddress->setStreet2('5555487');

$buyer->setShippingAddress($shippingAddress);
$buyer->setContactPhone(7563126);
$buyer->setDniNumber(5415668464654);
$buyer->setEmailAddress('buyer_test@test.com');
$buyer->setFullName('First name and second buyer  name');
$buyer->setMerchantBuyerId(1);
$order->setBuyer($buyer);
$order->setDescription('payment test');
$order->setNotifyUrl('http://www.tes.com/confirmation');
$order->setReferenceCode('TestPayU');
$order->setShippingAddress($shippingAddress);
$order->setSignature('7ee7cf808ce6a39b17481c54f2c57acc');
$transaction->setOrder($order);

$payer = new \Croydon\Payulatam\Helper\Service\Payer();
$payer->setFullName('First name and second payer name');
$payer->setEmailAddress('payer_test@test.com');
$payer->setDniNumber(5415668464654);
$payer->setContactPhone(7563126);

$billingAddress = new \Croydon\Payulatam\Helper\Service\BillingAddress();
$billingAddress->setStreet2(125544);
$billingAddress->setStreet1('calle 93');
$billingAddress->setPostalCode(000000);
$billingAddress->setPhone(7563126);
$billingAddress->setCountry('CO');
$billingAddress->setCity('Bogota');
$billingAddress->setState('Bogota DC');
$payer->setBillingAddress($billingAddress);
$payer->setMerchantPayerId(1);
$transaction->setPayer($payer);

$transaction->setPaymentCountry('CO');
$transaction->setPaymentMethod('VISA');
$transaction->setType('AUTHORIZATION_AND_CAPTURE');
$transaction->setUserAgent('Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0');

$apiPayu->setTransaction($transaction);
$apiPayu->setTest(true);

$ch = curl_init();
$content = json_encode($apiPayu);
curl_setopt($ch, CURLOPT_URL, 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi');
curl_setopt($ch, CURLOPT_POST, 1);// set post data to true
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($content))
);
curl_setopt($ch, CURLOPT_POSTFIELDS, $content);   // post data
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$json = curl_exec($ch);
curl_close($ch);
var_dump($json);
