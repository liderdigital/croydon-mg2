<?php
/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 8/07/19
 * Time: 15:12
 */

namespace Croydon\Payulatam\Model;


use Croydon\Payulatam\Helper\Api\Capture;
use Croydon\Payulatam\Helper\Api\CaptureInterface;
use Croydon\Payulatam\Helper\Api\Response\Json;
use Croydon\Payulatam\Helper\Api\Response\PayuResponseCodeException;
use Croydon\Payulatam\Helper\Api\Response\PayuResponseException;
use Croydon\Payulatam\Helper\Api\Response\PayuStateException;
use Exception;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\Cc;
use Magento\Sales\Model\Order;
use Magento\Payment\Model\Method\Logger;

class Payulatam extends Cc
{
    const EVENT_NAME = 'payulatam_capture_payment_event';
    /**
     * @var EventManager
     */
    private $eventManager;
    const CODE = 'croydon_payulatam';
    const COOKIE = 'croydon_payulatam';
    const RAMDOM_LENGTH = 20;
    protected $_code = self::CODE;

    protected $_isGateway = true;
    protected $_canCapture = true;
    protected $_canCapturePartial = true;
    protected $_canRefund = false;
    protected $_canRefundInvoicePartial = false;
    protected $_canAuthorize = false;

    /**
     * @var Capture
     */
    protected $capture;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param ModuleListInterface $moduleList
     * @param TimezoneInterface $localeDate
     * @param CaptureInterface $capture
     * @param ManagerInterface $messageManager
     * @param EventManager $eventManager
     * @param CookieManagerInterface $cookieManager
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        ModuleListInterface $moduleList,
        TimezoneInterface $localeDate,
        CaptureInterface $capture,
        ManagerInterface $messageManager,
        CookieManagerInterface $cookieManager,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [])
    {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            $resource,
            $resourceCollection,
            $data
        );
        try {
            $cookieManager->setPublicCookie(self::COOKIE, random_bytes(self::RAMDOM_LENGTH));
        } catch (InputException $e) {
        } catch (CookieSizeLimitReachedException $e) {
        } catch (FailureToSendException $e) {
        } catch (\Exception $e) {
        }
        $this->capture = $capture;
        $this->messageManager = $messageManager;
    }

    /**
     * Capture Payment.
     *
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws PayuResponseException
     * @throws PayuResponseCodeException
     * @throws PayuStateException
     * @throws \ReflectionException
     * @throws Exception
     */
    public function capture(InfoInterface $payment, $amount)
    {
        /** @var \Croydon\Payulatam\Logger\Logger $logger */
        $logger = ObjectManager::getInstance()->create(\Croydon\Payulatam\Logger\Logger::class);
        /** @var Order $order */
        $order = $payment->getOrder();
        //$this->_eventManager->dispatch(self::EVENT_NAME, ['order' => $order]);
        $this->_logger->info('Pruebas payu');
        $this->capture->setOrder($order);
        $this->capture->setPayment($payment);
        $jsonResponse = $this->capture->sendRequest();
        $this->_logger->info($jsonResponse);
        $json = new Json($jsonResponse);
        try {
            $json->processResponse();
            $json->processResponseCode();
            $json->processState();
        } catch (PayuResponseException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $logger->error('RESPONSE: ' . $e->getMessage());
            throw new PayuResponseException($e->getMessage());
        } catch (PayuResponseCodeException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $logger->error('CODE: ' . $e->getPayuCode());
            throw new PayuResponseCodeException($e->getMessage(), $e->getPayuCode());
        } catch (PayuStateException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $logger->error('STATE: ' . $e->getPayuCode());
            throw new PayuStateException($e->getMessage(), $e->getPayuCode());
        }
        return $this;
    }

    /**
     * Authorize a payment.
     *
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this
     */
    public function authorize(InfoInterface $payment, $amount)
    {
        try {
            ///build array of payment data for API request.
            $request = [
                'cc_type' => $payment->getCcType(),
                'cc_exp_month' => $payment->getCcExpMonth(),
                'cc_exp_year' => $payment->getCcExpYear(),
                'cc_number' => $payment->getCcNumberEnc(),
                'amount' => $amount
            ];
            //check if payment has been authorized
            $response = $this->makeAuthRequest($request);
        } catch (\Exception $e) {
            //$this->debug($payment->getData(), $e->getMessage());
            $this->_logger->info('Error Payment authorize ' . $e->getMessage());
        }
        if (isset($response['transactionID'])) {
            // Successful auth request.
            // Set the transaction id on the payment so the capture request knows auth has happened.
            $payment->setTransactionId($response['transactionID']);
            $payment->setParentTransactionId($response['transactionID']);
        }
        //processing is not done yet.
        $payment->setIsTransactionClosed(0);
        return $this;
    }

    /**
     * Set the payment action to authorize_and_capture
     *
     * @return string
     */
    public function getConfigPaymentAction()
    {
        return self::ACTION_AUTHORIZE_CAPTURE;
    }

    /**
     * Test method to handle an API call for capture request.
     *
     * @param Order $request
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function makeCaptureRequest($request)
    {

        $response = ['success']; //todo implement API call for capture request.
        if (!$response) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Failed capture request.'));
        }
        return $response;
    }
}