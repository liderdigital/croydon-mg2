define([
        'jquery',
        'Magento_Payment/js/view/payment/cc-form',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/action/place-order',
        'mage/url'
    ],
    function ($, Component, payment, customer, placeOrderAction, urlBuilder) {
        'use strict';

        return Component.extend({
            defaults: {
                redirectAfterPlaceOrder: false,
                template: 'Croydon_Payulatam/payment/payulatam'
            },
            placeOrderHandler: null,
            payulatamImageSrc: window.populatePayulatam.payulatamImageSrc,
            context: function () {
                return this;
            },

            getCode: function () {
                return 'croydon_payulatam';
            },

            isActive: function () {
                return true;
            },
            placeOrder: function (data, event) {
                if (event) {
                    event.preventDefault();
                }
                var self = this,
                    placeOrder,
                    emailValidationResult = customer.isLoggedIn(),
                    loginFormSelector = 'form[data-role=email-with-possible-login]';
                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }
                if (emailValidationResult && this.validate()) {
                    this.isPlaceOrderActionAllowed(false);
                    placeOrder = placeOrderAction(this.getData(), this.redirectAfterPlaceOrder);
                    $.when(placeOrder).done(function () {
                        $.mage.redirect(urlBuilder.build('payulatam/onepage/success'));
                    }).fail(function (jqXhr,more) {
                        console.log(jqXhr);
                        console.log(more);
                        self.isPlaceOrderActionAllowed(true);
                        //$.mage.redirect(urlBuilder.build('payulatam/onepage/error'));
                    });
                    return true;
                }
                return false;
            }
        });
    }
);