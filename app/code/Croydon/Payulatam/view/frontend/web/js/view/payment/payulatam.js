define([
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        rendererList.push(
            {
                type: 'croydon_payulatam',
                component: 'Croydon_Payulatam/js/view/payment/method-renderer/payulatam'
            }
        );

        /** Add view logic here if needed */
        return Component.extend({});
    });
