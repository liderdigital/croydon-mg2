<?php
/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 14/05/19
 * Time: 9:24
 */

namespace Croydon\Pse\Block;

use Croydon\Pse\Helper\Data;
use Croydon\Services\Model\PseInterface;
use Croydon\Servicios\MethodNotFoundException;
use Croydon\Servicios\InvokeServiceException;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\Order;
use \Magento\Customer\Model\Data\Customer;


class Index extends Template
{

    /**
     * @var Order
     */
    private $order;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var PseInterface
     */
    private $apiServicePse;
    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * @var Data
     */
    private $data;

    /**
     * Index constructor.
     * @param PseInterface $apiServicePse
     * @param Data $data
     */
    public function __construct(PseInterface $apiServicePse, Data $data)
    {
        $this->apiServicePse = $apiServicePse;
        $this->data = $data;
        parent::_construct();
    }

    /**
     * Lee el cliente que pertenece a la orden
     * @return Customer
     * @throws \Exception
     */
    public function getCustomer()
    {
        if (is_null($this->customer)) {
            $objectManager = ObjectManager::getInstance();
            /** @var \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository */
            $customerRepository = $objectManager->create('Magento\Customer\Api\CustomerRepositoryInterface');
            try {
                $this->customer = $customerRepository->getById($this->order->getCustomerId());
            } catch (NoSuchEntityException $e) {
                throw new \Exception($e->getMessage());
            } catch (LocalizedException $e) {
                throw new \Exception($e->getMessage());
            }
        }
        return $this->customer;
    }


    /**
     * Consume el servicio
     * @return \Croydon\Service\Pse\createTransactionResponseType
     * @throws MethodNotFoundException
     * @throws InvokeServiceException
     * @throws \Exception
     */
    public function consumeService()
    {
        $this->getOrder();
        try {
            $this->getCustomer();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        try {
            $response = $this->apiServicePse->createTransactionPayment($this->order)->getCreateTransactionPaymentResult();
            return $response;
        } catch (InvokeServiceException $e) {
            $this->_logger->error("Error -----" . $e->getMessage());
            throw new InvokeServiceException($e);
        } catch (MethodNotFoundException $e) {
            $this->_logger->error("Error -----" . $e->getMessage());
            throw new MethodNotFoundException($e->getMessage());
        }

    }

    private function getResponseUrl()
    {

    }

    private function getConfirmationUrl()
    {
    }

    private function getOrder()
    {
    }


}