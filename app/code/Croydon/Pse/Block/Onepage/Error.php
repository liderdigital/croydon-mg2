<?php


namespace Croydon\Pse\Block\Onepage;


use Croydon\Payment\Block\BaseError;

class Error extends BaseError
{


    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->order->getBillingAddress()->getEmail();
    }


}