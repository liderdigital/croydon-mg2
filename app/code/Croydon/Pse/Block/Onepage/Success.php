<?php


namespace Croydon\Pse\Block\Onepage;


use Croydon\Payment\Block\BaseSucces;

class Success extends BaseSucces
{

    public function getName()
    {
        $shippingAddress = $this->order->getShippingAddress();
        return $shippingAddress->getName();
    }

}