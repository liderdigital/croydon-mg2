<?php
/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 14/05/19
 * Time: 9:20
 */

namespace Croydon\Pse\Controller\Index;


use Croydon\Pse\Helper\Data;
use Croydon\Pse\Helper\Index as IndexHelper;
use Croydon\Pse\Helper\ReferenceArray;
use Croydon\Pse\Model\ResourceModel\PseOrder\Collection;
use Croydon\Services\Model\PseInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    /**
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * @var Registry $_coreRegistry
     */
    protected $_coreRegistry;

    /**
     * @var Data
     */
    protected $data;

    /**
     * @var ReferenceArray
     */
    protected $referenceArray;

    /**
     * @var IndexHelper
     */
    protected $index;

    /**
     * @var ResultFactory
     */
    protected $resultRedirect;

    /**
     * @var PseInterface
     */
    protected $pse;

    /**
     * @var Collection
     */
    protected $collection;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Data $data,
        ReferenceArray $referenceArray,
        IndexHelper $index,
        ResultFactory $result,
        PseInterface $pse,
        Collection $collection
    )
    {
        parent::__construct($context);
        $this->_pageFactory = $pageFactory;
        $this->data = $data;
        $this->referenceArray = $referenceArray;
        $this->index = $index;
        $this->resultRedirect = $result;
        $this->pse = $pse;
        $this->collection = $collection;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $collection = $this->collection->addFilter('order_id', $this->_request->getParam('order_id'))->load();
        if ($collection->count() <= 0) {
            throw new NotFoundException(__('Parameter is incorrect.'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $pseOrder = $collection->getFirstItem();
        try {
            $result = $this->pse->getTransactionInformation($this->data->getEntityCode(), $pseOrder->getData('ticked_id'));
        } catch (\Exception $e) {
            $resultRedirect->setPath('pse/onepage/error');
            return $resultRedirect;
        }
        switch ($result->getGetTransactionInformationResult()->getTranState()) {
            case 'SUCCESS':
            case 'OK':
                $resultRedirect->setPath('pse/onepage/success');
                break;
            case 'FAILED':
            case 'NOT_AUTHORIZED':
                $resultRedirect->setPath('pse/onepage/error');
                break;
            case 'PENDING':
                $resultRedirect->setPath('pse/onepage/pending');
                break;
        }
        return $resultRedirect;
    }


}