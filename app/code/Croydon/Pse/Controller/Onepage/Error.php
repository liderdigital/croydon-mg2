<?php


namespace Croydon\Pse\Controller\Onepage;


use Croydon\Pse\Helper\Data;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Error extends \Magento\Framework\App\Action\Action
{

    protected $_pageFactory;
    /** @var Registry $_coreRegistry */
    protected $_coreRegistry;
    private $data;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Data $data
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->data = $data;
        return parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        return $this->_pageFactory->create();
    }
}