<?php


namespace Croydon\Pse\Helper;

use Magento\Framework\App\Config\Initial;
use Magento\Framework\View\LayoutFactory;
use \Magento\Framework\App\Helper\Context;
use Magento\Payment\Model\Config;
use \Magento\Payment\Model\Method\Factory;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Payment\Helper\Data
{
    protected $_environment;

    /**
     * Data constructor.
     * @param Context $context
     * @param LayoutFactory $layoutFactory
     * @param Factory $paymentMethodFactory
     * @param Emulation $appEmulation
     * @param Config $paymentConfig
     * @param Initial $initialConfig
     */
    public function __construct(Context $context, LayoutFactory $layoutFactory, Factory $paymentMethodFactory, Emulation $appEmulation, Config $paymentConfig, Initial $initialConfig)
    {
        parent::__construct($context, $layoutFactory, $paymentMethodFactory, $appEmulation, $paymentConfig, $initialConfig);
        $this->_environment = (bool)(int)$this->scopeConfig->getValue('payment/pse/environment', ScopeInterface::SCOPE_STORE);
    }

    public function getEnviroment()
    {
        return $this->_environment;
    }

    public function getActive()
    {
        return (bool)(int)$this->scopeConfig->getValue('payment/pse/active', ScopeInterface::SCOPE_STORE);
    }

    public function getEntityCode()
    {
        if ($this->_environment) {
            return $this->scopeConfig->getValue('payment/pse/enviroment_pse/development/entityCode', ScopeInterface::SCOPE_STORE);
        } else {
            return $this->scopeConfig->getValue('payment/pse/enviroment_pse/production/entityCode', ScopeInterface::SCOPE_STORE);
        }
    }

    public function getSrvCode()
    {
        if ($this->_environment) {
            return $this->scopeConfig->getValue('payment/pse/enviroment_pse/development/srvCode', ScopeInterface::SCOPE_STORE);
        } else {
            return $this->scopeConfig->getValue('payment/pse/enviroment_pse/production/srvCode', ScopeInterface::SCOPE_STORE);
        }
    }


}