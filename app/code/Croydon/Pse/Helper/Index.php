<?php


namespace Croydon\Pse\Helper;


use Magento\Framework\App\Config\Initial;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Payment\Model\Config;
use Magento\Payment\Model\Method\Factory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\OrderRepository;
use Magento\Store\Model\App\Emulation;

class Index extends \Magento\Payment\Helper\Data
{



    const SESSION_NAME = 'order_pse_payment';

    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    /**
     * @var OrderInterface
     */
    private $orderInterface;

    public function __construct(
        Context $context,
        LayoutFactory $layoutFactory,
        Factory $paymentMethodFactory,
        Emulation $appEmulation,
        Config $paymentConfig,
        Initial $initialConfig,
        OrderRepository $orderRepository,
        SessionManagerInterface $sessionManager
    )
    {
        parent::__construct($context, $layoutFactory, $paymentMethodFactory, $appEmulation, $paymentConfig, $initialConfig);
        $this->orderRepository = $orderRepository;
        $this->sessionManager = $sessionManager;
    }

    /**
     *
     * @return OrderInterface
     * @throws NoSuchEntityException
     * @throws InputException
     */
    public function getOrder()
    {
        if (!is_null($this->orderInterface)) {
            return $this->orderInterface;
        }
        try {
            return $this->orderInterface = $this->orderRepository->get($this->sessionManager->getData(self::SESSION_NAME));
        } catch (InputException $e) {
            throw new InputException(null, $e);
        } catch (NoSuchEntityException $e) {
            throw new NoSuchEntityException(null, $e);
        }
    }

    /**
     * @return ReferenceArray
     */
    public function getCustomer()
    {
        $referenceArray = new ReferenceArray();
        $referenceArray->setName($this->orderInterface->getCustomerFirstname());
        $referenceArray->setDocumentNumber(123456789);
        $referenceArray->setDocumentType('CC');
        $referenceArray->setEmail($this->orderInterface->getCustomerEmail());
        $referenceArray->setLastName($this->orderInterface->getCustomerLastname());
        $referenceArray->setOrderId($this->orderInterface->getIncrementId());
        $referenceArray->setPhone(3112580141);
        $referenceArray->setRegistrationCode($this->orderInterface->getOriginalIncrementId());
        return $referenceArray;
    }

    /**
     * Obtiene la url de respuesta
     * @return string
     */
    public function getResponseUrl()
    {
        return $this->_urlBuilder->getRouteUrl('pse/respuesta');
    }

    /**
     * Obtiene la url de confirmacion
     * @return string
     */
    public function getConfirmationUrl()
    {
        return $this->_urlBuilder->getRouteUrl('pse/confirmacion');
    }
}