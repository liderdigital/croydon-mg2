<?php


namespace Croydon\Pse\Helper;


use Croydon\Pse\Logger\Logger;
use Croydon\Pse\Model\Payment\Pse;
use Croydon\Pse\Model\ResourceModel\PseOrder\Collection;
use Magento\Sales\Api\OrderRepositoryInterface;

class PaymentInformationManagementHelper
{

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Collection
     */
    protected $collection;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * GuestPaymentInformationManagement constructor.
     * @param Logger $logger
     * @param Collection $collection
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(Logger $logger, Collection $collection, OrderRepositoryInterface $orderRepository)
    {
        $this->logger = $logger;
        $this->collection = $collection;
        $this->orderRepository = $orderRepository;
    }

    public function process($result)
    {
        $order = $this->orderRepository->get($result);
        if ($order->getPayment()->getMethod() == Pse::CODE) {
            $results = $this->collection->getConnection()
                ->select()
                ->from('pse_orders')
                ->columns('*')
                ->where('order_id = ?', $order->getEntityId())
                ->query()
                ->fetchAll(\PDO::FETCH_OBJ);
            $pseOrder = reset($results);
            if (!is_null($pseOrder)) {
                return array(
                    'order' => $result,
                    'url' => $pseOrder->url
                );
            }
        }
        return $result;
    }

}