<?php


namespace Croydon\Pse\Model\Payment;


use Croydon\Clientes\Exceptions\PseServiceException;
use Croydon\Pse\Helper\Data;
use Croydon\Pse\Logger\Logger;
use Croydon\Pse\Model\PseOrder;
use Croydon\Pse\Model\PseOrderFactory;
use Croydon\Pse\Model\ResourceModel\PseOrder\Collection;
use Croydon\Services\Model\PseInterface;
use Magento\Sales\Model\Order;

class Pay
{
    /**
     * @var PseOrderFactory
     */
    protected $pseOrderFactory;

    /**
     * @var PseOrder
     */
    protected $pseOrder;

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var PseInterface
     */
    protected $pseService;

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $data;

    /**
     * Pay constructor.
     * @param PseOrderFactory $pseOrderFactory
     * @param Collection $collection
     * @param PseInterface $pseService
     * @param Logger $logger
     * @param Data $data
     */
    public function __construct(
        PseOrderFactory $pseOrderFactory,
        Collection $collection,
        PseInterface $pseService,
        Logger $logger,
        Data $data
    )
    {
        $this->pseOrderFactory = $pseOrderFactory;
        $this->collection = $collection;
        $this->pseService = $pseService;
        $this->logger = $logger;
        $this->data = $data;
        $this->pseService->setEntityCode($this->data->getEntityCode());
        $this->pseService->setSrvCode($this->data->getSrvCode());
    }


    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    public function pseOrder()
    {
        $collection = $this->collection->addFilter('order_id', $this->order->getId())->load();
        if ($collection->count() > 0) {
            $this->pseOrder = $collection->getFirstItem();
        } else {
            $this->pseOrder = $this->pseOrderFactory->create();
            $this->pseOrder->setData('order_id', $this->order->getId());
        }
    }

    /**
     * @throws PseServiceException
     */
    public function pay()
    {
        $pseService = $this->pseService->createTransactionPayment($this->order)->getCreateTransactionPaymentResult();
        $this->logger->info($this->pseService->__getLastRequest());
        $this->logger->info($this->pseService->__getLastResponse());
        $this->pseOrder->setData('return_code', $pseService->getReturnCode());
        $this->logger->info(sprintf('CODE: %s', $pseService->getReturnCode()));
        if ($pseService->getReturnCode() == 'SUCCESS') {
            $this->pseOrder->setData('ticked_id', $pseService->getTicketId());
            $this->pseOrder->setData('url', $pseService->getECollectUrl());
            return;
        }
        throw new PseServiceException(__('No es posible realizar le pago'));
    }

    /**
     * @throws \Exception
     */
    public function save()
    {
        try {
            $this->pseOrder->save();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @return PseOrder
     */
    public function getPseOrder(): PseOrder
    {
        return $this->pseOrder;
    }


}