<?php


namespace Croydon\Pse\Model\Payment;


use Croydon\Clientes\Exceptions\PseServiceException;
use Magento\Sales\Model\Order;

class PayExecute
{

    /**
     * @var Pay
     */
    protected $pay;

    /**
     * PayExecute constructor.
     * @param Pay $pay
     */
    public function __construct(Pay $pay)
    {
        $this->pay = $pay;
    }


    /**
     * @param Order $order
     * @return \Croydon\Pse\Model\PseOrder
     * @throws PseServiceException
     * @throws \Exception
     */
    public function execute(Order $order)
    {
        try {
            $this->pay->setOrder($order);
            $this->pay->pseOrder();
            $this->pay->pay();
            $this->pay->save();
            return $this->pay->getPseOrder();
        } catch (PseServiceException $e) {
            throw new PseServiceException($e->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }


}