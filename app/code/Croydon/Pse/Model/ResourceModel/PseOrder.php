<?php


namespace Croydon\Pse\Model\ResourceModel;


class PseOrder extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('pse_orders', 'entity_id');
    }
}