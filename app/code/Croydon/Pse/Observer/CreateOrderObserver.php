<?php
/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 14/05/19
 * Time: 9:45
 */

namespace Croydon\Pse\Observer;


use Croydon\Pse\Helper\Index;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Sales\Model\Order;

class CreateOrderObserver implements ObserverInterface
{

    /**
     * @var SessionManagerInterface
     */
    protected $_coreSession;

    /**
     * CreateOrderObserver constructor.
     * @param SessionManagerInterface $coreSession
     */
    public function __construct(SessionManagerInterface $coreSession)
    {
        $this->_coreSession = $coreSession;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getOrder();
        if (!$this->_coreSession->isSessionExists()) {
            $this->_coreSession->start();
        }
        $this->_coreSession->setData(Index::SESSION_NAME, $order->getId());
    }
}