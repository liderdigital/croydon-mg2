<?php


namespace Croydon\Pse\Plugin\Model;


use Croydon\Pse\Helper\PaymentInformationManagementHelper;

class GuestPaymentInformationManagement
{

    /**
     * @var PaymentInformationManagementHelper
     */
    protected $paymentInformationHelper;

    /**
     * GuestPaymentInformationManagement constructor.
     * @param PaymentInformationManagementHelper $paymentInformationHelper
     */
    public function __construct(PaymentInformationManagementHelper $paymentInformationHelper)
    {
        $this->paymentInformationHelper = $paymentInformationHelper;
    }


    /**
     * @param \Magento\Checkout\Model\GuestPaymentInformationManagement $subject
     * @param $result
     * @return mixed
     */
    public function afterSavePaymentInformationAndPlaceOrder(\Magento\Checkout\Model\GuestPaymentInformationManagement $subject, $result)
    {
        return $this->paymentInformationHelper->process($result);
    }

}