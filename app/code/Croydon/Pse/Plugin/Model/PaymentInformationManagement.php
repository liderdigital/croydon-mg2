<?php


namespace Croydon\Pse\Plugin\Model;


use Croydon\Pse\Helper\PaymentInformationManagementHelper;

class PaymentInformationManagement
{

    /**
     * @var PaymentInformationManagementHelper
     */
    protected $paymentInformationHelper;

    /**
     * GuestPaymentInformationManagement constructor.
     * @param PaymentInformationManagementHelper $paymentInformationHelper
     */
    public function __construct(PaymentInformationManagementHelper $paymentInformationHelper)
    {
        $this->paymentInformationHelper = $paymentInformationHelper;
    }

    /**
     * @param \Magento\Checkout\Model\PaymentInformationManagement $subject
     * @param $result
     * @return mixed
     */
    public function afterSavePaymentInformationAndPlaceOrder(\Magento\Checkout\Model\PaymentInformationManagement $subject, $result)
    {
        return $this->paymentInformationHelper->process($result);
    }

}