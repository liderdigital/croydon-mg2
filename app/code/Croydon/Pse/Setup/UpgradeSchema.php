<?php


namespace Croydon\Pse\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * @var string
     */
    const TABLE = 'pse_orders';

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        try {
            $table = $setup->getConnection()->newTable($setup->getTable(self::TABLE))
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['auto_increment' => true, 'identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true,],
                    'Identifier')
                ->addColumn(
                    'order_id',
                    Table::TYPE_TEXT,
                    '255',
                    ['nullable' => false],
                    'Order magento'
                )->addColumn(
                    'ticked_id',
                    Table::TYPE_TEXT,
                    '255',
                    ['nullable' => false],
                    'Order magento'
                )->addColumn(
                    'url',
                    Table::TYPE_TEXT,
                    '400',
                    ['nullable' => true],
                    'url Ecollect'
                )->addColumn(
                    'return_code',
                    Table::TYPE_TEXT,
                    '400',
                    ['nullable' => true],
                    'Response code pse'
                );
            $setup->getConnection()->createTable($table);
            $setup->endSetup();
        } catch (\Zend_Db_Exception $e) {
        }
    }
}