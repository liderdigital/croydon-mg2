define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'pse',
                component: 'Croydon_Pse/js/view/payment/method-renderer/pse-method'
            }
        );
        return Component.extend({});
    }
);