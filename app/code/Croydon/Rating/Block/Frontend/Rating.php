<?php


namespace Croydon\Rating\Block\Frontend;


use Croydon\Rating\Model\ResourceModel\Rating\Collection;
use Magento\Checkout\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Order;

class Rating extends Template
{

    /**
     * @var Order
     */
    private $order;
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * Invoice constructor.
     * @param Template\Context $context
     * @param Session $checkoutSession
     * @param Collection $collection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        Collection $collection,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->checkoutSession = $checkoutSession;
        $this->collection = $collection;
    }

    protected function _beforeToHtml()
    {
        $this->order = $this->checkoutSession->getLastRealOrder();
        return parent::_beforeToHtml(); // TODO: Change the autogenerated stub
    }

    /**
     * @return string
     */
    public function getPostUrl()
    {
        return $this->_urlBuilder->getRouteUrl('rating/post/create');
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order->getId();
    }

    /**
     * @return bool
     */
    public function isAvailableForm()
    {
        $collection = $this->collection
            ->addFilter('sales_order_entity_id', $this->order->getId())
            ->load();
        return $collection->count() <= 0;
    }

}