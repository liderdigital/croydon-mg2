<?php


namespace Croydon\Rating\Controller\Post;


use Croydon\Rating\Model\Rating;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Data\Form\FormKey\Validator;

class Create extends Action
{

    /**
     * @var Rating
     */
    protected $rating;
    /**
     * @var Validator
     */
    protected $validator;


    /**
     * Create constructor.
     * @param Context $context
     * @param Rating $rating
     * @param Validator $validator
     */
    public function __construct(Context $context, Rating $rating, Validator $validator)
    {
        parent::__construct($context);
        $this->rating = $rating;
        $this->validator = $validator;
    }


    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->getRequest()->isPost() || !$this->validator->validate($this->getRequest())) {
            $url = $this->_url->getUrl('efecty', ['_secure' => true]);
            $resultRedirect->setUrl($this->_redirect->error($url));
            return $resultRedirect;
        }
        $params = $this->_request->getParams();
        unset($params['form_key']);
        $this->rating->addData($params);
        try {
            $this->rating->save();
            $this->messageManager->addSuccessMessage(__('Calificación Creada correctamente'));
            $resultRedirect->setPath('efecty');
            return $resultRedirect;
        } catch (\Exception $e) {
            $url = $this->_url->getUrl('efecty', ['_secure' => true]);
            $resultRedirect->setUrl($this->_redirect->error($url));
            return $resultRedirect;
        }
    }
}