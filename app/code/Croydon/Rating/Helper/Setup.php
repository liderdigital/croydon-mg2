<?php


namespace Croydon\Rating\Helper;


use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class Setup
{

    /**
     * @var string
     */
    const table = 'croydon_order_rating';

    /**
     * @var string
     */
    const entity_id = 'entity_id';

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function create(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        try {
            $table = $setup->getConnection()
                ->newTable($setup->getTable(Setup::table))
                ->addColumn(
                    self::entity_id,
                    Table::TYPE_INTEGER,
                    null,
                    ['auto_increment' => true, 'identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true,],
                    'Identifier'
                )->addColumn(
                    'sales_order_entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Order from rating'
                )->addColumn(
                    'rating',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Rating saved'
                )->addColumn(
                    'reason',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Rating saved'
                )->addColumn(
                    'product',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Rating saved'
                )->addColumn(
                    'perception',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Rating saved'
                );
            $setup->getConnection()->createTable($table);
        } catch (\Zend_Db_Exception $e) {
            throw $e;
        }
    }

}