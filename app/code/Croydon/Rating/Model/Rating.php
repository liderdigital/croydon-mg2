<?php


namespace Croydon\Rating\Model;


use Magento\Framework\Model\AbstractModel;

class Rating extends AbstractModel
{

    /**
     * Model construct that should be used for object initialization
     *
     * @return void
     */

    protected function _construct()

    {
        $this->_init(ResourceModel\Rating::class);
    }

}