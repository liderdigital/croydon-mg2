<?php


namespace Croydon\Rating\Model\ResourceModel;


use Croydon\Rating\Helper\Setup;

class Rating extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Setup::table, Setup::entity_id);
    }
}