<?php


namespace Croydon\Rating\Ui\DataProvider;
use Croydon\Rating\Model\ResourceModel\Rating\CollectionFactory;

class RatingDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{


    /**
     * Payment collection
     *
     * @var \Magestore\Inventorypurchasing\Model\ResourceModel\PaymentTerm\Collection
     */

    protected $collection;


    /**
     * Construct
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Ui\DataProvider\AddFieldToCollectionInterface[] $addFieldStrategies
     * @param \Magento\Ui\DataProvider\AddFilterToCollectionInterface[] $addFilterStrategies
     * @param array $meta
     * @param array $data
     */

    public function __construct($name, $primaryFieldName, $requestFieldName, CollectionFactory $collectionFactory, array $meta = [], array $data = [])
    {

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

}