require([
    'jquery'
], function ($) {
    $('#text-error-options').hide();
    $('.encuesta').click(function () {
        $('#text-error-options').hide();
        $('.encuesta').css('background', '');
        $('.encuesta').css('color', '#FFF');
        $(this).css('background', '#FFF');
        $(this).css('color', '#000');
        $(this).css('border', '1px solid');
        $('#rating').val($(this).prop('value'));
    });
    $('#form-encuesta').submit(function (e) {
        var val = parseInt($('#rating').val());
        console.log(val);
        if (val < 0) {
            $('#text-error-options').show();
            e.preventDefault();
        }
    });
});
