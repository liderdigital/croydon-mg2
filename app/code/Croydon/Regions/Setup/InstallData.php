<?php
/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 19/07/19
 * Time: 11:11
 */

namespace Croydon\Regions\Setup;

use Eadesigndev\RomCity\Model\ResourceModel\RomCity;
use Eadesigndev\RomCity\Setup\InstallSchema;
use Magento\Directory\Helper\Data;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    /**
     * Directory data
     *
     * @var Data
     */
    private $directoryData;

    /**
     * Constructor
     *
     * @param Data $directoryData
     */
    public function __construct(Data $directoryData)
    {
        $this->directoryData = $directoryData;
    }

    private $regions = [
        '05' => 'ANTIOQUIA',
        '08' => 'ATLÁNTICO',
        '11' => 'BOGOTÁ, D.C.',
        '13' => 'BOLÍVAR',
        '15' => 'BOYACÁ',
        '17' => 'CALDAS',
        '18' => 'CAQUETÁ',
        '19' => 'CAUCA',
        '20' => 'CESAR',
        '23' => 'CÓRDOBA',
        '25' => 'CUNDINAMARCA',
        '27' => 'CHOCÓ',
        '41' => 'HUILA',
        '44' => 'LA GUAJIRA',
        '47' => 'MAGDALENA',
        '50' => 'META',
        '52' => 'NARIÑO',
        '54' => 'NORTE DE SANTANDER',
        '63' => 'QUINDÍO',
        '66' => 'RISARALDA',
        '68' => 'SANTANDER',
        '70' => 'SUCRE',
        '73' => 'TOLIMA',
        '76' => 'VALLE DEL CAUCA',
        '81' => 'ARAUCA',
        '85' => 'CASANARE',
        '86' => 'PUTUMAYO',
        '88' => 'ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA',
        '91' => 'AMAZONAS',
        '94' => 'GUAINÍA',
        '95' => 'GUAVIARE',
        '97' => 'VAUPÉS',
        '99' => 'VICHADA',
    ];

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        foreach ($this->regions as $code => $name) {
            $binds = ['country_id' => 'CO', 'code' => $code, 'default_name' => $name];
            $setup->getConnection()->insert($setup->getTable('directory_country_region'), $binds);
            $regionId = $setup->getConnection()->lastInsertId($setup->getTable('directory_country_region'));
            $binds = ['locale' => 'es_CO', 'region_id' => $regionId, 'name' => $name];
            $setup->getConnection()->insert($setup->getTable('directory_country_region_name'), $binds);
        }
    }
}