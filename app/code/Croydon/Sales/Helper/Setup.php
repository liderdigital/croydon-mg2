<?php


namespace Croydon\Sales\Helper;


use Magento\Framework\Setup\SchemaSetupInterface;

class Setup
{


    public function addErpCode(SchemaSetupInterface $setup, $table)
    {
        $setup->getConnection()->addColumn(
            $table,
            'erp_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 32,
                'nullable' => true,
                'default' => '',
                'comment' => 'Erp Code'
            ]
        );
    }

}