<?php


namespace Croydon\Sales\Model;

use Magento\SalesRule\Model\RuleFactory;

class Rule
{

    /**
     * @var RuleFactory
     */
    protected $ruleFactory;

    /**
     * Rule constructor.
     * @param RuleFactory $ruleFactory
     */
    public function __construct(RuleFactory $ruleFactory)
    {
        $this->ruleFactory = $ruleFactory;
        $this->ruleFactory->create();
    }


}