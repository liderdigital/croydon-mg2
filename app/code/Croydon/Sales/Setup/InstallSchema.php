<?php


namespace Croydon\Sales\Setup;


use Croydon\Sales\Helper\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup->startSetup();
        $setupDb = new Setup();
        $setupDb->addErpCode($installer, 'salesrule');
        $setupDb->addErpCode($installer, 'catalogrule');
        $installer->endSetup();
    }
}