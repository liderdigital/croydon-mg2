<?php


namespace Croydon\Services\Helper;


use Croydon\Services\Logger\Logger;
use Magento\Framework\App\ObjectManager;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;
use Magento\Tax\Model\TaxClass\Source\Product;

class FactData
{

    /**
     * Order $order
     */
    private $order;

    /**
     * @var \Magento\Sales\Model\Order\Address
     */
    private $billingAddress;

    /**
     * @var \Magento\Sales\Model\Order\Address
     */
    private $shippingAddress;

    /**
     * @var Quote
     */
    private $quote;

    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $connection;
    /**
     * Data constructor.
     * @param $order
     */
    public function __construct(Order $order)
    {

        $this->order = $order;
        $objectManager = ObjectManager::getInstance();
        /** @var Quote $quoteFactory */
        $quoteFactory = $objectManager->create('Magento\Quote\Model\Quote');
        $this->quote = $quoteFactory->load($this->order->getQuoteId());
        $this->billingAddress = $this->order->getBillingAddress();
        $this->shippingAddress = $this->order->getShippingAddress();
        $this->logger = ObjectManager::getInstance()->create(Logger::class);
        $this->connection = ObjectManager::getInstance()->create( \Magento\Framework\App\ResourceConnection::class);
    }


    /**
     * @return string
     */
    public function getNit(): string
    {
        return '8001206812';
    }


    /**
     * @return string
     */
    public function getCompania(): string
    {
        return '01';
    }


    /**
     * @return string
     */
    public function getAno(): string
    {
        return date('Y');
    }

    /**
     * @return string
     */
    public function getModulo(): string
    {
        return '03';
    }


    /**
     * @return string
     */
    public function getCedula(): string
    {
        $document = $this->shippingAddress->getDocument();
        if (is_null($document)) {
            $document = $this->quote->getShippingAddress()->getDocument();
        }
        return $document;
    }


    /**
     * @return string
     */
    public function getTipoDocumento(): string
    {
        $document = $this->shippingAddress->getType();
        if (is_null($document)) {
            $document = $this->quote->getShippingAddress()->getType();
        }
        return strtoupper($document);
    }


    /**
     * @return string
     */
    public function getNombresFactura(): string
    {
        return $this->billingAddress->getFirstname();
    }


    /**
     * @return string
     */
    public function getApellidosFactura(): string
    {
        return $this->billingAddress->getLastname();
    }


    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->billingAddress->getEmail();
    }


    /**
     * @return string
     */
    public function getDireccionFactura(): string
    {
        return $this->billingAddress->getStreet()[0];
    }


    /**
     * @return string
     */
    public function getDireccionDespacho(): string
    {
        return $this->shippingAddress->getStreet()[0];
    }


    /**
     * @return string
     */
    public function getNombresDespacho(): string
    {
        return $this->shippingAddress->getFirstname();
    }


    /**
     * @return string
     */
    public function getApellidosDespacho(): string
    {
        return $this->shippingAddress->getLastname();
    }


    /**
     * @return string
     */
    public function getMunicipioFactura(): string
    {
        $city = $this->connection->getConnection()
            ->fetchRow(sprintf('select * from %s where region_id = %s and city = %s',
                    $this->connection->getTableName('eadesign_romcity'),
                    "'{$this->shippingAddress->getRegionId()}'",
                    "'{$this->shippingAddress->getCity()}'")
            );
        return $city['city_code'];
    }


    /**
     * @return string
     */
    public function getMunicipioDespacho(): string
    {
        // return $this->billingAddress->getCity();
        $city = $this->connection->getConnection()
            ->fetchRow(sprintf('select * from %s where region_id = %s and city = %s',
                    $this->connection->getTableName('eadesign_romcity'),
                    "'{$this->shippingAddress->getRegionId()}'",
                    "'{$this->shippingAddress->getCity()}'")
            );
        return $city['city_code'];
    }


    /**
     * @return string
     */
    public function getPais(): string
    {
        //return $this->shippingAAddress->getCountryId();
        return "0169";
    }


    /**
     * @return string
     */
    public function getTelefonoFactura(): string
    {
        return $this->shippingAddress->getTelephone();
    }


    /**
     * @return string
     */
    public function getCelularFactura(): string
    {
        return $this->billingAddress->getTelephone();
    }


    /**
     * @return string
     */
    public function getTelefonoDespacho(): string
    {
        return $this->shippingAddress->getTelephone();
    }


    /**
     * @return string
     */
    public function getCelularDespacho(): string
    {
        return $this->shippingAddress->getTelephone();
    }


    /**
     * @return string
     */
    public function getCodigoClase(): string
    {
        return 'VW';
    }


    /**
     * @return string
     */
    public function getCodigoCondicionPago(): string
    {
        return '03';
    }


    /**
     * @return string
     */
    public function getCodigoDescuento(): string
    {
         /** @var \Magento\SalesRule\Model\Rule $saleRule */
        $saleRule = \Magento\Framework\App\ObjectManager::getInstance()->create(\Magento\SalesRule\Model\Rule::class);
        $salesruleIds = explode(',', $this->quote->getAppliedRuleIds());
        if (isset($salesruleIds[0])) {
            $saleRule = $saleRule->load($salesruleIds[0]);
        }
        $saleRuleErpId = $saleRule->getErpId();
        if (!is_null($saleRuleErpId) && !empty($saleRuleErpId)) {
            return $saleRuleErpId;
        }
        return ' ';
    }


    /**
     * @return string
     */
    public function getFechaDocumento(): string
    {
        return date('Ymd');
    }


    /**
     * @return string
     */
    public function getOrdenCompra(): string
    {
        $base = 100164870;
        return sprintf('%s%s', 'W', substr($base + (int)$this->quote->getId(), 1, 9));
    }


    /**
     * @return string
     */
    public function getSubtotalBruto(): string
    {
        return round($this->order->getSubtotal());
    }


    /**
     * @return string
     */
    public function getSubtotalFletes(): string
    {
        return round($this->order->getBaseShippingAmount() + $this->order->getSubtotal());
    }


    /**
     * @return string
     */
    public function getTotalCargos(): string
    {
        return round($this->order->getBaseShippingAmount() + $this->order->getSubtotal()+ $this->order->getShippingTaxAmount());
    }


    /**
     * @return string
     */
    public function getIva(): string
    {
        return round($this->order->getTaxAmount());
    }


    /**
     * @return string
     */
    public function getTotalDescuentos(): string
    {
        return round($this->order->getDiscountAmount());
    }


    /**
     * @return string
     */
    public function getTotalNeto(): string
    {
        return round($this->order->getGrandTotal());
    }


    /**
     * @return string
     */
    public function getNumDoc(): string
    {
        $this->logger->info('Quote Id: ' . $this->quote->getId());
        $this->logger->info(sprintf('Aurora Order: %s', $this->quote->getAuroraOrder()));
        return $this->quote->getAuroraOrder();
    }


    /**
     * @return array
     */
    public function getDetalle(): array
    {
        $detail = array();
        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($this->order->getAllVisibleItems() as $item) {
            $tax = $this->getItemTax($item);
            if ($item->getPrice() > 0) {
                $detail[] = array($item->getSku(), round($item->getPriceInclTax()), (int)$item->getQtyOrdered(), $tax);
            }
        }
        return $detail;
    }
    
     /**
     * 
     */
    public function getItemTax(\Magento\Sales\Model\Order\Item $item) {
        /** @var Product $taxClassObj */
        $taxClassObj = ObjectManager::getInstance()->create(Product::class);
        return $taxClassObj->getOptionText($item->getProduct()->getTaxClassId());
    }

}