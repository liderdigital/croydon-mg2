<?php

namespace Croydon\Services\Helper;

use Croydon\Services\Logger\Logger;
use Magento\Framework\App\ObjectManager;
use Magento\Quote\Model\Cart\CartTotalRepository;
use Magento\Quote\Model\Cart\Totals;
use Magento\Quote\Model\Quote;
use Magento\Tax\Model\TaxClass\Source\Product;

class OrderData {

    /**
     * @var Quote
     */
    private $quote;

    /**
     * @var CartTotalRepository
     */
    private $cartTotalRepository;

    /**
     * @var Totals
     */
    private $totals;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $connection;

    /**
     * OrderData constructor.
     * @param Quote $quote
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function __construct(Quote $quote) {
        $this->quote = $quote;
        $this->cartTotalRepository = ObjectManager::getInstance()->create(CartTotalRepository::class);
        $this->totals = $this->cartTotalRepository->get($quote->getId());
        $this->logger = ObjectManager::getInstance()->create(Logger::class);
        $this->connection = ObjectManager::getInstance()->create(\Magento\Framework\App\ResourceConnection::class);
    }

    /**
     * @return string
     */
    public function getNit(): string {
        return '8001206812';
    }

    /**
     * @return string
     */
    public function getCompania(): string {
        return '01';
    }

    /**
     * @return string
     */
    public function getAno(): string {
        return date('Y');
    }

    /**
     * @return string
     */
    public function getModulo(): string {
        return '03';
    }

    /**
     * @return string
     */
    public function getCedula(): string {
        $document = $this->quote->getShippingAddress()->getDocument();
        return $document;
    }

    /**
     * @return string
     */
    public function getTipoDocumento(): string {
        $document = $this->quote->getShippingAddress()->getType();
        return strtoupper($document);
    }

    /**
     * @return string
     */
    public function getNombresFactura(): string {
        $name = $this->quote->getBillingAddress()->getFirstname();
        if (!is_null($name)) {
            return $name;
        }
        return $this->quote->getShippingAddress()->getFirstname();
    }

    /**
     * @return string
     */
    public function getApellidosFactura(): string {
        $lastName = $this->quote->getBillingAddress()->getLastname();
        if (!is_null($lastName)) {
            return $lastName;
        }
        return $this->quote->getShippingAddress()->getLastname();
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        $email = $this->quote->getBillingAddress()->getEmail();
        if (!is_null($email)) {
            return $email;
        }
        return $this->quote->getShippingAddress()->getEmail();
        //return $this->quote->getCustomerEmail();
    }

    /**
     * @return string
     */
    public function getDireccionFactura(): string {

        $street = $this->quote->getBillingAddress()->getStreet();
        if (!is_null($street) && isset($street[0])) {
            return $street[0];
        }
        return $this->quote->getShippingAddress()->getStreet()[0];
    }

    /**
     * @return string
     */
    public function getDireccionDespacho(): string {
        return $this->quote->getShippingAddress()->getStreet()[0];
    }

    /**
     * @return string
     */
    public function getNombresDespacho(): string {
        return $this->quote->getShippingAddress()->getFirstname();
    }

    /**
     * @return string
     */
    public function getApellidosDespacho(): string {
        return $this->quote->getShippingAddress()->getLastname();
    }

    /**
     * @return string
     */
    public function getMunicipioFactura(): string {
        $city = $this->connection->getConnection()
                ->fetchRow(sprintf('select * from %s where region_id = %s and city = %s',
                        $this->connection->getTableName('eadesign_romcity'),
                        "'{$this->quote->getShippingAddress()->getRegionId()}'",
                        "'{$this->quote->getShippingAddress()->getCity()}'")
        );
        return $city['city_code'];
    }

    /**
     * @return string
     */
    public function getMunicipioDespacho(): string {
        // return $this->billingAddress->getCity();
        $city = $this->connection->getConnection()
                ->fetchRow(sprintf('select * from %s where region_id = %s and city = %s',
                        $this->connection->getTableName('eadesign_romcity'),
                        "'{$this->quote->getShippingAddress()->getRegionId()}'",
                        "'{$this->quote->getShippingAddress()->getCity()}'")
        );
        return $city['city_code'];
    }

    /**
     * @return string
     */
    public function getPais(): string {
        //return $this->shippingAAddress->getCountryId();
        return "0169";
    }

    /**
     * @return string
     */
    public function getTelefonoFactura(): string {
        return $this->quote->getShippingAddress()->getTelephone();
    }

    /**
     * @return string
     */
    public function getCelularFactura(): string {
        $phone = $this->quote->getBillingAddress()->getTelephone();
        if (!is_null($phone)) {
            return $phone;
        }
        return $this->quote->getShippingAddress()->getTelephone();
    }

    /**
     * @return string
     */
    public function getTelefonoDespacho(): string {
        return $this->quote->getShippingAddress()->getTelephone();
    }

    /**
     * @return string
     */
    public function getCelularDespacho(): string {
        return $this->quote->getShippingAddress()->getTelephone();
    }

    /**
     * @return string
     */
    public function getCodigoClase(): string {
        return 'VW';
    }

    /**
     * @return string
     */
    public function getCodigoCondicionPago(): string {
        return '03';
    }

    /**
     * @return string
     */
    public function getCodigoDescuento(): string {
        /** @var \Magento\SalesRule\Model\Rule $saleRule */
        $saleRule = \Magento\Framework\App\ObjectManager::getInstance()->create(\Magento\SalesRule\Model\Rule::class);
        $salesruleIds = explode(',', $this->quote->getAppliedRuleIds());
        if (isset($salesruleIds[0])) {
            $saleRule = $saleRule->load($salesruleIds[0]);
        }
        $saleRuleErpId = $saleRule->getErpId();
        if (!is_null($saleRuleErpId) && !empty($saleRuleErpId)) {
            return $saleRuleErpId;
        }
        return ' ';
    }

    /**
     * @return string
     */
    public function getFechaDocumento(): string {
        return date('Ymd');
    }

    /**
     * @return string
     */
    public function getOrdenCompra(): string {
        $base = 100164870;
        return sprintf('%s%s', 'W', substr($base + (int) $this->quote->getId(), 1, 9));
    }

    /**
     * @return string
     */
    public function getSubtotalBruto(): string {
        return round($this->quote->getSubtotal());
    }

    /**
     * @return string
     */
    public function getSubtotalFletes(): string {
        return round($this->quote->getShippingAddress()->getShippingAmount() + $this->quote->getSubtotal());
    }

    /**
     * @return string
     */
    public function getTotalCargos(): string {

        return round($this->totals->getBaseShippingAmount() + $this->quote->getSubtotal() + $this->totals->getShippingTaxAmount());
    }

    /**
     * @return string
     */
    public function getIva(): string {

        return round($this->totals->getTaxAmount());
    }

    /**
     * @return string
     */
    public function getTotalDescuentos(): string {
        return round($this->totals->getDiscountAmount());
    }

    /**
     * @return string
     */
    public function getTotalNeto(): string {
        return round($this->quote->getGrandTotal());
    }

    /**
     * @return array
     */
    public function getDetalle(): array {
        $detail = array();
        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($this->quote->getAllVisibleItems() as $item) {
            $tax = $this->getItemTax($item);
            if ($item->getPrice() > 0) {
                $this->logger->info(sprintf('Qty: %s', $item->getQty()));
                $this->logger->info(sprintf('getQtyOrdered: %s', $item->getQtyOrdered()));
                $detail[] = array($item->getSku(), round($item->getPriceInclTax()), (int) $item->getQty(), $tax);
            }
        }
        return $detail;
    }

    /**
     * 
     */
    public function getItemTax(\Magento\Quote\Model\Quote\Item $item) {
        /** @var Product $taxClassObj */
        $taxClassObj = ObjectManager::getInstance()->create(Product::class);
        return $taxClassObj->getOptionText($item->getProduct()->getTaxClassId());
    }

}
