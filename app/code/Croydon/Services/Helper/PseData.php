<?php


namespace Croydon\Services\Helper;


use Croydon\Pse\Logger\Logger;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order;

class PseData
{

    /** @var Order */
    protected $order;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * Data constructor.
     * @param UrlInterface $url
     */
    public function __construct(UrlInterface $url)
    {
        $this->url = $url;
    }

    /**
     * @return float
     */
    public function getTransValue(): float
    {
        return $this->order->getGrandTotal() - $this->order->getTaxAmount();
    }

    /**
     * @return float
     */
    public function getTransVatValue(): float
    {
        return $this->order->getTaxAmount();
    }

    /**
     * @return string
     */
    public function getSrvCurrency(): string
    {
        return $this->order->getOrderCurrencyCode();
    }

    /**
     * @return string
     */
    public function getURLResponse(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getURLRedirect(): string
    {
        return $this->url->getRouteUrl('pse', array('order_id' => $this->order->getId()));
    }

    /**
     * @return string
     */
    public function getSign(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getSignFields(): string
    {
        return '';
    }

    /**
     * @return string[]
     */
    public function getReferenceArray(): array
    {
        /** @var Logger $logger */
        $logger = ObjectManager::getInstance()->create(Logger::class);
        $billingAddress = $this->order->getBillingAddress();
        $logger->info('Order: ' . $this->order->getAuroraInvoice());
        return array(
            $billingAddress->getDocument(),
            $billingAddress->getType(),
            $this->order->getAuroraInvoice(),
            $billingAddress->getEmail(),
            sprintf('%s %s', $billingAddress->getFirstname(), $billingAddress->getLastname()),
            $billingAddress->getCity(),
            $billingAddress->getRegion(),
            $billingAddress->getStreet()[0],
            $billingAddress->getTelephone(),
            $billingAddress->getTelephone()
        );
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }
}