<?php


namespace Croydon\Services\Logger;

use Magento\Framework\Logger\Handler\Base;
use \Monolog\Logger as MonoLogger;

class Handler extends Base
{

    /**
     * Logging level
     * @var int
     */
    protected $loggerType = MonoLogger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/services.log';

}