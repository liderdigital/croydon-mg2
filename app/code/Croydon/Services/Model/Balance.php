<?php


namespace Croydon\Services\Model;


use Croydon\Services\Logger\BalanceLogger;
use Croydon\Servicios\Aurora\AuroraService;
use Croydon\Servicios\Aurora\FacConsultaEstadoCarteraResponse;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Balance implements BalanceInterface
{

    /**
     * @var BalanceLogger
     */
    private $_logger;

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * FactActOrder constructor.
     * @param BalanceLogger $_logger
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(BalanceLogger $_logger, ScopeConfigInterface $scopeConfig)
    {
        $this->_logger = $_logger;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param $cliente
     * @return FacConsultaEstadoCarteraResponse
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     */
    public function facConsultaEstadoCartera($cliente)
    {
        try {
            $auroraService = new AuroraService($this->scopeConfig->getValue('services/wsdl/inventory'));
            $response = $auroraService->getFacturacionPort()->facConsultaEstadoCartera('8001206812', '01', date('Y'), '03', $cliente);
            $this->_logger->info($auroraService->getFacturacionPort()->__getLastRequest());
            $this->_logger->info($auroraService->getFacturacionPort()->__getLastResponse());
            return $response;
        } catch (InvokeServiceException $e) {
            throw new InvokeServiceException($e);
        } catch (MethodNotFoundException $e) {
            throw new MethodNotFoundException($e->getMessage());
        }
    }
}