<?php


namespace Croydon\Services\Model;


use Croydon\Servicios\Aurora\FacConsultaEstadoCarteraResponse;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;

interface BalanceInterface
{

    /**
     * @param $cliente
     * @return FacConsultaEstadoCarteraResponse
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     */
    public function facConsultaEstadoCartera($cliente);

}