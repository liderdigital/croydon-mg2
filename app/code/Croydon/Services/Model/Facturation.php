<?php


namespace Croydon\Services\Model;


use Croydon\Services\Helper\FactData;
use Croydon\Services\Helper\OrderData;
use Croydon\Services\Logger\Logger;
use Croydon\Servicios\Facturacion\FacActOrdenPedidoResponse;
use Croydon\Servicios\Facturacion\FacturacionService;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;

class Facturation implements FacturationInterface
{


    /**
     * @var Logger
     */
    private $_logger;

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * @var FacturacionService
     */
    protected $facturacionService;

    /**
     * FactActOrder constructor.
     * @param Logger $_logger
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(Logger $_logger, ScopeConfigInterface $scopeConfig)
    {
        $this->_logger = $_logger;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Quote $order
     * @return FacActOrdenPedidoResponse
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function factAct(Quote $order)
    {
        $wsdl = $this->scopeConfig->getValue('services/wsdl/invoice');
        $data = $this->getOrderData($order);
        $this->_logger->info($wsdl);
        $this->facturacionService = new FacturacionService($wsdl);
        try {
            $service = $this->facturacionService->getFacturacionPort()->facActOrdenPedido(
                $data->getNit(),
                $data->getCompania(),
                $data->getAno(),
                $data->getModulo(),
                $data->getCedula(),
                $data->getTipoDocumento(),
                $data->getNombresFactura(),
                $data->getApellidosFactura(),
                $data->getEmail(),
                $data->getDireccionFactura(),
                $data->getDireccionDespacho(),
                $data->getNombresDespacho(),
                $data->getApellidosDespacho(),
                $data->getMunicipioFactura(),
                $data->getMunicipioDespacho(),
                $data->getPais(),
                $data->getTelefonoFactura(),
                $data->getCelularFactura(),
                $data->getTelefonoDespacho(),
                $data->getCelularDespacho(),
                $data->getCodigoClase(),
                $data->getCodigoCondicionPago(),
                $data->getCodigoDescuento(),
                $data->getFechaDocumento(),
                $data->getOrdenCompra(),
                $data->getSubtotalBruto(),
                $data->getSubtotalFletes(),
                $data->getTotalCargos(),
                $data->getIva(),
                $data->getTotalDescuentos(),
                $data->getTotalNeto(),
                $data->getDetalle()
            );
            $this->_logger->info('1: ' . $this->facturacionService->getFacturacionPort()->__getLastRequest());
            $this->_logger->info('1: ' . $this->facturacionService->getFacturacionPort()->__getLastResponse());
            return $service;
        } catch (InvokeServiceException $e) {
            $this->_logger->info(sprintf('InvokeServiceException %s; %s', sprintf('%s:%s', __METHOD__, __LINE__), $e->getMessage()));
            throw new InvokeServiceException($e);
        } catch (MethodNotFoundException $e) {
            $this->_logger->info(sprintf('MethodNotFoundException %s; %s', sprintf('%s:%s', __METHOD__, __LINE__), $e->getMessage()));
            throw new MethodNotFoundException($e->getMessage());
        }
    }

    /**
     * @param Order $order
     * @param $idAurora
     * @return FacActOrdenPedidoResponse
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     */
    public function factActFact(Order $order)
    {
        $data = $this->getData($order);
        $this->facturacionService = new FacturacionService($this->scopeConfig->getValue('services/wsdl/invoice'));
        try {
            $service = $this->facturacionService->getFacturacionPort()->facActFactura(
                $data->getNit(),
                $data->getCompania(),
                $data->getAno(),
                $data->getModulo(),
                $data->getCedula(),
                $data->getTipoDocumento(),
                $data->getNombresFactura(),
                $data->getApellidosFactura(),
                $data->getEmail(),
                $data->getDireccionFactura(),
                $data->getDireccionDespacho(),
                $data->getNombresDespacho(),
                $data->getApellidosDespacho(),
                $data->getMunicipioFactura(),
                $data->getMunicipioDespacho(),
                $data->getPais(),
                $data->getTelefonoFactura(),
                $data->getCelularFactura(),
                $data->getTelefonoDespacho(),
                $data->getCelularDespacho(),
                $data->getCodigoClase(),
                $data->getCodigoCondicionPago(),
                $data->getCodigoDescuento(),
                $data->getFechaDocumento(),
                $data->getOrdenCompra(),
                $data->getSubtotalBruto(),
                $data->getSubtotalFletes(),
                $data->getTotalCargos(),
                $data->getIva(),
                $data->getTotalDescuentos(),
                $data->getTotalNeto(),
                $data->getNumDoc(),
                $data->getDetalle()
            );
            $this->_logger->info('2: ' . $this->facturacionService->getFacturacionPort()->__getLastRequest());
            $this->_logger->info('2: ' . $this->facturacionService->getFacturacionPort()->__getLastResponse());
            return $service;
        } catch (InvokeServiceException $e) {
            $this->_logger->info(sprintf('InvokeServiceException %s; %s', sprintf('%s:%s', __METHOD__, __LINE__), $e->getMessage()));
            throw new InvokeServiceException($e);
        } catch (MethodNotFoundException $e) {
            $this->_logger->info(sprintf('MethodNotFoundException %s; %s', sprintf('%s:%s', __METHOD__, __LINE__), $e->getMessage()));
            throw new MethodNotFoundException($e->getMessage());
        }
    }


    /**
     * @param $order
     * @return FactData
     */
    protected function getData($order)
    {
        return new FactData($order);
    }

    /**
     * @param $quote
     * @return OrderData
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getOrderData($quote)
    {
        return new OrderData($quote);
    }


}