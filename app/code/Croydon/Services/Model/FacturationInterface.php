<?php


namespace Croydon\Services\Model;


use Croydon\Servicios\Facturacion\FacActOrdenPedidoResponse;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;

interface FacturationInterface
{

    /**
     * @param Quote $order
     * @return FacActOrdenPedidoResponse
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     */
    public function factAct(Quote $order);

    /**
     * @param Order $order
     * @return FacActOrdenPedidoResponse
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     */
    public function factActFact(Order $order);

}