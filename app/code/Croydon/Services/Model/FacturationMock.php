<?php


namespace Croydon\Services\Model;


use Croydon\Checkout\Logger\Logger;
use Croydon\Servicios\Facturacion\FacActOrdenPedidoResponse;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;

class FacturationMock implements FacturationInterface
{

    /**
     * @var Logger
     */
    private $looger;

    /**
     * FacturationMock constructor.
     * @param Logger $looger
     */
    public function __construct(Logger $looger)
    {
        $this->looger = $looger;
    }


    /**
     * @param Quote $quote
     * @return FacActOrdenPedidoResponse
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     */
    public function factAct(Quote $quote)
    {
        $this->looger->info('FacturatoinMock: ' . $quote->getIncrementId());
        $response = new FacActOrdenPedidoResponse();
        $response->setNumdoc($quote->getIncrementId());
        $response->setCoddoc('PE');
        $response->setMsg('Success');
        return $response;
    }

    /**
     * @param Order $order
     * @return FacActOrdenPedidoResponse
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     */
    public function factActFact(Order $order)
    {
        $response = new FacActOrdenPedidoResponse();
        $this->looger->info('Aurora Response Invoice: ' . $order->getIncrementId());
        $response->setNumdoc('CR-' . $order->getIncrementId());
        $response->setCoddoc('FA');
        $response->setMsg('Success');
        return $response;
    }
}