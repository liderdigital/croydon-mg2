<?php


namespace Croydon\Services\Model;


use Croydon\Services\Logger\LoggerInventory;
use Croydon\Servicios\Aurora\AuroraService;
use Croydon\Servicios\Aurora\ErrorResponse;
use Croydon\Servicios\Aurora\InvConsultaDispResponse;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Magento\Framework\App\Config\ScopeConfigInterface;

class inventory implements InventoryInterface
{

    /**
     * @var LoggerInventory
     */
    private $_logger;

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * InventoryQuery constructor.
     * @param LoggerInventory $logger
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(LoggerInventory $logger, ScopeConfigInterface $scopeConfig)
    {
        $this->_logger = $logger;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param $sku
     * @param $qty
     * @return InvConsultaDispResponse|ErrorResponse
     * @throws MethodNotFoundException
     * @throws InvokeServiceException
     */
    public function invConsultaDispItemBod($sku, $qty)
    {
        try {
            $auroraService = new AuroraService($this->scopeConfig->getValue('services/wsdl/inventory'));
            $response = $auroraService->getFacturacionPort()->invConsultaDispItemBod('8001206812', '01', date('Y'), '03', $sku, $qty);
            $this->_logger->info($auroraService->getFacturacionPort()->__getLastRequest());
            $this->_logger->info($auroraService->getFacturacionPort()->__getLastResponse());
            return $response;
        } catch (InvokeServiceException $e) {
            $this->_logger->info(sprintf('Error Servicios: %s, %s', $sku, $qty));
            throw new InvokeServiceException($e);
        } catch (MethodNotFoundException $e) {
            $this->_logger->info(sprintf('Error Servicios: %s, %s', $sku, $qty));
            throw new MethodNotFoundException($e);
        }
    }
}