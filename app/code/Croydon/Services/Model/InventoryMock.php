<?php


namespace Croydon\Services\Model;


use Croydon\Servicios\Aurora\ErrorResponse;
use Croydon\Servicios\Aurora\InvConsultaDispResponse;
use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;

class InventoryMock implements InventoryInterface
{

    /**
     * @param $sku
     * @param $qty
     * @return InvConsultaDispResponse|ErrorResponse
     * @throws MethodNotFoundException
     * @throws InvokeServiceException
     */
    public function invConsultaDispItemBod($sku, $qty)
    {
        $invConsultaDispResponse = new InvConsultaDispResponse();
        $invConsultaDispResponse->setCantDisponible($qty);
        return $invConsultaDispResponse;
    }
}