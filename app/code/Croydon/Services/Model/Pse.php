<?php


namespace Croydon\Services\Model;


use Croydon\Service\Pse\createTransactionPayment;
use Croydon\Service\Pse\createTransactionPaymentResponse;
use Croydon\Service\Pse\createTransactionType;
use Croydon\Service\Pse\ECollectWebservicesv2;
use Croydon\Service\Pse\getTransactionInformation;
use Croydon\Service\Pse\getTransactionInformationResponse;
use Croydon\Service\Pse\getTransactionInformationType;
use Croydon\Services\Helper\PseData;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order;
use SoapFault;

class Pse implements PseInterface
{


    /**
     * @var ECollectWebservicesv2
     */
    private $ecollect;

    /**
     * @var string
     */
    private $srvCode;

    /**
     * @var string
     */
    private $entityCode;

    /**
     * @var PseData
     */
    private $data;

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * PseService constructor.
     * @param PseData $data
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(PseData $data, ScopeConfigInterface $scopeConfig)
    {
        $this->data = $data;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Order $order
     * @return createTransactionPaymentResponse
     * @throws SoapFault
     */
    public function createTransactionPayment(Order $order)
    {
        $this->data->setOrder($order);
        $createTransactionType = new createTransactionType($this->data->getTransValue(), $this->data->getTransVatValue());
        $createTransactionType->setEntityCode($this->entityCode);
        $createTransactionType->setSrvCode($this->srvCode);
        $createTransactionType->setReferenceArray($this->data->getReferenceArray());
        $createTransactionType->setSign($this->data->getSign());
        $createTransactionType->setSignFields($this->data->getSignFields());
        $createTransactionType->setSrvCurrency($this->data->getSrvCurrency());
        $createTransactionType->setURLRedirect($this->data->getURLRedirect());
        $createTransactionType->setURLResponse($this->data->getURLResponse());
        $createTransaction = new createTransactionPayment($createTransactionType);
        $this->ecollect = new ECollectWebservicesv2(array(), $this->scopeConfig->getValue('services/wsdl/pse'));
        return $this->ecollect->createTransactionPayment($createTransaction);
    }

    /**
     * @param string $entityCode
     * @param string $ticketId
     * @return getTransactionInformationResponse
     * @throws SoapFault
     */
    public function getTransactionInformation(string $entityCode, string $ticketId)
    {
        $request = new getTransactionInformationType();
        $request->setEntityCode($entityCode);
        $request->setTicketId($ticketId);
        $getTransactionInformation = new getTransactionInformation($request);
        $this->ecollect = new ECollectWebservicesv2(array(), $this->scopeConfig->getValue('services/wsdl/pse'));
        $response = $this->ecollect->getTransactionInformation($getTransactionInformation);
        return $response;
        //if ($response instanceof createTransactionPaymentResponse)
        //    return $response;
        //throw new SoapFault($response->getCode(), $response->getMessage());
    }

    /**
     * @param string $entityCode
     * @return string
     */
    public function setEntityCode(string $entityCode)
    {
        $this->entityCode = $entityCode;
    }

    /**
     * @param string $srvCode
     * @return string
     */
    public function setSrvCode(string $srvCode)
    {
        $this->srvCode = $srvCode;
    }

    /**
     * @return string
     */
    public function __getLastRequest()
    {
        return $this->ecollect->__getLastRequest();
    }

    /**
     * @return string
     */
    public function __getLastResponse()
    {
        return $this->ecollect->__getLastResponse();
    }
}