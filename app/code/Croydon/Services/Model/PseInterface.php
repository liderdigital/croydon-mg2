<?php


namespace Croydon\Services\Model;


use Croydon\Service\Pse\createTransactionPaymentResponse;
use Croydon\Service\Pse\getTransactionInformationResponse;
use Magento\Sales\Model\Order;

interface PseInterface
{

    /**
     * @param Order $order
     * @return createTransactionPaymentResponse
     */
    public function createTransactionPayment(Order $order);

    /**
     * @param string $entityCode
     * @param string $ticketId
     * @return getTransactionInformationResponse
     */
    public function getTransactionInformation(string $entityCode, string $ticketId);


    /**
     * @param string $entityCode
     * @return string
     */
    public function setEntityCode(string $entityCode);

    /**
     * @param string $srvCode
     * @return string
     */
    public function setSrvCode(string $srvCode);

    /**
     * @return string
     */
    public function __getLastRequest();

    /**
     * @return string
     */
    public function __getLastResponse();

}