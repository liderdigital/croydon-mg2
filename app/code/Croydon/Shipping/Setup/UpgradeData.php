<?php


namespace Croydon\Shipping\Setup;


use Croydon\Regions\Setup\SetupAux;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->getConnection()->delete($setup->getTable('eadesign_romcity'), 'entity_id > 0');
        $setupAux = new SetupAux();
        $data = array();
        foreach ($setupAux->cities as $city) {
            $deparmentCode = substr($city['codigo'], 0, 2);
            $sql = sprintf('SELECT * FROM `%s` WHERE country_id=%s and code = %s', $setup->getTable('directory_country_region'), "'CO'", "'{$deparmentCode}'");
            $region = $setup->getConnection()->fetchRow($sql);
            $array = array(
                'region_id' => $region['region_id'],
                'city' => $city['label'],
                'city_code' => $city['codigo'],
                'price' => (float)$city['valor_flete']
            );
            $data[] = $array;
        }
        $setup->getConnection()->insertMultiple($setup->getTable('eadesign_romcity'), $data);
    }


}