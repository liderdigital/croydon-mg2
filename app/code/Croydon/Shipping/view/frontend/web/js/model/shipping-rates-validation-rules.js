define(
    [],
    function () {
        'use strict';
        return {
            getRules: function() {
                return {
                    'city': {
                        'required': true
                    },
                    'region_id': {
                        'required': true
                    }
                };
            }
        };
    }
)