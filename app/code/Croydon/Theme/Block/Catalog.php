<?php


namespace Croydon\Theme\Block;


use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Category\Interceptor;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;

class Catalog extends \Magento\Catalog\Block\Product\View\Description
{

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var array
     */
    private $childProducts = null;
    /**
     * @var array
     */
    protected $metaKeywords = null;
    protected $categories = null;
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    public function __construct(
        Context $context,
        Registry $registry,
        ProductRepositoryInterface $productRepository,
        CategoryRepository $categoryRepository,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $data);
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }


    /**
     * @return array
     */
    public function getChildProducts()
    {
        if (!is_null($this->childProducts)) {
            return $this->childProducts;
        }
        $this->childProducts = array();
        $children = $this->_product->getTypeInstance()->getChildrenIds($this->_product->getId());
        $this->_logger->info(json_encode($children));
        foreach ($children[0] as $childrenId) {
            try {
                $product = $this->productRepository->getById($childrenId);
                if (!is_null($product->getCustomAttribute('color'))) {
                    $this->childProducts[] = $product->getAttributeText('color');
                }
            } catch (NoSuchEntityException $e) {
                return array();
            }
        }
        $return = array_unique($this->childProducts);
        $this->childProducts = array();
        foreach ($return as $item) {
            $this->childProducts[] = $item;
        }
        return $this->childProducts;
    }

    public function getMetaKeywords()
    {
        if (!is_null($this->metaKeywords)) {
            return $this->metaKeywords;
        }
        if (strlen(trim($this->_product->getMetaKeyword())) <= 0) {
            return array();
        }
        $keywords = explode(',', trim($this->_product->getMetaKeyword()));
        $this->metaKeywords = array();
        foreach ($keywords as $keyword) {
            $this->metaKeywords[] = trim($keyword);
        }
        return $this->metaKeywords;
    }


    /**
     * @return \Magento\Framework\Data\Collection|null
     */
    public function getCategories()
    {
        if (is_null($this->categories)) {
            $this->categories = $this->_product->getCategoryCollection();
        }
        return $this->categories;
    }

    /**
     * @param Interceptor $interceptor
     * @return \Magento\Catalog\Api\Data\CategoryInterface|mixed
     * @throws \Exception
     */
    public function getCategory(Interceptor $interceptor)
    {
        try {
            $category = $this->categoryRepository->get($interceptor->getEntityId());
            return $category;
        } catch (NoSuchEntityException $e) {
            throw new \Exception($e->getMessage());
        }
    }
}