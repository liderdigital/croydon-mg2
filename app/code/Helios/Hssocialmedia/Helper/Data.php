<?php
namespace Helios\Hssocialmedia\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
   protected $scopeConfig;
   protected $storeScope;
   
   public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
   {
      $this->scopeConfig = $scopeConfig;
      $this->storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
   }
    /**
     * Check Module is Enable or Disable
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
   
    public function getModuleStatus()
    {
        return $this->scopeConfig->getValue('hsmedia/mediasetting/hsmedia_enabled', $this->storeScope);
    }
	/**
     * Check we want to Include JS or not
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getJsStatus()
    {                
        return $this->scopeConfig->getValue('hsmedia/mediasetting/jquery_yesno', $this->storeScope);
    }
	/**
     * get Share Button Positions
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getButtonPosition()
    {                
        return $this->scopeConfig->getValue('hsmedia/mediasetting/buttonposition', $this->storeScope);
    }
	/**
     * get Margin 
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getMargin()
    {         
        return $this->scopeConfig->getValue('hsmedia/mediasetting/buttonmargin', $this->storeScope);       
    }
	/**
     * get Button Effect
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getEffect()
    {           
        return $this->scopeConfig->getValue('hsmedia/mediasetting/buttoneffect', $this->storeScope);     		
    }
	/**
     * get facebook link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getFacebookUrl()
    {         
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/facebookurl', $this->storeScope);       		
    }
    public function getFacebookTitle()
    {              
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/facebooktitle', $this->storeScope);  
    }
	/**
     * get Linkedin link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getLinkidinUrl()
    {   
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/linkedinurl', $this->storeScope);             		
    }
    public function getLinkidinTitle()
    {    
            return $this->scopeConfig->getValue('hsmedia/hsmediashare/linkedintitle', $this->storeScope);            		
    }
	/**
     * get Twitter link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getTwitterUrl()
    {     
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/twitterurl', $this->storeScope);           		
    }
    public function getTwitterTitle()
    {    
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/twittertitle', $this->storeScope);            		
    }
	/**
     * get Youtube link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getYoutubeUrl()
    {    
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/youtubeurl', $this->storeScope);            		
    }
    public function getYoutubeTitle()
    {     
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/youtubetitle', $this->storeScope);           		
    }
	/**
     * get Google + link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getGoogleUrl()
    {    
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/googleplusurl', $this->storeScope);           		
    }
    public function getGoogleTitle()
    {      
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/googleplustitle', $this->storeScope);          		
    }
	/**
     * get Pinterest + link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
    */
    public function getPinterestUrl()
    {    
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/pinteresturl', $this->storeScope);           		
    }
    public function getPinterestTitle()
    {      
          return $this->scopeConfig->getValue('hsmedia/hsmediashare/pinteresttitle', $this->storeScope);          		
    }
    /**
     * get Instagram + link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
     */
    public function getInstagramUrl()
    {
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/instagramurl', $this->storeScope);
    }
    public function getInstagramTitle()
    {
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/instagramtitle', $this->storeScope);
    }
    /**
     * get Flickr + link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
     */
    public function getFlickrUrl()
    {
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/flickrurl', $this->storeScope);
    }
    public function getFlickrTitle()
    {
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/flickrtitle', $this->storeScope);
    }
    /**
     * get Website + link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
     */
    public function getWebUrl()
    {
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/weburl', $this->storeScope);
    }
    public function getWebTitle()
    {
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/webtitle', $this->storeScope);
    }
    /**
     * get Email Address + link Details
     *
     * @return Helios_Hssocialmedia_Helper_Data
     */
    public function getMailUrl()
    {
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/webmail', $this->storeScope);
    }
    public function getMailTitle()
    {
        return $this->scopeConfig->getValue('hsmedia/hsmediashare/webmailtitle', $this->storeScope);
    }
}