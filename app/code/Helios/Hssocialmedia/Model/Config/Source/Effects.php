<?php

namespace Helios\Hssocialmedia\Model\Config\Source;

class Effects implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return array(
		
            array('value' => 'swing', 'label'=>__('swing')),
            array('value' => 'easeOutQuad', 'label'=>__('easeOutQuad')),
            array('value' => 'easeOutCirc', 'label'=>__('easeOutCirc')),
            array('value' => 'easeOutElastic', 'label'=>__('easeOutElastic')),
            array('value' => 'easeOutExpo', 'label'=>__('easeOutExpo')),
        );
    }
}
