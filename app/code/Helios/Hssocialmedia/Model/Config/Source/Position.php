<?php

namespace Helios\Hssocialmedia\Model\Config\Source;

class Position implements \Magento\Framework\Option\ArrayInterface
{
   /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
		
            array('value' => 1, 'label'=>__('Right')),
            array('value' => 2, 'label'=>__('Left')),
        );
    }
}
