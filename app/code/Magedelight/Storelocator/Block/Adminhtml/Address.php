<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Block\Adminhtml;

use \Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Address extends Template
{

    /**
     *
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * call template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('storelocator/address.phtml');
    }
}
