<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Block\Adminhtml;

use \Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magedelight\Storelocator\Model\Source\IsOpen;

/**
 * Adminhtml Store time renderer
 */
class Storetime extends Template
{

    /**
     * @var string
     */
    protected $_template = 'storelocator/storetime.phtml';

    /**
     *
     * @param Context $context
     * @param IsOpen $IsOpen
     */
    public function __construct(
        Context $context,
        IsOpen $IsOpen
    ) {
        $this->IsOpen = $IsOpen;
        parent::__construct($context);
    }

    /**
     * Call Template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('storelocator/storetime.phtml');
    }

    /**
     * Retrieve list of initial customer groups
     *
     * @return array
     */
    protected function _getInitialCustomerGroups()
    {
        return [$this->_groupManagement->getAllCustomersGroup()->getId() => __('ALL GROUPS')];
    }

    /**
     *
     * @return Array
     */
    public function getDays()
    {
        return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    }

    /**
     *
     * @return Array
     */
    public function getIsOpen()
    {
        return $this->IsOpen->getOptions();
    }
}
