<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Block;

use \Magento\Framework\View\Element\Template;
use Magedelight\Storelocator\Model\StorelocatorFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlFactory;
use Magedelight\Storelocator\Model\Source\Country;
use Magedelight\Storelocator\Model\Source\Region;
use Magedelight\Storelocator\Model\Storelocator\Image as ImageModel;
use Magento\Store\Model\ScopeInterface;

class Singlestorelocator extends Template
{

    /**
     * Default image for storelist
     */
    const XML_PATH_STORE_IMAGE = 'magedelight_storelocator/storeinfo/logo';

    /**
     *  Distance in Kilometer/Miles
     */
    const XML_PATH_STORE_DISTANCE = 'magedelight_storelocator/storesearch/distanceunit';
    
    /* Google Map Api Key */
    const XML_PATH_STORE_MAP_API_KEY = 'magedelight_storelocator/googlemap/mapapi';

    /* Map Marker Image*/
     const XML_PATH_STORE_MARKER_IMAGE ='magedelight_storelocator/googlemap/markericon';

    protected $_modelstorelocatorFactory;
    protected $urlFactory;
    protected $countryOptions;
    protected $scopeConfig;

    /**
     * image model
     *
     * @var \Magedelight\Storelocator\Model\Storelocator\Image
     */
    protected $imageModel;

    /**
     *
     * @param Context $context
     * @param Registry $registry
     * @param Country $countryOptions
     * @param ImageModel $imageModel
     * @param UrlFactory $urlFactory
     * @param StorelocatorFactory $modelStorelocatorFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Country $countryOptions,
        Region $regionOptions,
        ImageModel $imageModel,
        UrlFactory $urlFactory,
        StorelocatorFactory $modelStorelocatorFactory
    ) {
        $this->_modelstorelocatorFactory = $modelStorelocatorFactory;
        $this->urlFactory = $urlFactory;
        $this->countryOptions = $countryOptions;
        $this->regionOptions = $regionOptions;
        $this->registry = $registry;
        $this->imageModel = $imageModel;
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }

    /**
     *
     * @return \Magedelight\Storelocator\Block\Singlestorelocator
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($this->getStoreTitle());
        }

        return $this;
    }

    /**
     *
     * @param string $countryval
     * @return string
     */
    public function getCountryName($countryval)
    {
        $countryArray = $this->countryOptions->getOptions();
        return $countryArray[$countryval];
    }

    /**
     *
     * @return Array
     */
    public function getSingleStoreData()
    {
        $storelocatorid = $this->registry->registry('storelocatorid');
        $storelocatorModel = $this->_modelstorelocatorFactory->create();
        $storelocatorCollection = $storelocatorModel->getCollection();
        $storelocatorCollection->addFieldToFilter('storelocator_id', $storelocatorid);
        $storelocatorData = $storelocatorCollection->getData();
        return $storelocatorData;
    }

    /**
     *
     * @return string
     */
    public function getBackUrl()
    {
        $fronturl = $this->scopeConfig->getValue(
            'magedelight_storelocator/listviewinfo/frontend_url',
            ScopeInterface::SCOPE_STORES
        );

        return $fronturl;
    }

    /**
     *
     * @param type $storelocatorid
     * @return string
     */
    public function getStoreImage($storelocatorid)
    {
        $storelocatorModel = $this->_modelstorelocatorFactory->create();
        $storelocatorModel->load($storelocatorid);
        $storeimage = $storelocatorModel->getStoreimage();
        $storename = $storelocatorModel->getStorename();

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_config_image = $this->scopeConfig->getValue(self::XML_PATH_STORE_IMAGE, $storeScope);
        if ($storeimage != '') {
            $imagehtml = $this->imageModel->getBaseUrl() . $storeimage;
            return $imagehtml;
        } elseif ($_config_image != '') {
            return $this->imageModel->getBaseUrl() . '/' . $_config_image;
        } else {
            return $this->getViewFileUrl('Magedelight_Storelocator::images/image-default.png');
        }
    }

    /**
     *
     * @param type $storelocatorid
     * @return string
     */
    public function getContactInfo($storelocatorid)
    {
        $storelocatorModel = $this->_modelstorelocatorFactory->create();
        $storelocatorModel->load($storelocatorid);
        $phone = $storelocatorModel->getTelephone();
        $phone_frontend_status = $storelocatorModel->getPhoneFrontendStatus();
        $html = "";

        if ($phone_frontend_status) {
            if ($phone != '') {
                $html .= "<tr><td><strong>" . __("Phone no:") . "</strong></td><td>";
                $televalue = explode(':', $phone);
                foreach ($televalue as $key => $singlevalue) {
                    $html .= "<p>" . $singlevalue . "</p>";
                }
                $html .= "</td></tr>";
            }
        }
        return $html;
    }

    /**
     *
     * @return String
     */
    public function getStoreTitle()
    {
        $_storeData = $this->getSingleStoreData();
        return $_storeData[0]['storename'];
    }

    /**
     *
     * @return string
     */
    public function getDistanceUnit()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_config_DistanceUnit = $this->scopeConfig->getValue(self::XML_PATH_STORE_DISTANCE, $storeScope);
        return $_config_DistanceUnit;
    }
    
    /**
     *
     * @return String
     */
    public function getGoogleMapApiKey()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_Map_Api_Key = $this->scopeConfig->getValue(self::XML_PATH_STORE_MAP_API_KEY, $storeScope);
        return $_Map_Api_Key;
    }

    /**
     *
     * @return string
     */
    public function getMarkerImage()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_marker_image = $this->scopeConfig->getValue(self::XML_PATH_STORE_MARKER_IMAGE, $storeScope);
        $_marker_image_Url = $this->imageModel->getBaseUrl() . '/' . $_marker_image;
        if (empty($_marker_image)) {
            return '';
        }
        return $_marker_image_Url;
    }

    /**
     *
     * @param string $regionvalue
     * @return string
     */
    public function getRegionName($region_id)
    {
        $regionArray = $this->regionOptions->getOptions();
        return $regionArray[$region_id];
    }
    
    public function getWysiwygImage($content)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create('\Magento\Cms\Model\Template\FilterProvider');
        $filterManager = $model->getPageFilter()->filter($content);
        return $filterManager;
    }
}
