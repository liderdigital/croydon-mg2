<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Block;

use \Magento\Framework\View\Element\Template;
use Magedelight\Storelocator\Model\StorelocatorFactory;
use Magedelight\Storelocator\Model\StoreholidayFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlFactory;
use Magedelight\Storelocator\Model\Source\Country;
use Magedelight\Storelocator\Model\Storelocator\Image as ImageModel;
use Magento\Store\Model\ScopeInterface;

class Storeholiday extends Template
{

    
    protected $_modelstorelocatorFactory;
    protected $urlFactory;
    protected $countryOptions;
    protected $scopeConfig;

    /**
     * image model
     *
     * @var \Magedelight\Storelocator\Model\Storelocator\Image
     */
    protected $imageModel;

    /**
     *
     * @param Context $context
     * @param Registry $registry
     * @param Country $countryOptions
     * @param ImageModel $imageModel
     * @param UrlFactory $urlFactory
     * @param StorelocatorFactory $modelStorelocatorFactory
     * @param StoreholidayFactory $storeholidayFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Country $countryOptions,
        ImageModel $imageModel,
        UrlFactory $urlFactory,
        StorelocatorFactory $modelStorelocatorFactory,
        StoreholidayFactory $storeholidayFactory
    ) {
        $this->_modelstorelocatorFactory = $modelStorelocatorFactory;
        $this->storeholidayFactory = $storeholidayFactory;
        $this->urlFactory = $urlFactory;
        $this->countryOptions = $countryOptions;
        $this->registry = $registry;
        $this->imageModel = $imageModel;
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }

    
    /**
     *
     * @return Array
     */
    public function getSingleStoreData()
    {
        $storelocatorid = $this->registry->registry('storelocatorid');
        $storelocatorModel = $this->_modelstorelocatorFactory->create();
        $storelocatorCollection = $storelocatorModel->getCollection();
        $storelocatorCollection->addFieldToFilter('storelocator_id', $storelocatorid);
        $storelocatorData = $storelocatorCollection->getData();
        return $storelocatorData;
    }

    /**
     *
     * @param type $storeId
     * @return array
     */
    public function getHoliday($storeId)
    {
        $storelocatorModel = $this->storeholidayFactory->create();
        $storelocatorCollection = $storelocatorModel->getCollection();
        $storelocatorCollection->addFieldToFilter('is_active', '1');
        $_storecollection = $this->getStoreFilter($storelocatorCollection, $storeId);
        $_holidaydays = $this->getStoreHolidays($_storecollection);
        return $_holidaydays;
    }

    /**
     *
     * @param object $storelocatorCollection
     * @param int $storeId
     * @return Array
     */
    public function getStoreFilter($storelocatorCollection, $storeId)
    {
        $_storecollection = $storelocatorCollection->getData();
        $_elementkey = 0;
        foreach ($storelocatorCollection as $collection) {
            $_appliedStore = $collection->getHolidayAppliedStores();
            $_appliedStore = explode(',', $_appliedStore);
            if (in_array('0', $_appliedStore, true) || in_array($storeId, $_appliedStore, true)) {
            } else {
                unset($_storecollection[$_elementkey]);
            }
            $_elementkey += 1;
        }
        return $_storecollection;
    }

    /**
     *
     * @param Array $_storecollection
     * @return Array
     */
    public function getStoreHolidays($_storecollection)
    {
        $_holidaydays = [];
        foreach ($_storecollection as $key => $store) {
            $from_date = date_create($store['holiday_date_from']);
            $to_date = date_create($store['holiday_date_to']);
            $diff = date_diff($from_date, $to_date);

            if ($diff->format("%a") == 0) {
                if ($store['is_repetitive'] == 1) {
                    list($y, $m, $d) = explode('-', $store['holiday_date_from']);
                    $newdate = (date("Y")) . "-$m-$d";
                    $date = date("l jS F", strtotime($newdate . "+0 days"));
                    $_holidaydays[] = [
                        'date' => $date,
                        'holiday_name' => $store['holiday_name']
                        ];
                } else {
                    $_year_check = date_create($store['holiday_date_from']);
                    if ($_year_check->format("Y") == date("Y")) {
                        $date = date("l jS F", strtotime($store['holiday_date_from'] . "+0 days"));
                        $_holidaydays[] = [
                        'date' => $date,
                        'holiday_name' => $store['holiday_name']
                        ];
                    }
                }
            } elseif ($diff->format("%a") > 0) {
                if ($store['is_repetitive'] == 1) {
                    list($y, $m, $d) = explode('-', $store['holiday_date_from']);
                    $newdate = (date("Y")) . "-$m-$d";

                    for ($i = 0; $i <= $diff->format("%a"); $i++) {
                        $date = date("l jS F", strtotime($newdate . "+" . $i . "days"));
                         $_holidaydays[] = [
                        'date' => $date,
                        'holiday_name' => $store['holiday_name']
                         ];
                    }
                } else {
                    $_year_check = date_create($store['holiday_date_from']);
                    if ($_year_check->format("Y") == date("Y")) {
                        for ($i = 0; $i <= $diff->format("%a"); $i++) {
                            $date = date("l jS F", strtotime($store['holiday_date_from'] . "+" . $i . "days"));
                             $_holidaydays[] = [
                            'date' => $date,
                            'holiday_name' => $store['holiday_name']
                             ];
                        }
                    }
                }
            }
        }
        //echo "<pre>";print_r($_holidaydays);die();
        if (empty($_holidaydays)) {
            return false;
        }
        return $_holidaydays;
    }
}
