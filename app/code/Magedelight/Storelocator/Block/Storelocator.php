<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Block;

use Magento\Framework\View\Element\Template;
use Magedelight\Storelocator\Model\StorelocatorFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlFactory;
use Magedelight\Storelocator\Model\Storelocator\Image as ImageModel;
use Magedelight\Storelocator\Model\Source\Country;
use Magedelight\Storelocator\Model\Source\Region;
use Magento\Framework\View\Result\PageFactory;
use Magento\Checkout\Model\Cart;
use \Magento\Framework\Controller\Result\JsonFactory;

class Storelocator extends Template
{

    protected $_modelstorelocatorFactory;
    protected $urlFactory;
    protected $countryOptions;
    protected $regionOptions;
    protected $pageFactory;

    /**
     * @ Default Store image
     */
    const XML_PATH_STORE_IMAGE = 'magedelight_storelocator/storeinfo/logo';

    /**
     *  Store Title
     */
    const XML_PATH_STORE_TITLE = 'magedelight_storelocator/listviewinfo/frontend_title';

    /**
     *  Meta Information
     */
    const META_DESCRIPTION_CONFIG_PATH = 'magedelight_storelocator/listviewinfo/meta_description';
    const META_Title_CONFIG_PATH = 'magedelight_storelocator/listviewinfo/meta_title';
    const META_KEYWORDS_CONFIG_PATH = 'magedelight_storelocator/listviewinfo/meta_keywords';

    /* Distance in Kilometer/Miles  */
    const XML_PATH_STORE_DISTANCE = 'magedelight_storelocator/storesearch/distanceunit';

    /* Default Distance  */
    const XML_PATH_STORE_RADIOUS = 'magedelight_storelocator/storesearch/defaultradious';

    /* Max Radious */
    const XML_PATH_STORE_MAXRADIOUS = 'magedelight_storelocator/storesearch/maxradious';

    /* Google Map Api Key */
    const XML_PATH_STORE_MAP_API_KEY = 'magedelight_storelocator/googlemap/mapapi';

    /* Map Marker Image */
    const XML_PATH_STORE_MARKER_IMAGE = 'magedelight_storelocator/googlemap/markericon';

    /**
     * image model
     *
     * @var \Magedelight\Storelocator\Model\Storelocator\Image
     */
    protected $imageModel;

    /**
     *
     * @param Context $context
     * @param Registry $registry
     * @param Country $countryOptions
     * @param Region $regionOptions
     * @param ImageModel $imageModel
     * @param UrlFactory $urlFactory
     * @param PageFactory $pageFactory
     * @param StorelocatorFactory $modelStorelocatorFactory
     * @param Cart $cart
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Country $countryOptions,
        Region $regionOptions,
        ImageModel $imageModel,
        UrlFactory $urlFactory,
        PageFactory $pageFactory,
        StorelocatorFactory $modelStorelocatorFactory,
        Cart $cart,
        JsonFactory $resultJsonFactory
    ) {
        $this->_modelstorelocatorFactory = $modelStorelocatorFactory;
        $this->urlFactory = $urlFactory;
        $this->pageFactory = $pageFactory;
        $this->imageModel = $imageModel;
        $this->countryOptions = $countryOptions;
        $this->regionOptions = $regionOptions;
        $this->registry = $registry;
        $this->scopeConfig = $context->getScopeConfig();
        $this->cart = $cart;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($this->getStoreTitle());
        }

        return $this;
    }

    /**
     *
     * @return Array
     */
    public function getStorelist()
    {
        $storelocatorModel = $this->_modelstorelocatorFactory->create();
        $storelocatorCollection = $storelocatorModel->getCollection();
        $storelocatorCollection->addFieldToFilter('is_active', 1);
        $storelocatorData = $storelocatorCollection->getData();

        $currentStoreId = $this->_storeManager->getStore()->getId();
        foreach ($storelocatorData as $storekey => $store) {
            if ($store['storelocator_id']) {
                if (in_array($currentStoreId, explode(",", $store['store_ids'])) || in_array(0, explode(",", $store['store_ids']))) {
                } else {
                    unset($storelocatorData[$storekey]);
                }
            }
        }
        return $storelocatorData;
    }

    /**
     *
     * @param string $countryval
     * @return string
     */
    public function getCountryName($countryval)
    {
        $countryArray = $this->countryOptions->getOptions();
        $country = '';
        if(array_key_exists($countryval,$countryArray))
        {
            $country = $countryArray[$countryval];
        }
        return $country;
    }

    /**
     *
     * @param int $storelocatorid
     * @return string
     */
    public function getStoreImage($storelocatorid)
    {
        $storelocatorModel = $this->_modelstorelocatorFactory->create();
        $storelocatorModel->load($storelocatorid);
        $storeimage = $storelocatorModel->getStoreimage();
        //$storename = $storelocatorModel->getStorename();

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_config_image = $this->scopeConfig->getValue(self::XML_PATH_STORE_IMAGE, $storeScope);
        if ($storeimage != '') {
            $imagehtml = $this->imageModel->getBaseUrl() . $storeimage;
            return $imagehtml;
        } elseif ($_config_image != '') {
            return $this->imageModel->getBaseUrl() . '/' . $_config_image;
        } else {
            return $this->getViewFileUrl('Magedelight_Storelocator::images/image-default.png');
        }
    }

    /**
     *
     * @return string
     */
    public function getStoreTitle()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_config_Title = $this->scopeConfig->getValue(self::META_Title_CONFIG_PATH, $storeScope);

        if ($_config_Title != '') {
            return $_config_Title;
        } else {
            return 'Storelocator';
        }
    }

    /**
     *
     * @param int $storelocatorid
     * @return string
     */
    public function getContactInfo($storelocatorid)
    {
        $storelocatorModel = $this->_modelstorelocatorFactory->create();
        $storelocatorModel->load($storelocatorid);
        $phone = $storelocatorModel->getTelephone();
        $phone_frontend_status = $storelocatorModel->getPhoneFrontendStatus();
        $html = "";

        if ($phone_frontend_status) {
            if ($phone != '') {
                $html .= "<tr><td><strong>" . __("Phone no:") . "</strong></td><td>";
                $televalue = explode(':', $phone);
                foreach ($televalue as $key => $singlevalue) {
                    $html .= "<p>" . $singlevalue . "</p>";
                }
                $html .= "</td></tr>";
            }
            return $html;
        }
        return;
    }

    /**
     *
     * @return type string
     */
    public function getDistanceUnit()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_config_DistanceUnit = $this->scopeConfig->getValue(self::XML_PATH_STORE_DISTANCE, $storeScope);
        return $_config_DistanceUnit;
    }

    /**
     *
     * @return Int
     */
    public function getDefaultRadious()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_config_Radious = $this->scopeConfig->getValue(self::XML_PATH_STORE_RADIOUS, $storeScope);
        if (empty($_config_Radious)) {
            $_config_Radious = 10;
        }
        return $_config_Radious;
    }

    /**
     *
     * @return int
     */
    public function getMaxRadious()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_config_Radious = $this->scopeConfig->getValue(self::XML_PATH_STORE_MAXRADIOUS, $storeScope);
        if (empty($_config_Radious)) {
            $_config_Radious = 100;
        }
        return $_config_Radious;
    }

    /**
     *
     * @return String
     */
    public function getGoogleMapApiKey()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_Map_Api_Key = $this->scopeConfig->getValue(self::XML_PATH_STORE_MAP_API_KEY, $storeScope);
        return $_Map_Api_Key;
    }

    /**
     *
     * @param string $urlKey
     * @return string
     */
    public function getStoreUrlFromKey($urlKey)
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $suffix = trim($this->scopeConfig->getValue('magedelight_storelocator/listviewinfo/listpage_suffix', $storeScope), '/');
        $urlKey = $this->getUrl($urlKey);
        $urlKey = rtrim($urlKey, "/");
        $urlKey .= (strlen($suffix) > 0 || $suffix != '') ? '.' . str_replace('.', '', $suffix) : '';
        return $urlKey;
    }

    /**
     *
     * @return string
     */
    public function getMarkerImage()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $_marker_image = $this->scopeConfig->getValue(self::XML_PATH_STORE_MARKER_IMAGE, $storeScope);
        $_marker_image_Url = $this->imageModel->getBaseUrl() . '/' . $_marker_image;
        if (empty($_marker_image)) {
            return '';
        }
        return $_marker_image_Url;
    }

    /**
     *
     * @param string $regionvalue
     * @return string
     */
    public function getRegionName($region_id)
    {
        $regionArray = $this->regionOptions->getOptions();
        return $regionArray[$region_id];
    }
}
