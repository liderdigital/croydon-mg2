<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Block;

class Toplink extends \Magento\Framework\View\Element\Html\Link
{

    
    protected $_template = 'Magedelight_Storelocator::toplink.phtml';

    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->scopeConfig = $context->getScopeConfig();
    }

    /**
     *
     * @return string
     */
    public function getHref()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $urlKey = trim($this->scopeConfig->getValue('magedelight_storelocator/listviewinfo/frontend_url', $storeScope), '/');
        if (empty($urlKey)) {
            $urlKey = 'storelocator';
        }
        $suffix = trim($this->scopeConfig->getValue('magedelight_storelocator/listviewinfo/listpage_suffix', $storeScope), '/');
        $urlKey .= (strlen($suffix) > 0 || $suffix != '') ? '.' . str_replace('.', '', $suffix) : '/';
        return $this->_storeManager->getStore()->getBaseUrl() . $urlKey;
    }

    /**
     *
     * @return label
     */
    public function getLabel()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $label = $this->scopeConfig->getValue('magedelight_storelocator/storeinfo/linktitle', $storeScope);
        if (empty($label)) {
            $label = 'storelocator';
        }
        return __($label);
    }

    /**
     *
     * @return boolean
     */
    public function isStorelocatorEnable()
    {
        return $this->scopeConfig->getValue('magedelight_storelocator/storeinfo/storelocatorenable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     *
     * @return boolean
     */
    public function isTopLinkDisplay()
    {
        return $this->scopeConfig->getValue('magedelight_storelocator/storeinfo/displaytopmenu', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 'toplink' ? true : false;
    }
}
