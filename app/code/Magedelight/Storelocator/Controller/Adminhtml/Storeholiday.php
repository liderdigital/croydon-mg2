<?php

namespace Magedelight\Storelocator\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magedelight\Storelocator\Model\StoreholidayFactory;
use Magento\Framework\Registry;

abstract class storeholiday extends Action
{

    /**
     * storelocator factory
     *
     * @var AuthorFactory
     */
    protected $storeholidayFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * date filter
     *
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date
     */
    protected $dateFilter;

    /**
     * @param Registry $registry
     * @param AuthorFactory $storelocatorFactory
     * @param Context $context
     */
    public function __construct(
        Registry $registry,
        StoreholidayFactory $storeholidayFactory,
        Context $context
    ) {
        $this->coreRegistry = $registry;
        $this->storeholidayFactory = $storeholidayFactory;
        $this->resultRedirectFactory = $context->getRedirect();
        parent::__construct($context);
    }

    /**
     * @return \Magedelight\Storelocator\Model\Storelocator
     */
    protected function initStoreholiday()
    {
        $storeholidayId = (int) $this->getRequest()->getParam('holiday_id');
        /** @var \Magedelight\Storelocator\Model\Storelocator $storelocator */
        $storeholiday = $this->storeholidayFactory->create();
        if ($storeholidayId) {
            $storeholiday->load($storeholidayId);
        }
        $this->coreRegistry->register('magedelight_storelocator_storeholiday', $storeholiday);
        return $storeholiday;
    }
}
