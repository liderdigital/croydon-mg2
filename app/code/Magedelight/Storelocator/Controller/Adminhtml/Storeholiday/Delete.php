<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Controller\Adminhtml\Storeholiday;

use Magedelight\Storelocator\Controller\Adminhtml\Storeholiday;

class Delete extends Storeholiday
{

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('holiday_id');
        if ($id) {
            $name = "";
            try {
                /** @var \Magedelight\Storelocator\Model\Storelocator $storelocator */
                $storeholiday = $this->storeholidayFactory->create();
                $storeholiday->load($id);
                $name = $storeholiday->getName();
                $storeholiday->delete();

                $this->messageManager->addSuccess(__('The store has been deleted.'));
                $this->_eventManager->dispatch(
                    'adminhtml_magedelight_storelocator_storeholiday_on_delete',
                    ['name' => $name, 'status' => 'success']
                );
                $resultRedirect->setPath('*/*/');

                return $resultRedirect;
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_magedelight_storelocator_storeholiday_on_delete',
                    ['name' => $name, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                $resultRedirect->setPath('*/*/edit', ['holiday_id' => $id]);
                return $resultRedirect;
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magedelight_Storelocator::save');
    }
}
