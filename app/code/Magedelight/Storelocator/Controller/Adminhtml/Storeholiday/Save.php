<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Controller\Adminhtml\Storeholiday;

use Magento\Framework\Registry;
use Magedelight\Storelocator\Controller\Adminhtml\Storeholiday;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magedelight\Storelocator\Model\StoreholidayFactory;
use Magento\Backend\Model\Session;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\Helper\Js as JsHelper;

class Save extends Storeholiday
{

    /**
     * storelocator factory
     * @var
     */
    protected $storeholidayFactory;

    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $jsHelper;

    /**
     *
     * @param JsHelper $jsHelper
     * @param Registry $registry
     * @param StoreholidayFactory $storeholidayFactory
     * @param Context $context
     */
    public function __construct(
        JsHelper $jsHelper,
        Registry $registry,
        StoreholidayFactory $storeholidayFactory,
        Context $context
    ) {
        $this->jsHelper = $jsHelper;
        parent::__construct($registry, $storeholidayFactory, $context);
    }

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        //$data = $this->getRequest()->getPost();

        $data = $this->getRequest()->getPost('storeholiday');
        $tempdata = $data;

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($tempdata['all_store'] == 0) {
            $data["holiday_applied_stores"] = $tempdata['all_store'];
        } else {
            $data["holiday_applied_stores"] = implode(',', $tempdata['holiday_applied_stores']);
        }

        if (!array_key_exists('is_repetitive', $tempdata)) {
            /* array_push($data,"is_repetitive",0); */
            $data["is_repetitive"] = 0;
        }

        if ($data) {
            $storeholiday = $this->initStoreholiday();
            $storeholiday->setData($data);
            
            try {
                $storeholiday->save();
                $this->messageManager->addSuccess(__('The store has been saved.'));
                $this->_getSession()->setMagedelightStorelocatorStoreholidayData(false);

                if ($this->getRequest()->getParam('back')) {
                    $resultRedirect->setPath(
                        '*/*/edit',
                        [
                        'holiday_id' => $storeholiday->getId(),
                        '_current' => true
                            ]
                    );
                    return $resultRedirect;
                }
                $resultRedirect->setPath('*/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the store.'));
            }

            $this->_getSession()->setMagedelightStorelocatorStoreholidayData($data);
            $resultRedirect->setPath(
                '*/*/edit',
                [
                'storelocator_id' => $storelocator->getId(),
                '_current' => true
                    ]
            );
            return $resultRedirect;
        }
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magedelight_Storelocator::save');
    }
}
