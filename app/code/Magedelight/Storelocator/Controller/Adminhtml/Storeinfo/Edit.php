<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Controller\Adminhtml\Storeinfo;

use Magedelight\Storelocator\Controller\Adminhtml\Storelocator as StorelocatorController;
use Magento\Framework\Registry;
use Magedelight\Storelocator\Model\StorelocatorFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Edit extends StorelocatorController
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     *
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param StorelocatorFactory $storelocatorFactory
     * @param Context $context
     */
    public function __construct(
        Registry $registry,
        PageFactory $resultPageFactory,
        StorelocatorFactory $storelocatorFactory,
        Context $context
    ) {
        $this->backendSession = $context->getSession();
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($registry, $storelocatorFactory, $context);
    }

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('storelocator_id');
        /** @var \Magedelight\Storelocator\Model\Storelocator $storelocator */
        $storelocator = $this->initStorelocator();

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magedelight_Storelocator::storelocator');
        $resultPage->getConfig()->getTitle()->set((__('Storelocator')));

        // 2. Initial checking
        if ($id) {
            $storelocator->load($id);
            if (!$storelocator->getId()) {
                $this->messageManager->addError(__('This storelocator no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath(
                    'magedelight_storelocator/*/edit',
                    [
                    'storelocator_id' => $storelocator->getId(),
                    '_current' => true
                        ]
                );
                return $resultRedirect;
            }
        }

        // 3. Set entered data if was error when we do save

        $resultPage->getConfig()->getTitle()->prepend(__('Pages'));
        $resultPage->getConfig()->getTitle()
                ->prepend($storelocator->getId() ? $storelocator->getStorename() : __('New Store'));

        $data = $this->backendSession->getData('magedelight_storelocator_storelocator_data', true);
        if (!empty($data)) {
            $storelocator->setData($data);
        }
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magedelight_Storelocator::storeinfo_save');
    }
}
