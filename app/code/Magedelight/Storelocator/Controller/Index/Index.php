<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Controller\Index;

use Magedelight\Storelocator\Controller\Storelocator as StorelocatorController;
use Magento\Store\Model\ScopeInterface;

class Index extends StorelocatorController
{

    /**
     * Meta information
     */
    const META_DESCRIPTION_CONFIG_PATH = 'magedelight_storelocator/listviewinfo/meta_description';
    const META_Title_CONFIG_PATH = 'magedelight_storelocator/listviewinfo/meta_title';
    const META_KEYWORDS_CONFIG_PATH = 'magedelight_storelocator/listviewinfo/meta_keywords';

    /* Store Title */
    const XML_PATH_STORE_TITLE = 'magedelight_storelocator/listviewinfo/meta_title';

    public function execute()
    {

        $resultPage = $this->pageFactory->create();
        $title = $this->scopeConfig->getValue(self::META_Title_CONFIG_PATH, ScopeInterface::SCOPE_STORE);

        if (empty($title)) {
            $title = $this->scopeConfig->getValue(self::XML_PATH_STORE_TITLE, ScopeInterface::SCOPE_STORE);
        }

        if (empty($title)) {
            $title = 'Storelocator';
        }

        if ($title) {
            $resultPage->getConfig()->getTitle()->set($title);
        }
        $description = $this->scopeConfig->getValue(self::META_DESCRIPTION_CONFIG_PATH, ScopeInterface::SCOPE_STORE);
        if ($description) {
            $resultPage->getConfig()->setDescription($description);
        }
        $keyword = $this->scopeConfig->getValue(self::META_KEYWORDS_CONFIG_PATH, ScopeInterface::SCOPE_STORE);
        if ($keyword) {
            $resultPage->getConfig()->setKeywords($keyword);
        }
        
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
