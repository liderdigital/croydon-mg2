<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Controller\Index;

use Magedelight\Storelocator\Controller\Storelocator as StorelocatorController;
use Magento\Store\Model\ScopeInterface;

class View extends StorelocatorController
{

    public function execute()
    {

        $storelocatorid = $this->getRequest()->getParam('id');
        $this->registry->register('storelocatorid', $storelocatorid);

        $storelocatorModel = $this->_modelstorelocatorFactory->create();
        $storelocatorData = $storelocatorModel->load($storelocatorid);

        $resultPage = $this->pageFactory->create();
        if ($storelocatorData->getMetaTitle()) {
            $resultPage->getConfig()->getTitle()->set($storelocatorData->getMetaTitle());
        }
        if ($storelocatorData->getMetaDescription()) {
            $resultPage->getConfig()->setDescription($storelocatorData->getMetaDescription());
        }
        if ($storelocatorData->getMetaKeywords()) {
            $resultPage->getConfig()->setKeywords($storelocatorData->getMetaKeywords());
        }
        
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
