<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\ObjectManagerInterface;

class StoreList implements ArrayInterface
{

    /**
     * @var Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    public function __construct(
        ObjectManagerInterface $objectManager
        /* Context $context */
    ) {
        $this->_objectManager = $objectManager;
        /* parent::__construct($context); */
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => Storelocator::STATUS_ENABLED,
                'label' => __('Enable')
            ], [
                'value' => Storelocator::STATUS_DISABLED,
                'label' => __('Disable')
            ],
        ];
    }

    /**
     * get options as key value pair
     *
     * @return array
     */
    public function getOptions()
    {
        $storelocator = $this->_objectManager->create('Magedelight\Storelocator\Model\ResourceModel\Storelocator\Collection');
        $_options = [];
        foreach ($storelocator as $option) {
            $_options[] = [
                'value' => $option['storelocator_id'],
                'label' => $option['storename'],
            ];
        }
        return $_options;
    }
}
