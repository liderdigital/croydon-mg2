<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Filter\FilterManager;
use Magedelight\Storelocator\Model\Source\IsActive;
use Magedelight\Storelocator\Model\Source\StoreList;

class Storeholiday extends AbstractModel
{

    /**
     * status enabled
     *
     * @var int
     */
    const STATUS_ENABLED = 1;

    /**
     * status disabled
     *
     * @var int
     */
    const STATUS_DISABLED = 0;

    /**
     * @var IsActive
     */
    protected $statusList;
    protected $storeList;

    /**
     *
     * @param FilterManager $filter
     * @param IsActive $statusList
     * @param StoreList $storeList
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    function __construct(
        FilterManager $filter,
        IsActive $statusList,
        StoreList $storeList,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->filter = $filter;
        $this->statusList = $statusList;
        $this->storeList = $storeList;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magedelight\Storelocator\Model\ResourceModel\Storeholiday');
    }

    /**
     * sanitize the url key
     *
     * @param $string
     * @return string
     */
    public function formatUrlKey($string)
    {
        return $this->filter->translitUrl($string);
    }

    /**
     *
     * @return Array
     */
    public function getAvailableStatuses()
    {

        return $this->statusList->getOptions();
    }

    public function getAvailableStores()
    {

        return $this->storeList->getOptions();
    }

    public function checkUrlKey($urlKey, $storeId)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $storeId);
    }
}
