<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Filter\FilterManager;
use Magedelight\Storelocator\Api\Data\StorelocatorInterface;
use Magedelight\Storelocator\Model\Source\IsActive;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\App\ResourceConnection;

class Storelocator extends AbstractModel
{

    /**
     * status enabled
     *
     * @var int
     */
    const STATUS_ENABLED = 1;

    /**
     * status disabled
     *
     * @var int
     */
    const STATUS_DISABLED = 0;

    /**
     * @var IsActive
     */
    protected $statusList;
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * Filesystem instance.
     *
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;
    protected $resourceConnection;

    /**
     *
     * @param FilterManager $filter
     * @param IsActive $statusList
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    function __construct(
        FilterManager $filter,
        IsActive $statusList,
        \Magento\Framework\Filesystem $filesystem,
        StoreManagerInterface $storeManager,
        ResourceConnection  $resourceConnection,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->filter = $filter;
        $this->statusList = $statusList;
        $this->storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        $this->_logger = $context->getLogger();
        $this->resourceConnection = $resourceConnection;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magedelight\Storelocator\Model\ResourceModel\Storelocator');
    }

    /**
     * sanitize the url key
     *
     * @param $string
     * @return string
     */
    public function formatUrlKey($string)
    {
        return $this->filter->translitUrl($string);
    }

    public function getAvailableStatuses()
    {

        return $this->statusList->getOptions();
    }

    public function checkUrlKey($urlKey, $storeId)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $storeId);
    }
    
    public function uploadAndImport(\Magento\Framework\DataObject $object)
    {
        
        try {
            $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $uploader = $this->_objectManager->create('Magento\MediaStorage\Model\File\Uploader', ['fileId' => 'storeimport']);
            $uploader->setAllowedExtensions(['csv']);
        } catch (\Exception $e) {
            if ($e->getCode() == '666') {
                return $this;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
            }
        }
        $csvFile = $uploader->validateFile()['tmp_name'];
        $website = $this->storeManager->getWebsite($object->getScopeId());

        $this->_importWebsiteId = (int) $website->getId();
        $this->_importUniqueHash = [];
        $this->_importErrors = [];
        $this->_importedRows = 0;

        $tmpDirectory = $this->_filesystem->getDirectoryRead(DirectoryList::SYS_TMP);
        
       
        $path = $tmpDirectory->getRelativePath($csvFile);
        
        $stream = $tmpDirectory->openFile($path);
        $headers = $stream->readCsv();
        
        $_columns = array(
            'storename',
            'url_key',
            'description',
            'website_url',
            'facebook_url',
            'twitter_url',
            'address',
            'city',
            'state',
            'region_id',
            'country',
            'zipcode',
            'longitude',
            'latitude',
            'phone_frontend_status',
            'telephone',
            'storeimage',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'is_active',
            'store_ids',
            'days',
            'day_status',
            'open_hour',
            'open_minute',
            'close_hour',
            'close_minute',
            'delete',
        );
        
        if ($headers !== $_columns) {
            $this->_importErrors[] = "Headers do not match ";
            foreach ($headers as $header) {
                if(!in_array($header, $_columns)) {
                    $this->_importErrors[] = 'Headers do not match: ' . $header;
                }
            }
            if ($this->_importErrors) {
                $erros = __('We couldn\'t import this file because of these errors:  %1', implode(" \n", $this->_importErrors));
            }
            $stream->close();
            throw new \Magento\Framework\Exception\LocalizedException(__($erros));
        }
        
        if ($headers === false || count($headers) < 1) {
            $stream->close();
            throw new \Magento\Framework\Exception\LocalizedException(__('Please correct Storelocater File Format.'));
        }
        
        $connection = $this->resourceConnection->getConnection(ResourceConnection::DEFAULT_CONNECTION);
        $connection->beginTransaction();
        
        try {
            $rowNumber = 1;
            $importData = [];

            $temp = [];
            $csv= [];
            while (false !== ($csvLine = $stream->readCsv())) {
                ++$rowNumber;

                if (empty($csvLine)) {
                    continue;
                }
                $row = array();
                foreach ($csvLine as $key => $field)  {
                    $row[$headers[$key]] = $field;
                }
                $row = array_filter($row);
                $data[] = $row;
            }
            $newarray = [];
            $starttimearray = [];
            $count = 1;
            $currentRow = [];
            $i = 0;
            
            foreach ($data as $csvdata) {
                if(isset($csvdata['storename'])) {
                    if(count($data) == $count){
                       $currentRow = $csvdata;                   
                       $newarray[] = $currentRow;            
                    }
                    elseif(isset($currentRow) AND !empty($currentRow)){
                        $currentRow['storetime'] = $starttimearray;
                        $newarray[] = $currentRow;                 
                    }                    
                    $currentRow = $csvdata;
                    $starttimearray = [];                    
                } else {
                    $starttimearray[] = $csvdata;
                    if(count($data) == $count){
                        $currentRow['storetime'] = $starttimearray;
                        $newarray[] = $currentRow;
                    }
                }
                $count++;
            }
            
            foreach ($newarray as $storelocater) {
                if (!empty($storelocater['storetime'])) {
                    $storetime = $storelocater['storetime'];
                    $storetime1 = $storelocater['storetime'];
                    $i=0;
                    foreach ($storetime1 as $key => $value) {
                        if(array_key_exists('day_status', $value) && array_key_exists('open_hour', $value) && array_key_exists('open_minute', $value) && array_key_exists('close_hour', $value) && array_key_exists('close_minute', $value)){
                            $storetime[$i]['open_hour'] = str_pad($storetime[$i]['open_hour'], 2, "0", STR_PAD_LEFT);
                            $storetime[$i]['open_minute'] = str_pad($storetime[$i]['open_minute'], 2, "0", STR_PAD_LEFT);
                            $storetime[$i]['close_hour'] = str_pad($storetime[$i]['close_hour'], 2, "0", STR_PAD_LEFT);
                            $storetime[$i]['close_minute'] = str_pad($storetime[$i]['close_minute'], 2, "0", STR_PAD_LEFT);
                            $i++;
                        }else{
                            if(array_key_exists('day_status', $value) || array_key_exists('open_hour', $value) || array_key_exists('open_minute', $value) || array_key_exists('close_hour', $value) || array_key_exists('close_minute', $value)){
                                $storetime[$i]['day_status'] = "0";
                                $storetime[$i]['open_hour'] = (array_key_exists('open_hour', $value))?str_pad($storetime[$i]['open_hour'], 2, "0", STR_PAD_LEFT):"00";
                                
                                $storetime[$i]['open_minute'] = (array_key_exists('open_minute', $value))?str_pad($storetime[$i]['open_minute'], 2, "0", STR_PAD_LEFT):"00";
                                
                                $storetime[$i]['close_hour'] = (array_key_exists('close_hour', $value))?str_pad($storetime[$i]['close_hour'], 2, "0", STR_PAD_LEFT):"00";
                                
                                $storetime[$i]['close_minute'] = (array_key_exists('close_minute', $value))?str_pad($storetime[$i]['close_minute'], 2, "0", STR_PAD_LEFT):"00";
                                $i++;
                            }else{
                                unset($storetime[$i]);
                                $i++;
                            }
                        }   
                    }
                    $storelocater["storetime"] = serialize($storetime);
                    
                }
              
                /*Change for array to string conversion error - starts here*/
                if (empty($storelocater['storetime'])){
                    $storelocater['storetime'] = null;
                }
                /*Change for array to string conversion error - ends here*/
                if (!empty($storelocater['store_ids'])) {
                    $webstores = explode('-' , $storelocater['store_ids']);
                    $storelocater['store_ids'] = implode(',', $webstores);
                }
                $this->setData($storelocater);
                $this->save();
            }
            $stream->close();
            
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $connection->rollback();
            $stream->close();
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        } catch (\Exception $e) {
            $connection->rollback();
            $stream->close();
            $this->_logger->critical($e);
            throw new \Magento\Framework\Exception\LocalizedException(
            __('Something went wrong while importing Stores.')
            );
        }

        $connection->commit();
        
        if ($this->_importErrors) {
            $error = __('We couldn\'t import this file because of these errors: %1', implode(" \n", $this->_importErrors)
            );
            throw new \Magento\Framework\Exception\LocalizedException($error);
        }

        return $this;
    }
}