<?php
/**
 * Filters
 * Copyright (C) 2019  mofosys technologies
 * 
 * This file is part of Mofosys/Filters.
 * 
 */

namespace Mofosys\Filters\Block;

use Magento\Catalog\Model\Layer\Filter\FilterInterface;

class FilterRenderer extends \Magento\LayeredNavigation\Block\Navigation\FilterRenderer
{

    public function render(FilterInterface $filter) 
    {
        $this->assign('filterItems', $filter->getItems());
        $this->assign('filter', $filter);
        $html = $this->_toHtml();
        $this->assign('filterItems', array());
        return $html;
    }

    public function getRangeForPrice($filter) 
    {
        $priceForFilters = array('min' => 0, 'max' => 0);
        $priceGet = $filter->getResource()->loadPrices(10000000000);
        $priceForFilters['min'] = reset($priceGet);
        $priceForFilters['max'] = end($priceGet);
        return $priceForFilters;
    }

    public function getUrlForFilter() 
    {
        $query = array('price' => '');
        return $this->getUrl('*/*/*', array('_current' => true, '_use_rewrite' => true, '_query' => $query));
    }

}
