<?php

/**
 * Filters
 * Copyright (C) 2019  mofosys technologies
 * 
 * This file is part of Mofosys/Filters.
 * 
 */

namespace Mofosys\Filters\Helper;

use Magento\Framework\Registry;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Data extends AbstractHelper {

    protected $_registry;
    protected $_urlInterface;
    protected $_scopeConfig;
    protected $_paramrequest;

    public function __construct
    (
    Registry $registry, \Magento\Framework\UrlInterface $urlInterface, \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $productAttributeCollectionFactory, \Magento\Framework\Webapi\Rest\Request $request, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\App\Request\Http $paramrequest
    ) {

        $this->_registry = $registry;
        $this->_urlInterface = $urlInterface;
        $this->productAttributeCollectionFactory = $productAttributeCollectionFactory;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->request = $paramrequest;
    }

    public function getCategotyId() {
        $category = $this->_registry->registry('current_category');

        return $category->getId();
    }

    public function getCurrentUrl() {
        return $this->_urlInterface->getCurrentUrl();
    }

    public function getSearchBarOption($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT) {
        return $this->_scopeConfig->getValue(
                        'filters/filters/SearchBarOption', $scope
        );
    }

    public function getOutOfStockOption($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT) {
        return $this->_scopeConfig->getValue(
                        'cataloginventory/options/show_out_of_stock', $scope
        );
    }

    public function getSeeMoreOption($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT) {
        return $this->_scopeConfig->getValue(
                        'filters/filters/SeeMoreOption', $scope
        );
    }

    public function getIddata($param) {
        return $this->_request->getParam($param);
    }

}
