<?php
/**
 * Filters
 * Copyright (C) 2019  mofosys technologies
 * 
 * This file is part of Mofosys/Filters.
 * 
 */
namespace Mofosys\Filters\Model\Config\Source;

class SearchBarOption implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray() 
    {
        $options = array(
            array(
                'label' => __('yes'),
                'value' => '1',
            ),
            array(
                'label' => __('no'),
                'value' => '2',
            )
        );
        return $options;
    }

}
