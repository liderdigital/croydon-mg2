<?php

/**
 * Filters
 * Copyright (C) 2019  mofosys technologies
 * 
 * This file is part of Mofosys/Filters.
 * 
 */

namespace Mofosys\Filters\Model\Config\Source;

class SeeMoreOption implements \Magento\Framework\Option\ArrayInterface {

    public function toOptionArray() {
        $options = array(
            array(
                'label' => __('5'),
                'value' => '1',
            ),
            array(
                'label' => __('10'),
                'value' => '2',
            ),
            array(
                'label' => __('15'),
                'value' => '3',
            )
        );
        return $options;
    }

}
