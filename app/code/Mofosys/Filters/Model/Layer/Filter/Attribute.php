<?php
/**
 * Filters
 * Copyright (C) 2019  mofosys technologies
 * 
 * This file is part of Mofosys/Filters.
 * 
 */
namespace Mofosys\Filters\Model\Layer\Filter;

use Magento\Catalog\Model\Layer\Filter\AbstractFilter;


class Attribute extends AbstractFilter
{

    protected $_tagFilter;
    protected $_filterPlus;


    public function __construct(
        \Magento\Catalog\Model\Layer\Filter\ItemFactory $filterItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Layer $layer,
        \Magento\Catalog\Model\Layer\Filter\Item\DataBuilder $itemDataBuilder,
        \Magento\Framework\Filter\StripTags $tagFilter,
        array $data = array()
    ) {
        parent::__construct(
            $filterItemFactory, $storeManager, $layer, $itemDataBuilder, $data
        );
        $this->_tagFilter = $tagFilter;
        $this->_filterPlus = false;
    }


    public function apply(\Magento\Framework\App\RequestInterface $request) 
    {
        $filter = $request->getParam($this->_requestVar);
        if ($filter) {
            $this->_filterPlus = true;
        }

        if (!$filter || is_array($filter)) {
            return $this;
        }

        $filters = explode(',', $filter);
        $attribute = $this->getAttributeModel();
        $productCollection = $this->getLayer()->getProductCollection();
        // apply filtter to collection
        $productCollection->addFieldToFilter($attribute->getAttributeCode(), array("finset" => $filters));

        foreach ($filters as $option) {
            $text = $this->getOptionText($option);
            if ($option && $text) {
                $this->getLayer()->getState()->addFilter(
                    $this->_createItem($text, $option)
                );
            }
        }

        return $this;
    }

    protected function _getItemsData() 
    {
        $attribute = $this->getAttributeModel();
        /** @var \Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection $productCollection */
        $productCollection = $this->getLayer()
                ->getProductCollection();
        $optionsFacetedData = $productCollection->getFacetedData($attribute->getAttributeCode());
        $isAttributeFilterable = $this->
                getAttributeIsFilterable($attribute) === static::ATTRIBUTE_OPTIONS_ONLY_WITH_RESULTS;

        if (count($optionsFacetedData) === 0 && !$isAttributeFilterable) {
            return $this->itemDataBuilder->build();
        }

        $productSize = $productCollection->getSize();

        $options = $attribute->getFrontend()
                ->getSelectOptions();
        foreach ($options as $option) {
            $this->buildOptionData($option, $isAttributeFilterable, $optionsFacetedData, $productSize);
        }

        return $this->itemDataBuilder->build();
    }


    protected function buildOptionData($option, $isAttributeFilterable, $optionsFacetedData, $productSize) 
    {
        $value = $this->getOptionValue($option);
        $count = $this->getOptionCount($value, $optionsFacetedData);

        if ($this->_filterPlus) {
            if ($isAttributeFilterable && $count === 0) {
                return;
            }
        } else {
            if ($isAttributeFilterable && (!$this->isOptionReducesResults($count, $productSize) || $count === 0)) {
                return;
            }
        }

        $this->itemDataBuilder->addItemData(
            $this->_tagFilter->filter($option['label']), $value, $count
        );
    }


    protected function getOptionValue($option) 
    {
        if (empty($option['value']) && !is_numeric($option['value'])) {
            return false;
        }

        return $option['value'];
    }

  
    protected function getOptionCount($value, $optionsFacetedData) 
    {
        return isset($optionsFacetedData[$value]['count']) ? (int) $optionsFacetedData[$value]['count'] : 0;
    }

}
