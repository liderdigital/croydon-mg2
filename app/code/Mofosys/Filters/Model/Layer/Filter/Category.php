<?php

/**
 * Filters
 * Copyright (C) 2019  mofosys technologies
 * 
 * This file is part of Mofosys/Filters.
 * 
 */


namespace Mofosys\Filters\Model\Layer\Filter;

use Magento\Catalog\Model\Layer\Filter\DataProvider\Category as CategoryDataProvider;
use Magento\Catalog\Model\Layer\Filter\DataProvider\CategoryFactory;


class Category extends \Magento\Catalog\Model\Layer\Filter\AbstractFilter
{


    protected $_categoryId;
    protected $_appliedCategory;
    protected $_escaper;
    protected $_coreRegistry;
    protected $_dataProvider;
    protected $_filterPlus;

    public function __construct(
        \Magento\Catalog\Model\Layer\Filter\ItemFactory $filterItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Layer $layer,
        \Magento\Catalog\Model\Layer\Filter\Item\DataBuilder $itemDataBuilder,
        \Magento\Framework\Escaper $escaper,
        CategoryFactory $categoryDataProviderFactory,
        array $data = array()
    ) {
        parent::__construct($filterItemFactory, $storeManager, $layer, $itemDataBuilder, $data);
        $this->_escaper = $escaper;
        $this->_requestVar = 'cat';
        $this->_dataProvider = $categoryDataProviderFactory->create(array('layer' => $this->getLayer()));
        $this->appliedFilter = array();
        $this->_filterPlus = false;
    }


    public function getResetValue() 
    {
        return $this->_dataProvider->getResetValue();
    }


    public function apply(\Magento\Framework\App\RequestInterface $request) 
    {
        $categoryId = $request->getParam($this->_requestVar) ?: $request->getParam('id');

        if (empty($categoryId)) {
            return $this;
        }

        $categoryIds = explode(',', $categoryId);
        $categoryIds = array_unique($categoryIds);
        if ($request->getParam('id') != $categoryId) {
        $this->appliedFilter = $categoryId;

            if (!$this->_filterPlus) {
                $this->_filterPlus = true;
            }

            $productCollection->addCategoriesFilter(array('in' => $categoryIds));
            $category = $this->getLayer()->getCurrentCategory();
            $child = $category->getCollection()
                    ->addFieldToFilter($category->getIdFieldName(), array('in' => $categoryIds))
                    ->addAttributeToSelect('name');
            $categoriesInState = array();
            foreach ($categoryIds as $categoryId) {
                if ($currentCategory = $child->getItemById($categoryId)) {
                    $categoriesInState[$currentCategory->getId()] = $currentCategory->getName();
                }
            }

            foreach ($categoriesInState as $key => $category) {
                $state = $this->_createItem($category, $key);
                $this->getLayer()->getState()->addFilter($state);
            }
        }

        return $this;
    }


    protected function _getItemsData() 
    {

        $productCollection = $this->getLayer()->getProductCollection();
        $optionsFacetedData = $productCollection->getFacetedData('category');
        $category = $this->_dataProvider->getCategory();
        $categories = $category->getChildrenCategories();
        $collectionSize = $productCollection->getSize();
        $activeFilters = array();
        if ($this->appliedFilter) {
            $activeFilters = explode(',', $this->appliedFilter);
        }

        $currentProductIds = $productCollection->getAllIds();

        $this->getLayer()->getProductCollection()->addCountToCategories($categories);
        if ($category->getIsActive()) {
            foreach ($categories as $category) {
                if ($category->getIsActive() && isset($optionsFacetedData[$category->getId()])
                ) {
                    $active = in_array($category->getId(), $activeFilters);
                    $this->_itemBuilder->addItemData(
                        $this->escaper->escapeHtml($category->getName()), $category->getId(),
                        $optionsFacetedData[$category->getId()]['count'], $active, $this->_filterPlus
                    );
                }
            }
        }

        return $this->itemDataBuilder->build();
    }


    public function getName() 
    {
        return __('Category');
    }

}
