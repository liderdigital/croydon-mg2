<?php

/**
 * Filters
 * Copyright (C) 2019  mofosys technologies
 * 
 * This file is part of Mofosys/Filters.
 * 
 */

namespace Mofosys\Filters\Model\Layer\Filter;

class Item extends \Magento\Catalog\Model\Layer\Filter\Item {

    protected $_url;
    protected $_htmlPagerBlock;
    protected $_request;

    public function __construct(
    \Magento\Framework\UrlInterface $url, \Magento\Theme\Block\Html\Pager $htmlPagerBlock, \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_url = $url;
        $this->_htmlPagerBlock = $htmlPagerBlock;
        $this->_request = $request;
    }

    public function getFilter() {
        $filter = $this->getData('filter');
        if (!is_object($filter)) {
            throw new \Magento\Framework\Exception\LocalizedException(
            __('The filter must be an object. Please set the correct filter.')
            );
        }

        return $filter;
    }

    public function getUrl() {
        $value = array();
        $requestVar = $this->getFilter()->getRequestVar();
        if ($requestValue = $this->_request->getParam($requestVar)) {
            $value = explode(',', $requestValue);
        }

        $value[] = $this->getValue();
        $urlList = implode(',', $value);
        $query = array(
            $this->getFilter()->getRequestVar() => $urlList,
            // exclude current page from urls
            $this->_htmlPagerBlock->getPageVarName() => null,
        );
        return $this->_url->getUrl('*/*/*', array('_current' => true, '_use_rewrite' => true, '_query' => $query));
    }

    public function getRemoveUrl() {

        $value = array();
        $requestVar = $this->getFilter()->getRequestVar();
        if ($requestValue = $this->_request->getParam($requestVar)) {
            $value = explode(',', $requestValue);
        }

        if (in_array($this->getValue(), $value)) {
            $value = array_diff($value, array($this->getValue()));
        }

        if ($requestVar == 'price') {
            $value = [];
        }

        $query = array($requestVar => count($value) ? implode(',', $value) : $this->getFilter()->getResetValue());
        $params['_current'] = true;
        $params['_use_rewrite'] = true;
        $params['_query'] = $query;
        $params['_escape'] = true;
        return $this->_url->getUrl('*/*/*', $params);
    }

    public function getClearLinkUrl() {
        $clearLinkText = $this->getFilter()->getClearLinkText();
        if (!$clearLinkText) {
            return false;
        }

        $urlParams = array(
            '_current' => true,
            '_use_rewrite' => true,
            '_query' => array($this->getFilter()->getRequestVar() => null),
            '_escape' => true,
        );
        return $this->_url->getUrl('*/*/*', $urlParams);
    }

    public function getName() {
        return $this->getFilter()->getName();
    }

    public function getValueString() {
        $value = $this->getValue();
        if (is_array($value)) {
            return implode(',', $value);
        }

        return $value;
    }

}
