<?php
/**
 * Filters
 * Copyright (C) 2019  mofosys technologies
 * 
 * This file is part of Mofosys/Filters.
 * 
 */

namespace Mofosys\Filters\Model\ResourceModel\Layer\Filter;

class Attribute extends \Magento\Catalog\Model\ResourceModel\Layer\Filter\Attribute
{
    public function getCount(\Magento\Catalog\Model\Layer\Filter\FilterInterface $filter)
    {
        // clone select from collection with filters
        $layer = $filter->getLayer();
        if ($layer instanceof \Magento\Catalog\Model\Layer\Search) {
            $collectionSelect = $layer->getProductCollection()->getSelect();
        } else {
            $collectionSelect = $layer->getCurrentCategory()->getProductCollection()->getSelect();
        }

        $select = clone $collectionSelect;
        // reset columns, order and limitation conditions
        $select->reset(\Magento\Framework\DB\Select::COLUMNS);
        $select->reset(\Magento\Framework\DB\Select::ORDER);
        $select->reset(\Magento\Framework\DB\Select::LIMIT_COUNT);
        $select->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET);

        $connection = $this->getConnection();
        $attribute = $filter->getAttributeModel();
        $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $filter->getStoreId()),
        );

        $select->join(
            array($tableAlias => $this->getMainTable()),
            join(' AND ', $conditions),
            array('value', 'count' => new \Zend_Db_Expr("COUNT({$tableAlias}.entity_id)"))
        )->group(
            "{$tableAlias}.value"
        );

        return $connection->fetchPairs($select);
    }
}
