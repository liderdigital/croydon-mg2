<?php
/**
 * Filters
 * Copyright (C) 2019  mofosys technologies
 * 
 * This file is part of Mofosys/Filters.
 * 
 */

namespace Mofosys\Filters\Plugin;

class ModuleSwitch
{
    
    const XML_CONFIG_ENABLE = 'filters/filters/enable';
    protected $_scopeConfig;
    protected $_cacheTypeList;
    protected $_cacheFrontendPool;
    protected $_request;
    protected $_resourceConfig;
    protected $_configFactory;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Config\Model\Config\Factory $configFactory, 
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_cacheTypeList = $cacheTypeList;
        $this->_scopeConfig = $scopeConfig;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->_request = $request;
        $this->_resourceConfig = $resourceConfig;
        $this->_configFactory = $configFactory;
    }

    public function aroundExecute(\Magento\Config\Controller\Adminhtml\System\Config\Save $subject, \Closure $proceed) 
    {
        $section = $this->_request->getParam('section');
        $returnValue = $proceed();
        if ($section == 'filters') {
            $enableEnquiry = $this->_scopeConfig->getValue(self::XML_CONFIG_ENABLE);
            if (!$enableEnquiry) {
                $this->_resourceConfig->saveConfig(
                    "advanced/modules_disable_output/Mofosys_Filters",
                    true, 'default', 0
                );
            } else {
                $this->_resourceConfig->saveConfig(
                    "advanced/modules_disable_output/Mofosys_Filters",
                    false, 'default', 0
                );
            }
        }

        
        $types = array('config', 'layout', 'block_html', 'collections', 'reflection', 'db_ddl', 'eav',
            'config_integration', 'config_integration_api', 'full_page', 'translate', 'config_webservice');
        foreach ($types as $type) {
            $this->_cacheTypeList->cleanType($type);
        }

        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }

        return $returnValue;
    }

}