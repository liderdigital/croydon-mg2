# Ajaxlayerednavigation

It's a metapackage for the [original repository](https://github.com/swissup/module-ajaxlayerednavigation).

### [Installation](https://docs.swissuplabs.com/m2/extensions/ajaxlayerednavigation/installation/)

###### For clients

There are several ways to install extension for clients:

 1. If you've bought the product at Magento's Marketplace - use
    [Marketplace installation instructions](https://docs.magento.com/marketplace/user_guide/buyers/install-extension.html)

 2. Otherwise, you have two options:
    - Install the sources directly from [our repository](https://docs.swissuplabs.com/m2/extensions/ajaxlayerednavigation/installation/composer/) - **recommended**
    - Download archive and use [manual installation](https://docs.swissuplabs.com/m2/extensions/ajaxlayerednavigation/installation/manual/)


###### For maintainers

Run the following commands:
```bash
cd <magento_root>
composer config repositories.swissup composer https://docs.swissuplabs.com/packages/
composer require swissup/ajaxlayerednavigation --prefer-source
bin/magento module:enable Swissup_Core Swissup_Ajaxlayerednavigation
bin/magento setup:upgrade
bin/magento indexer:reindex
bin/magento setup:static-content:deploy
```
