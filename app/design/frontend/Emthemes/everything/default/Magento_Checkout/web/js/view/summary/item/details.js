define(
    [
        'uiComponent',
        'mage/url',
        'Magento_Customer/js/customer-data',
        'jquery',
        'ko',
        'underscore',
        'sidebar',
        'mage/translate'
    ],
    function (Component, url, customerData, $, ko, _) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'Magento_Checkout/summary/item/details'
            },
            getValue: function (quoteItem) {
                var itemId = elem.data('cart-item'),
                    itemQty = elem.data('item-qty');
                return quoteItem.name;
            },
            getDataPost: function (itemId) {
                console.log(itemId);
                var itemsData = window.checkoutConfig.quoteItemData;
                var obj = {};
                var obj = {
                    data: {}
                };

                itemsData.forEach(function (item) {
                    if (item.item_id == itemId) {
                        var mainlinkUrl = url.build('checkout/cart/delete/');
                        var baseUrl = url.build('checkout/');
                        console.log(mainlinkUrl);
                        obj.action = mainlinkUrl;
                        obj.data.id = item.item_id;
                        obj.data.uenc = btoa(baseUrl);
                    }
                });
                return JSON.stringify(obj);
            },
            getConfigUrl: function (itemId) {
                var itemsData = window.checkoutConfig.quoteItemData;
                var configUrl = null;
                var mainlinkUrl = url.build('checkout/cart/configure');
                var linkUrl;
                itemsData.forEach(function (item) {
                    var itm_id = item.item_id;
                    var product_id = item.product.entity_id;
                    if (item.item_id == itemId) {
                        linkUrl = mainlinkUrl + "/id/" + itm_id + "/product_id/" + product_id;
                    }
                });
                if (linkUrl != null) {
                    return linkUrl;
                } else {
                    return '';
                }

            }
        });
    }
);